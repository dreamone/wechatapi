package com.adopapa.wechatapi;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLTest {

	public static void main(String[] args) throws Exception {
		String postData = " <xml><ToUserName>wx406ea2215654009e</ToUserName><Encrypt><![CDATA[czFi0qM4mLzyH00DWnDxIMbPkOmCEQWR1rORbr3Uf6Y8jFSX2adVfIxoaBD20YL0zexQK8cLr1H1KlJPdAg6e7LDOsDdRVbWuky2GxT4Uh6WHxM6lFJWG7VHfLAq9xYXKkJmrA1B9YN/8LUSuONF0NAnuu7Ikk1z73GZDHO1n4HyAnohoROthonoXAKNlTU7L98mNWBqCtTX+UMJP4G6L9AVkmjXZ/KvNBn2U0DGgJHbJL/usJs4pj22hVkwYn4f8+2T6WE8t6vLdB4n2z/V67QeCrVKc53YXIjZ9rPqNBFLj++iOurhZtAQn3u5bJuGrHRAINnJQCIp3X28q+Q6PerN951c+qMYsRskrQS7n3utYdoIesG1tLS/FfW8jg62g/Ae0AYpHk0UkK13vskjb97EcQJYwFh3/47d6Hz9SsE=]]></Encrypt><AgentID><![CDATA[0]]></AgentID></xml>";

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		StringReader sr = new StringReader(postData);
		InputSource is = new InputSource(sr);
		Document document = db.parse(is);

		Element root = document.getDocumentElement();
		NodeList nodelist1 = root.getElementsByTagName("Encrypt");
		
		System.out.println(nodelist1.item(0).getNodeName());
		
		System.out.println(nodelist1.item(0).getTextContent());
		
	}

}
