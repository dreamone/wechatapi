package com.adopapa.wechatapi;

import com.adopapa.wechatapi.application.service.EWechatApi;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.ETextCustomerMessage;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.ETextCustomerMessage.Text;

public class Test {
	
	public static void main(String[] args) throws Exception {
		double data = 1.23E5;
		String formulate = "y =x*x*x+10*x*x+5*x+6";
		formulate = "var x=".concat(String.valueOf(data)).concat(";").concat(formulate).concat(";");
		Object result = ScriptEnginer.getJavaScriptEnginer().eval(formulate);
		
		System.out.println(result);
		
		String accessToken = "dQXoy-mLcIu-UAEdEwyFJ5Hqwjmslq0QjAD5xoK9IT09K7KdbpcTABs-CphB25A0";
		ETextCustomerMessage message = new ETextCustomerMessage();
		message.setAgentId(0);
		message.setSafe("0");
		//message.setToUser(memberUserInfo.getUserCode());
		//message.setToUser("F001|F002|WangQianHui|BoBi|DanDing|C001|adopapa");
		message.setToTag("1");
		message.setText(new Text("您已经通过审核，正式成为管家会员，请遵守管家会员协议开展业务。"));
		System.out.println(message.toString());
		boolean res = EWechatApi.sendMessage(accessToken, message);
		System.out.println(res);
	}

}
