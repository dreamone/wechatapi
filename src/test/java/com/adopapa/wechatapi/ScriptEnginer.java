package com.adopapa.wechatapi;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public final class ScriptEnginer {
	
	private final static ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
	
	private final static String JAVA_SCRIPT = "javascript";
	
	
	public static ScriptEngine getJavaScriptEnginer() {
		
		return scriptEngineManager.getEngineByName(JAVA_SCRIPT);
		
	}
	
	public static void main(String[] args) {
		System.out.println( getJavaScriptEnginer().getClass().getName() );
	}
	

}
