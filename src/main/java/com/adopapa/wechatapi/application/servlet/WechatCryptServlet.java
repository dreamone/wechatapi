package com.adopapa.wechatapi.application.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.adopapa.wechatapi.util.SignUtil;
import com.qq.weixin.mp.aes.AesException;
import com.qq.weixin.mp.aes.WXBizMsgCrypt;

public class WechatCryptServlet extends HttpServlet {

	private static final long serialVersionUID = 2351999635954439571L;

	private Logger logger = Logger.getLogger(WechatCryptServlet.class);

	private String token;
	private String aesKey;
	private String appId;

	private Object serviceObject;
	private Method serviceMethod;

	private WXBizMsgCrypt crypt = null;

	@Override
	public void init(ServletConfig config) throws ServletException {

		super.init(config);

		token = config.getInitParameter("token");
		aesKey = config.getInitParameter("aesKey");
		appId = config.getInitParameter("appId");

		String service = config.getInitParameter("service");
		String method = config.getInitParameter("method");
		// validate

		try {
			if (method == null) {
				// default method is process
				method = "process";
			}
			if (service == null) {
				service = "com.adopapa.wechatapi.application.service.WeChatService";
			}
			logger.info("service is ".concat(service).concat(" and method is ".concat(method)));
			Class clazz = Class.forName(service);
			serviceObject = clazz.newInstance();
			serviceMethod = clazz.getMethod(method, HttpServletRequest.class, HttpServletResponse.class,
					WXBizMsgCrypt.class);
			logger.info("token is ".concat(token).concat(" aesKey is ".concat(aesKey)//
					.concat("appId is ".concat(appId))));
			crypt = new WXBizMsgCrypt(token, aesKey, appId);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}

	/**
	 * 确认请求来自微信服务器
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// // 微信加密签名
		// String signature = request.getParameter("signature");
		// // 时间戳
		// String timestamp = request.getParameter("timestamp");
		// // 随机数
		// String nonce = request.getParameter("nonce");
		// // 随机字符串
		// String echostr = request.getParameter("echostr");
		//
		// logger.info(signature);
		// logger.info(timestamp);
		// logger.info(nonce);
		// logger.info(echostr);
		// logger.info(request.getQueryString());
		// PrintWriter out = response.getWriter();
		// // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
		// String result = null;
		// try {
		// // 验证URL函数
		// result = crypt.verifyUrl(signature, timestamp, nonce, echostr);
		// } catch (AesException e) {
		// e.printStackTrace();
		// }
		// if (result == null) {
		// // result为空，赋予token
		// result = token;
		// }
		// out.print(result);
		// out.close();
		// out = null;
		// 微信加密签名
		String signature = request.getParameter("signature");
		// 时间戳
		String timestamp = request.getParameter("timestamp");
		// 随机数
		String nonce = request.getParameter("nonce");
		// 随机字符串
		String echostr = request.getParameter("echostr");

		PrintWriter out = response.getWriter();
		// 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
		if (!SignUtil.checkSignature(signature, timestamp, nonce)) {
			logger.error("Validate failed!");
		}
		logger.info(echostr);
		out.print(echostr);
		out.close();
		out = null;
	}

	/**
	 * 处理微信服务器发来的消息
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 将请求、响应的编码均设置为UTF-8（防止中文乱码）
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		logger.info("process request from wechat!");
		// 调用核心业务类接收消息、处理消息
		try {
			serviceMethod.invoke(serviceObject, request, response, crypt);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

		// 响应消息
		// PrintWriter out = response.getWriter();
		// out.print(respMessage);
		//
		// out.close();
	}
}
