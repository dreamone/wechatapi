package com.adopapa.wechatapi.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Map;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.adopapa.wechatapi.domain.media.DownloadMediaResult;
import com.adopapa.wechatapi.domain.pay.CloseRequest;
import com.adopapa.wechatapi.domain.pay.RedpacketRequest;
import com.adopapa.wechatapi.domain.pay.RefundRequest;
import com.adopapa.wechatapi.domain.pay.UnifiedOrder;

public class WeChatUtil {

	protected static Logger logger = Logger.getLogger(WeChatUtil.class);

	/**
	 * 发起https请求并获取结果
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static String httpsRequest(String requestUrl, String requestMethod, String outputStr) {
		String result = null;
		StringBuffer buffer = new StringBuffer();
		try {
			// 创建SSLContext对象，并使用我们指定的信任管理器初始化
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			// 从上述SSLContext对象中得到SSLSocketFactory对象
			SSLSocketFactory ssf = sslContext.getSocketFactory();

			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			httpUrlConn.setRequestMethod(requestMethod);

			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();

			// 当有数据需要提交时
			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 将返回的输入流转换成字符串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			result = buffer.toString();
		} catch (ConnectException ce) {
			logger.error("Weixin server connection timed out.");
			ce.printStackTrace();
		} catch (Exception e) {
			logger.error("https request error:{}", e);
			e.printStackTrace();
		}
		return result;
	}

	public static String httpsRequestWithCert(String requestUrl, String requestMethod, String outputStr,
			InputStream certStream, String password) {
		String result = null;
		StringBuffer buffer = new StringBuffer();
		try {
			// 创建SSLContext对象，并使用我们指定的信任管理器初始化
			// TrustManager[] tm = { new MyX509TrustManager() };
			// SSLContext sslContext = SSLContext.getInstance("SSL",
			// "SunJSSE");
			// sslContext.init(null, tm, new
			// java.security.SecureRandom());
			// // 从上述SSLContext对象中得到SSLSocketFactory对象
			// SSLSocketFactory ssf = sslContext.getSocketFactory();

			// 证书
			char[] pwdBytes = password.toCharArray();
			KeyStore keyStore = KeyStore.getInstance("PKCS12");
			try {
				keyStore.load(certStream, pwdBytes);
			} finally {
				certStream.close();
			}

			// 实例化密钥库 & 初始化密钥工厂
			KeyManagerFactory keyManagerFactory = KeyManagerFactory
					.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keyStore, pwdBytes);

			// 创建 SSLContext
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(keyManagerFactory.getKeyManagers(), null, new SecureRandom());

			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
			httpUrlConn.setSSLSocketFactory(sslContext.getSocketFactory());

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			httpUrlConn.setRequestMethod(requestMethod);

			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();

			// 当有数据需要提交时
			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 将返回的输入流转换成字符串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			result = buffer.toString();
		} catch (ConnectException ce) {
			logger.error("Weixin server connection timed out.");
			ce.printStackTrace();
		} catch (Exception e) {
			logger.error("https request error:{}", e);
			e.printStackTrace();
		} finally {

		}
		return result;
	}

	/**
	 * 发起https请求并获取结果
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static String httpRequest(String requestUrl, String requestMethod, String outputStr) {
		String result = null;
		StringBuffer buffer = new StringBuffer();
		try {
			URL url = new URL(requestUrl);
			HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			httpUrlConn.setRequestMethod(requestMethod);

			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();

			// 当有数据需要提交时
			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 将返回的输入流转换成字符串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			result = buffer.toString();
		} catch (ConnectException ce) {
			logger.error("Weixin server connection timed out.");
		} catch (Exception e) {
			logger.error("https request error:{}", e);
		}
		return result;
	}

	/**
	 * 上传媒体文件
	 */
	public static String upload(String url, File file) throws Exception {
		String BOUNDARY = "----WebKitFormBoundaryiDGnV9zdZA1eM1yL";
		StringBuffer bufferRes = null;
		URL postUrl = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "Keep-Alive");
		conn.setRequestProperty("user-agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36");
		conn.setRequestProperty("Charsert", "UTF-8");
		conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

		OutputStream out = new DataOutputStream(conn.getOutputStream());
		byte[] endData = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();// 定义最后数据分隔线
		StringBuilder sb = new StringBuilder();
		sb.append("--");
		sb.append(BOUNDARY);
		sb.append("\r\n");
		sb.append("Content-Disposition: form-data;name=\"media\";filename=\"" + file.getName() + "\"\r\n");
		sb.append("Content-Type:application/octet-stream\r\n\r\n");
		byte[] data = sb.toString().getBytes();
		out.write(data);
		DataInputStream fs = new DataInputStream(new FileInputStream(file));
		int bytes = 0;
		byte[] bufferOut = new byte[1024];
		while ((bytes = fs.read(bufferOut)) != -1) {
			out.write(bufferOut, 0, bytes);
		}
		out.write("\r\n".getBytes());
		fs.close();
		out.write(endData);
		out.flush();
		out.close();

		InputStream in = conn.getInputStream();
		BufferedReader read = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		String valueString = null;
		bufferRes = new StringBuffer();
		while ((valueString = read.readLine()) != null) {
			bufferRes.append(valueString);
		}
		in.close();
		if (conn != null) {
			conn.disconnect();
		}
		return bufferRes.toString();
	}

	/**
	 * 下载资源
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static DownloadMediaResult downloadMedia(String url) throws IOException {
		logger.info("Download: " + url);
		DownloadMediaResult media = null;
		URL getUrl = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) getUrl.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("GET");
		conn.connect();
		if (conn.getContentType().equalsIgnoreCase("text/plain")) {
			InputStream inputStream = conn.getInputStream();
			String json = IOUtils.toString(inputStream);
			inputStream.close();
			media = JsonUtil.json2Bean(json, DownloadMediaResult.class);
		} else {
			media = new DownloadMediaResult();
			BufferedInputStream bufferedInputStream = new BufferedInputStream(conn.getInputStream());
			String ds = conn.getHeaderField("Content-disposition");
			String fullName = ds.substring(ds.indexOf("filename=\"") + 10, ds.length() - 1);
			String relName = fullName.substring(0, fullName.lastIndexOf("."));
			String suffix = fullName.substring(relName.length() + 1);
			media.setFullName(fullName);
			media.setFileName(relName);
			media.setSuffix(suffix);
			media.setContentLength(conn.getHeaderField("Content-Length"));
			media.setContentType(conn.getHeaderField("Content-Type"));
			media.setInputStream(bufferedInputStream);
		}
		return media;
	}

	/**
	 * sign for prepay id
	 */
	public static String sign4PrepayId(UnifiedOrder unifiedOrder, String mechantKey) {
		TreeMap<String, String> contentMap = new TreeMap<String, String>();
		contentMap.put("appid", unifiedOrder.getAppId());
		contentMap.put("mch_id", unifiedOrder.getMechantId());
		contentMap.put("nonce_str", unifiedOrder.getNonceStr());
		contentMap.put("body", unifiedOrder.getBody());
		contentMap.put("out_trade_no", unifiedOrder.getOutTradeNo());
		contentMap.put("total_fee", String.valueOf(unifiedOrder.getTotalFee()));
		contentMap.put("spbill_create_ip", unifiedOrder.getSpbillCreateIP());
		contentMap.put("notify_url", unifiedOrder.getNotifyUrl());
		contentMap.put("trade_type", unifiedOrder.getTradeType());
		if (unifiedOrder.getOpenId() != null) {
			contentMap.put("openid", unifiedOrder.getOpenId());
			contentMap.put("device_info", unifiedOrder.getDeviceInfo());
		}
		// contentMap.put("time_start", unifiedOrder.getTimeStart());
		// contentMap.put("time_expire", unifiedOrder.getTimeExpire());

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : contentMap.entrySet()) {
			if (entry.getValue() != null) {
				sb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		if (sb.length() > 0) {
			sb.append("&key=").append(mechantKey);
			logger.info(sb.toString());
			String result = MD5Util.MD5Encode(sb.substring(1));
			return result.toUpperCase();
		}
		return null;
	}

	/**
	 * sign for pay appId, timeStamp, nonceStr, package, signType。
	 */
	public static String sign4Pay(String appId, String timestamp, String nonceStr, String prepayId, String signType,
			String mechantKey) {
		TreeMap<String, String> contentMap = new TreeMap<String, String>();
		contentMap.put("appId", appId);
		contentMap.put("timeStamp", timestamp);
		contentMap.put("nonceStr", nonceStr);
		contentMap.put("package", "prepay_id=".concat(prepayId));
		contentMap.put("signType", signType);
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : contentMap.entrySet()) {
			sb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
		}
		if (sb.length() > 0) {
			sb.append("&key=").append(mechantKey);
			String result = MD5Util.MD5Encode(sb.substring(1));
			return result.toUpperCase();
		}
		return null;
	}

	public static String sign4AppPay(String appId, String mechantId, String timestamp, String nonceStr, String prepayId,
			String mechantKey) {
		TreeMap<String, String> contentMap = new TreeMap<String, String>();
		contentMap.put("appid", appId);
		contentMap.put("partnerid", mechantId);
		contentMap.put("prepayid", prepayId);
		contentMap.put("package", "Sign=WXPay");
		contentMap.put("noncestr", nonceStr);
		contentMap.put("timestamp", timestamp);
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : contentMap.entrySet()) {
			sb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
		}
		if (sb.length() > 0) {
			sb.append("&key=").append(mechantKey);
			String result = MD5Util.MD5Encode(sb.substring(1));
			return result.toUpperCase();
		}
		return null;
	}

	public static String sign4Refund(RefundRequest refundRequest, String mechantKey) {
		TreeMap<String, String> contentMap = new TreeMap<String, String>();
		contentMap.put("appid", refundRequest.getAppId());
		contentMap.put("mch_id", refundRequest.getMechantId());
		contentMap.put("nonce_str", refundRequest.getNonceStr());
		contentMap.put("sign_type", "MD5");
		if (refundRequest.getTransactionId() != null) {
			contentMap.put("transaction_id", refundRequest.getTransactionId());
		}
		if (refundRequest.getOutTradeNo() != null) {
			contentMap.put("out_trade_no", refundRequest.getOutTradeNo());
		}
		contentMap.put("out_refund_no", refundRequest.getOutRefundNo());
		contentMap.put("total_fee", refundRequest.getTotalFee());
		contentMap.put("refund_fee", refundRequest.getRefundFee());

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : contentMap.entrySet()) {
			if (entry.getValue() != null) {
				sb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		if (sb.length() > 0) {
			sb.append("&key=").append(mechantKey);
			logger.info(sb.toString());
			String result = MD5Util.MD5Encode(sb.substring(1));
			return result.toUpperCase();
		}
		return null;
	}

	public static String sign4Close(CloseRequest closeRequest, String mechantKey) {
		TreeMap<String, String> contentMap = new TreeMap<String, String>();
		contentMap.put("appid", closeRequest.getAppId());
		contentMap.put("mch_id", closeRequest.getMechantId());
		contentMap.put("nonce_str", closeRequest.getNonceStr());
		contentMap.put("sign_type", "MD5");
		contentMap.put("out_trade_no", closeRequest.getOutTradeNo());

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : contentMap.entrySet()) {
			if (entry.getValue() != null) {
				sb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		if (sb.length() > 0) {
			sb.append("&key=").append(mechantKey);
			logger.info(sb.toString());
			String result = MD5Util.MD5Encode(sb.substring(1));
			return result.toUpperCase();
		}
		return null;
	}

	/*
	 * public static String sign4JsApi(String jsApiTicket, String nonceStr, String
	 * timestamp, String url) { String params =
	 * "jsapi_ticket=%s&nonceStr=%s&timestamp=%s&url=%s"; String result =
	 * SignUtil.sign(String.format(params, jsApiTicket, nonceStr, timestamp, url));
	 * return result; }
	 */

	/**
	 * sign for jsApi
	 */
	public static Map<String, String> sign4JsApi(String jsApiTicket, String url) {
		Map<String, String> result = Sign4Js.sign(jsApiTicket, url);
		return result;
	}

	/**
	 * sign for redpack
	 */
	public static String sign4Redpacket(RedpacketRequest rRequest, String mechantKey) {
		TreeMap<String, String> contentMap = new TreeMap<String, String>();
		contentMap.put("client_ip", rRequest.getClientIp());// 1
		contentMap.put("mch_billno", rRequest.getMchBillno());// 2
		contentMap.put("mch_id", rRequest.getMchId());// 3
		contentMap.put("nonce_str", rRequest.getNonceStr());// 4
		contentMap.put("re_openid", rRequest.getReOpenid());// 5
		if (rRequest.getSceneId() != null) {
			contentMap.put("scene_id", rRequest.getSceneId());// 6
		}
		contentMap.put("send_name", rRequest.getSendName());// 7
		contentMap.put("wishing", rRequest.getWishing());// 8
		contentMap.put("wxappid", rRequest.getWxappId());// 9
		contentMap.put("total_amount", String.valueOf(rRequest.getTotalAmount()));// 10
		contentMap.put("total_num", String.valueOf(rRequest.getTotalNum()));// 11
		contentMap.put("remark", rRequest.getRemark());// 12
		if (rRequest.getRiskInfo() != null) {
			contentMap.put("risk_info", rRequest.getRiskInfo()); // 13
		}
		contentMap.put("act_name", rRequest.getActName());// 14
		// contentMap.put("sign_type", "MD5");

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : contentMap.entrySet()) {
			if (entry.getValue() != null) {
				sb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		if (sb.length() > 0) {
			sb.append("&key=").append(mechantKey);
			logger.info(sb.toString());
			String result = MD5Util.MD5Encode(sb.substring(1));
			return result.toUpperCase();
		}
		return null;

	}

	public static void main(String[] args) {
		// System.out.println(sign4JsApi("11111",
		// "http://example.com"));

		String appId = "wxca333581893212b9";
		String mechantId = "1312869101";
		String timestamp = "1454570050";
		String nonceStr = "967669afe88c4e109d9166ac10c379ac";
		String prepayId = "wx20160204142953dd89d5a4c80236441562";
		String mechantKey = "lianhesupeixinapidemima20160204a";
		System.out.println(sign4AppPay(appId, mechantId, timestamp, nonceStr, prepayId, mechantKey));

		appId = "wx20c3792650764e5d";
		mechantId = "1486220982";
		timestamp = "1500968760";
		nonceStr = "46715851b266447498e4f2041a0604c7";
		prepayId = "wx20170725155056c5b3950cd80464512509";
		mechantKey = "2beb30c4b6e84121852734609612a229";
		// appId, timeStamp, nonceStr, package, signType
		String string = "appId=wx20c3792650764e5d&nonceStr=46715851b266447498e4f2041a0604c7";
		string += "&package=prepay_id=wx20170725155056c5b3950cd80464512509&signType=MD5&timeStamp=1500968760";
		string += "&key=2beb30c4b6e84121852734609612a229";

		System.out.println(
				"appId=wx20c3792650764e5d&nonceStr=46715851b266447498e4f2041a0604c7&package=prepay_id=wx20170725155056c5b3950cd80464512509&signType=MD5&timeStamp=1500968760&key=2beb30c4b6e84121852734609612a229");
		System.out.println(string);

		System.out.println(MD5Util.MD5Encode(string).toUpperCase());
		System.out.println("1C16E33D1FC95B6919DCD7C1EA502F26");

		System.err.println(sign4Pay(appId, timestamp, nonceStr, prepayId, "MD5", mechantKey));

		// {
		// "appId": "wx8888888888888888",
		// "timeStamp": "1414411784",
		// "nonceStr": "gbwr71b5no6q6ne18c8up1u7l7he2y75",
		// "package": "prepay_id=wx201410272009395522657a690389285100",
		// "signType": "MD5",
		// "paySign": "9C6747193720F851EB876299D59F6C7D"
		// }

		appId = "wx8888888888888888";
		timestamp = "1414411784";
		nonceStr = "gbwr71b5no6q6ne18c8up1u7l7he2y75";
		prepayId = "wx201410272009395522657a690389285100";
		// signType="MD5";
		// paySign"9C6747193720F851EB876299D59F6C7D"

		// System.err.println(sign4Pay(appId, timestamp, nonceStr, prepayId, signType,
		// mechantKey));
		
		System.out.println(MD5Util.MD5Encode("act_name=新春报盘红包&client_ip=39.106.181.169&mch_billno=H201902241337529289&mch_id=1486220982&nonce_str=e9261da36ad24b06ae5a14e376d0af52&re_openid=og5m6w8OMH4bty1Rfwe6KB8558VU&remark=一点钢-你的钢贸管家&scene_id=PRODUCT_2&send_name=一点钢网&total_amount=158&total_num=1&wishing=新春大吉，恭喜发财&wxappid=wx20c3792650764e5d&key=2beb30c4b6e84121852734609612a229"));
		System.out.println(MD5Util.MD5Encode("act_name=新春报盘红包&client_ip=39.106.181.169&mch_billno=H201902241337529289&mch_id=1486220982&nonce_str=e9261da36ad24b06ae5a14e376d0af52&re_openid=og5m6w8OMH4bty1Rfwe6KB8558VU&remark=一点钢-你的钢贸管家&scene_id=PRODUCT_2&send_name=一点钢网&total_amount=158&total_num=1&wishing=新春大吉，恭喜发财&wxappid=wx20c3792650764e5d&key=2beb30c4b6e84121852734609612a229"));
		System.out.println(MD5Util.MD5Encode("act_name=新春报盘红包&client_ip=39.106.181.169&mch_billno=H201902241337529289&mch_id=1486220982&nonce_str=e9261da36ad24b06ae5a14e376d0af52&re_openid=og5m6w8OMH4bty1Rfwe6KB8558VU&remark=一点钢-你的钢贸管家&scene_id=PRODUCT_2&send_name=一点钢网&total_amount=158&total_num=1&wishing=新春大吉，恭喜发财&wxappid=wx20c3792650764e5d&key=2beb30c4b6e84121852734609612a229"));
		System.out.println(MD5Util.MD5Encode("act_name=新春报盘红包&client_ip=39.106.181.169&mch_billno=H201902241337529289&mch_id=1486220982&nonce_str=e9261da36ad24b06ae5a14e376d0af52&re_openid=og5m6w8OMH4bty1Rfwe6KB8558VU&remark=一点钢-你的钢贸管家&scene_id=PRODUCT_2&send_name=一点钢网&total_amount=158&total_num=1&wishing=新春大吉，恭喜发财&wxappid=wx20c3792650764e5d&key=2beb30c4b6e84121852734609612a229"));
	}

}