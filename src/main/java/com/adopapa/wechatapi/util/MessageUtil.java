package com.adopapa.wechatapi.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

//import org.dom4j.Document;
//import org.dom4j.Element;
//import org.dom4j.io.SAXReader;
//import org.xml.sax.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

public class MessageUtil {
	
	

	/**
	 * 解析微信发来的请求（XML）
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static Map<String, String> parseXml(InputStream inputStream) throws Exception {
		// 将解析结果存储在HashMap中
		Map<String, String> map = new HashMap<String, String>();

		// 从request中取得输入流
		// InputStream inputStream = request.getInputStream();
		// 读取输入流
		// SAXReader reader = new SAXReader();
		// Document document = reader.read(inputStream);
		// // 得到xml根元素
		// Element root = document.getRootElement();
		// // 得到根元素的所有子节点
		// List<Element> elementList = root.elements();
		//
		// // 遍历所有子节点
		// for (Element e : elementList)
		// map.put(e.getName(), e.getText());

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbf.setXIncludeAware(false);
		dbf.setExpandEntityReferences(false);
		
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(inputStream);
		document.normalize();
		Element root = document.getDocumentElement();

		NodeList childNodes = root.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			map.put(childNodes.item(i).getNodeName(), childNodes.item(i).getTextContent());
		}
		// 释放资源
		inputStream.close();
		inputStream = null;

		return map;
	}

	public static Map<String, String> parseXml(String request) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(request.getBytes("UTF-8"));
		return parseXml(inputStream);
	}

	private static XppDriver xppDriver0;
	private static XppDriver xppDriver1;

	public synchronized static XppDriver getXppDriver(final boolean need) {
		if (xppDriver0 == null) {
			xppDriver0 = new XppDriver() {

				public HierarchicalStreamWriter createWriter(Writer out) {
					return new PrettyPrintWriter(out) {
						// 对所有xml节点的转换都增加CDATA标记
						boolean cdata = false;

						@SuppressWarnings("rawtypes")
						public void startNode(String name, Class clazz) {
							cdata = clazz == java.lang.String.class;

							if ("item".equals(name) || "xml".equals(name)) {
								super.startNode(name);
							} else {
								super.startNode(name.substring(0, 1).toUpperCase().concat(name.substring(1)));
							}
						}

						protected void writeText(QuickWriter writer, String text) {
							if (cdata && need) {
								writer.write("<![CDATA[");
								writer.write(text);
								writer.write("]]>");
							} else {
								writer.write(text);
							}
						}
					};
				}
			};
		}
		return xppDriver0;
	}
	
	public synchronized static XppDriver getXppDriver(final boolean need, final boolean firstUpper) {
		if (xppDriver1 == null) {
			xppDriver1 = new XppDriver() {
				public HierarchicalStreamWriter createWriter(Writer out) {
					return new PrettyPrintWriter(out) {
						// 对所有xml节点的转换都增加CDATA标记
						boolean cdata = false;
						@SuppressWarnings("rawtypes")
						public void startNode(String name, Class clazz) {
							cdata = clazz == java.lang.String.class;
							if ("item".equals(name) || "xml".equals(name) || !firstUpper) {
								super.startNode(name);
							} else {
								super.startNode(name.substring(0, 1).toUpperCase().concat(name.substring(1)));
							}
						}
						protected void writeText(QuickWriter writer, String text) {
							if (cdata && need) {
								writer.write("<![CDATA[");
								writer.write(text);
								writer.write("]]>");
							} else {
								writer.write(text);
							}
						}
					};
				}
			};
		}
		return xppDriver1;
	}

}
