package com.adopapa.wechatapi.util;

import java.text.MessageFormat;
import java.util.Date;

import javax.servlet.http.HttpUtils;

public class ApiUrls {

	/**
	 * 获取accessToken
	 */
	public final static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";
	/**
	 * 获取用户基本信息
	 */
	public final static String GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}&lang={2}";

	/**
	 * 上传多媒体文件
	 */
	public final static String UPLOAD_MEDIA_FILE_URL = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token={0}&type={1}";
	/**
	 * 下载多媒体问题
	 */
	public final static String GET_MEDIA_FILE_URL = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={0}&media_id={1}";
	/**
	 * 创建菜单
	 */
	public final static String MENU_CREATE_URL = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}";
	/**
	 * 查询菜单
	 */
	public final static String MENU_GET_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}";
	/**
	 * 删除菜单
	 */
	public final static String MENU_DELETE_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token={0}";
	/**
	 * 更新用户备注
	 */
	public final static String UPDATE_REMARK = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token={0}";
	/**
	 * 获取code
	 */
	public final static String SCOPE_CODE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type={2}&scope={3}&state={4}#wechat_redirect";
	/**
	 * 获取网页accessToken
	 */
	public final static String WEB_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type={3}";
	/**
	 * 刷新accessToken
	 */
	public final static String REFRESH_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={0}&grant_type={1}&refresh_token={2}";
	/**
	 * 获取web用户信息
	 */
	public final static String GET_WEB_USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token={0}&openid={1}&lang={2}";
	/**
	 * 检查accessToken是否过期
	 */
	public final static String CHECK_WEB_ACCESSTOKEN = "https://api.weixin.qq.com/sns/auth?access_token={0}&openid={1}";
	/**
	 * 创建二维码
	 */
	public final static String CREATE_QRCODE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}";
	/**
	 * 显示二维码
	 */
	public final static String SHOW_QRCODE_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}";
	/**
	 * 发送客服消息
	 */
	public final static String CUSTOMER_MESSAGE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}";
	/**
	 * 发送模板消息
	 */
	public final static String TEMPLATE_MESSAGE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";
	/**
	 * 第三方网站使用微信登录获取code appid 是 应用唯一标识 redirect_uri 是 重定向地址，需要进行UrlEncode
	 * response_type 是 填code scope 是 应用授权作用域，拥有多个作用域用逗号（,）分隔，网页应用目前仅填写snsapi_login即可
	 * state 否 用于保持请求和回调的状态，授权请求后原样带回给第三方
	 * 。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验
	 */
	public final static String SCOPE_QRCODE_URL = "https://open.weixin.qq.com/connect/qrconnect?appid={0}&redirect_uri={1}&response_type={2}&scope={3}&state={4}#wechat_redirect";

	/**
	 * 订阅消息用户授权路径
	 */
	public final static String SUBSCRIBE_MESSAGE_AUTH_URL = "https://mp.weixin.qq.com/mp/subscribemsg?action=get_confirm&appid=%s&scene=%s&template_id=%s&redirect_url=%s&reserved=%s#wechat_redirect";
	/**
	 * 发送订阅消息
	 */
	public final static String SEND_SUBSCRIBE_MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/subscribe?access_token=%s";

	/***********************************************
	 * 高级群发
	 ***********************************************************************************************/
	/**
	 * 上传图文素材
	 */
	public final static String UPLOAD_NEWS_URL = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token={0}";
	/**
	 * 上传视频素材
	 */
	public final static String UPLOAD_VIDEO_URL = "https://file.api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token={0}";
	/**
	 * 根据分组群发
	 */
	public final static String MASS_MESSAGE_SENDALL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={0}";
	/**
	 * 根据OpenID列表群发
	 */
	public final static String MASS_MESSAGE_SEND = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={0}";
	/********************************************************************************************************************************************************/

	/**
	 * 支付统一下单
	 */
	public final static String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	/**
	 * 申请退款
	 */
	public final static String REFUND_ORDER_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
	/**
	 * 关闭订单
	 */
	public final static String CLOSE_ORDER_URL = "https://api.mch.weixin.qq.com/pay/closeorder";
	/**
	 * 
	 * 获取jsapi_ticket
	 */
	public final static String GET_JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={0}&type=jsapi";
	/**
	 * 发送红包
	 */
	public final static String SEND_REDPACK_URL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";

	// ////////////////////////////////////////////////////////////////以上为服务号或者订阅号，以下为企业号/////////////////////////////////////////////////////////////////
	/**
	 * 获取accessToken
	 */
	public final static String EACCESS_TOKEN_URL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s";
	/**
	 * 创建部门
	 */
	public final static String DEPARTMENT_CREATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=%s";
	/**
	 * 更新部门
	 */
	public final static String DEPARTMENT_UPDATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=%s";
	/**
	 * 删除部门
	 */
	public final static String DEPARTMENT_DELETE_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=%s&id=%s";
	/**
	 * 获取部门列表
	 */
	public final static String DEPARTMENT_LIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=%s&id=%s";
	/**
	 * 创建成员
	 */
	public final static String MEMBER_CREATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=%s";
	/**
	 * 更新成员
	 */
	public final static String MEMBER_UPDATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=%s";
	/**
	 * 删除成员
	 */
	public final static String MEMBER_DELETE_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=%s&userid=%s";
	/**
	 * 批量更新成员
	 */
	public final static String MEMBER_BATCH_DELETE_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=%s";
	/**
	 * 获取成员
	 */
	public final static String MEMBER_GET_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=%s&userid=%s";
	/**
	 * 获取部门成员
	 */
	public final static String MEMBER_SIMPLE_LIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=%s&department_id=%s&fetch_child=%s&status=%s";
	/**
	 * 获取部门成员(详情)
	 */
	public final static String MEMBER_LIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=%s&department_id=%s&fetch_child=%s&status=%s";
	/**
	 * 创建标签
	 */
	public final static String CREATE_TAG_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token=%s";
	/**
	 * 更新标签名字
	 */
	public final static String UPDATE_TAG_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token=%s";
	/**
	 * 删除标签
	 */
	public final static String DELETE_TAG_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token=%s&tagid=%s";
	/**
	 * 获取标签成员
	 */
	public final static String GET_TAG_MEMBER_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token=%s&tagid=%s";
	/**
	 * 增加标签成员
	 */
	public final static String CREATE_TAG_MEMBER_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token=%s";
	/**
	 * 删除标签成员
	 */
	public final static String DELETE_TAG_MEMBER_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token=%s";
	/**
	 * 获取标签列表
	 */
	public final static String GET_TAG_LIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token=%s";

	/**
	 * 企业号主动发送消息
	 */
	public final static String SEND_ACTIVE_MESSAGE_URL = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s";
	/**
	 * 客服信息
	 */
	public final static String SEND_CUSTOMER_MESSAGE_URL = "https://qyapi.weixin.qq.com/cgi-bin/kf/send?access_token=%s";
	/**
	 * userid=>openId
	 */
	public final static String USERID_TO_OPENID_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=%s";
	/**
	 * openId=>userId
	 */
	public final static String OPENID_TO_USERID_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_userid?access_token=%s";
	/**
	 * 获取code
	 */
	public final static String ESCOPE_CODE_OAUTH_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=%s&scope=%s&state=%s#wechat_redirect";
	public final static String ESCOPE_CODE_SSO_URL = "https://open.work.weixin.qq.com/wwopen/sso/qrConnect?appid=%s&agentid=%s&redirect_uri=%s&state=%s";
	/**
	 * 获取userid
	 */
	public final static String GET_USERID_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=%s&code=%s";
	/**
	 * 获取jsapikey
	 */
	public final static String JSAPI_TICKET_URL = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=%s";
	/**
	 * 增量更新成员
	 */
	public final static String BATCH_SYNC_USER_URL = "https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser?access_token=%s";
	/**
	 * 全量覆盖成员
	 */
	public final static String BATCH_REPLACE_USER_URL = "https://qyapi.weixin.qq.com/cgi-bin/batch/replaceuser?access_token=%s";
	/**
	 * 全量覆盖部门
	 */
	public final static String BATCH_REPLACE_PARTY_URL = "https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty?access_token=%s";
	/**
	 * 获取异步任务结果
	 */
	public final static String BATCH_JOB_RESULT_URL = "https://qyapi.weixin.qq.com/cgi-bin/batch/getresult?access_token=%s&jobid=%s";
	/**
	 * 上传临时素材
	 */
	public final static String UPLOAD_TEMP_MEDIA_URL = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";
	/**
	 * 获取临时素材
	 */
	public final static String DOWNLOAD_TEMP_MEDIA_URL = "https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";

	/**
	 * 创建会话
	 */
	public final static String CEATE_CHAT_URL = "https://qyapi.weixin.qq.com/cgi-bin/appchat/create?access_token=%s";
	/**
	 * 获取会话
	 */
	public final static String GET_CHAT_URL = "https://qyapi.weixin.qq.com/cgi-bin/chat/get?access_token=%s&chatid=%s";
	/**
	 * 修改会话信息
	 */
	public final static String UPDATE_CHAT_URL = "https://qyapi.weixin.qq.com/cgi-bin/chat/update?access_token=%s";
	/**
	 * 退出会话
	 */
	public final static String QUIT_CHAT_URL = "https://qyapi.weixin.qq.com/cgi-bin/chat/quit?access_token=%s";
	/**
	 * 发送会话消息
	 */
	public final static String SEND_CHAT_URL = "https://qyapi.weixin.qq.com/cgi-bin/chat/send?access_token=%s";

	///////////////////////////////////////// 扫一扫
	/**
	 * 获取商户信息。使用该接口，商户可获取账号下的类目与号段等信息。
	 */
	public final static String MERCHANT_INFO_URL = "https://api.weixin.qq.com/scan/merchantinfo/get?access_token=%s";

	public static void main(String[] args) {
		// System.out.println(MessageFormat.format(ACCESS_TOKEN_URL, "1", "2"));
		//
		// String messageFormat = "lexical error at position {0}, encountered {1},
		// expected {2}";
		// System.out.println(MessageFormat.format(messageFormat, new Date(), 100,
		// 456));
		// String stringFormat = "lexical error at position %s, encountered %s, expected
		// %s ";
		// System.out.println(String.format(stringFormat, 123, 100, 456));
		//
		// String accessToken =
		// "qy7ofp2EgKBEti5ntIN8Ua0fA9zXsc17tSdLW223-lzd-n_FMgTWksMqGaNBfE8QgL_XNPCJ4RqhO_ylW7KAiWOWvk1K1Dd0SyvVPgf12Po";
		// System.out.println(MessageFormat.format(GET_USER_INFO, accessToken, "2",
		// "3"));

		String accessToken = "10_MDCLq7j3K1PWB4gLN7_uVDiecUaSKIG0xGy9fszzlqqtA3EGKuzDkcJN3jCrmx4csFnYachKEGX_Su0YFqiUXHdCjpEqzcCZAW0TKhtIi0dFENomR1A2vSIknFzmLhZJH1pjWJP1uJhLRm98UXOjAJAADU";
		String result = WeChatUtil.httpsRequest(String.format(MERCHANT_INFO_URL, accessToken), "GET", null);
		System.out.println(result);

	}

}
