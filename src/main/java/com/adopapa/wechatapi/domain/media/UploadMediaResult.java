package com.adopapa.wechatapi.domain.media;

import java.io.File;
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadMediaResult extends ResultMessage implements Serializable {

	private static final long serialVersionUID = -1085312917756318255L;

	// type
	// 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
	// media_id 媒体文件上传后，获取时的唯一标识
	// created_at 媒体文件上传时间戳

	public UploadMediaResult(int errcode, String errmsg) {
		super(errcode, errmsg);
	}

	public UploadMediaResult() {
		this(0, "ok");
	}

	private String type;
	@JsonProperty("media_id")
	private String mediaId;
	@JsonProperty("created_at")
	private String createAt;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	@Override
	public String toString() {
		return "UploadMediaResult [type=" + type + ", mediaId=" + mediaId + ", createAt=" + createAt + ", getErrcode()=" + getErrcode() + ", getErrmsg()=" + getErrmsg() + "]";
	}

	// public String toJson() throws IOException {
	// String json = JsonUtil.bean2Json(this);
	// json = json.replace("media_id", "mediaId").replace("created_at",
	// "createAt");
	// return json;
	// }

	public enum MediaType {

		image, voice, video, thumb,
		// 图文消息，用户群发接口
		mpnews,
		// 下面用于企业号file
		file,;
		
		public Long limitSize;

		// 公众账号上传的多媒体文件有格式和大小限制，如下：
		// 图片（image）: 2M，支持bmp/png/jpeg/jpg/gif格式
		// 语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
		// 视频（video）：10MB，支持MP4格式
		// 缩略图（thumb）：64KB，支持JPG格式
		// 媒体文件在后台保存时间为3天，即3天后media_id失效。
		// 企业号上传
		// 所有文件size必须大于5个字节
		// 图片（image）:2MB，支持JPG,PNG格式
		// 语音（voice）：2MB，播放长度不超过60s，支持AMR格式
		// 视频（video）：10MB，支持MP4格式
		// 普通文件（file）：20MB
		public Long getLimitSize() {
			switch (this) {
			case image:
				return 1024 * 1024L;
			case voice:
				return 2 * 1024 * 1024L;
			case video:
				return 10 * 1024 * 1024L;
			case thumb:
				return 64 * 1024L;
			case file:
				return 20 * 1024 * 1024L;
			default:
				return 0L;
			}
		}

		/**
		 * 判断上传的文件是否超过大小限制
		 */
		public boolean checkSizeLimit(File file) {
			Long size = getLimitSize();
			Long length = file.length();
			if (size >= length) {
				return true;
			}
			return false;
		}
	}

}
