package com.adopapa.wechatapi.domain.message;

import java.util.Map;

public class LinkRequestMessage extends RequestMessage {

	/**
	 * Title 消息标题 Description 消息描述 Url 消息链接
	 */
	private String title;
	private String description;
	private String url;

	public LinkRequestMessage() {
		setMsgType(MessageType.link.name());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>1351776360</CreateTime>
	 * <MsgType><![CDATA[link]]></MsgType>
	 * <Title><![CDATA[公众平台官网链接]]></Title>
	 * <Description><![CDATA[公众平台官网链接]]></Description>
	 * <Url><![CDATA[url]]></Url> <MsgId>1234567890123456</MsgId> </xml>
	 * @throws Exception 
	 */
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setFromUserName(map.get("FromUserName"));
		setToUserName(map.get("ToUserName"));
		setMsgId(Long.parseLong(map.get("MsgId")));
		setTitle(map.get("Title"));
		setDescription(map.get("Description"));
		setUrl(map.get("Url"));
	}

	@Override
	public String toString() {
		return "LinkRequestMessage [title=" + title + ", description=" + description + ", url=" + url + ", getToUserName()=" + getToUserName()
				+ ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType()
				+ ", getMsgId()=" + getMsgId() + "]";
	}
	
	
	

}
