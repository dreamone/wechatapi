package com.adopapa.wechatapi.domain.message.customer;

import javax.persistence.MappedSuperclass;

import com.adopapa.wechatapi.domain.message.Image;
import com.adopapa.wechatapi.domain.message.Music;
import com.adopapa.wechatapi.domain.message.Video;
import com.adopapa.wechatapi.domain.message.Voice;

@MappedSuperclass
public class CustomerServiceMessage {
	
//	access_token	是	调用接口凭证
//	touser	是	普通用户openid
//	msgtype	是	消息类型，文本为text，图片为image，语音为voice，视频消息为video，音乐消息为music，图文消息为news
//	content	是	文本消息内容
//	media_id	是	发送的图片/语音/视频的媒体ID
//	thumb_media_id	是	缩略图的媒体ID
//	title	否	图文消息/视频消息/音乐消息的标题
//	description	否	图文消息/视频消息/音乐消息的描述
//	musicurl	是	音乐链接
//	hqmusicurl	是	高品质音乐链接，wifi环境优先使用该链接播放音乐
//	url	否	图文消息被点击后跳转的链接
//	picurl	否	图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80

	private String toUser;

	private String msgType;

	private Text text;

	private Image image;

	private Music music;

	private Video video;

	private Voice voice;

	private News news;

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public Voice getVoice() {
		return voice;
	}

	public void setVoice(Voice voice) {
		this.voice = voice;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public String toString() {
		return "CustomerServiceMessage [toUser=" + toUser + ", msgType=" + msgType + ", text=" + text + ", image=" + image + ", music=" + music
				+ ", video=" + video + ", voice=" + voice + ", news=" + news + "]";
	}
	
}

//发送文本消息
//
//{
//    "touser":"OPENID",
//    "msgtype":"text",
//    "text":
//    {
//         "content":"Hello World"
//    }
//}
//发送图片消息
//
//{
//    "touser":"OPENID",
//    "msgtype":"image",
//    "image":
//    {
//      "media_id":"MEDIA_ID"
//    }
//}
//发送语音消息
//
//{
//    "touser":"OPENID",
//    "msgtype":"voice",
//    "voice":
//    {
//      "media_id":"MEDIA_ID"
//    }
//}
//发送视频消息
//
//{
//    "touser":"OPENID",
//    "msgtype":"video",
//    "video":
//    {
//      "media_id":"MEDIA_ID",
//      "thumb_media_id":"MEDIA_ID",
//      "title":"TITLE",
//      "description":"DESCRIPTION"
//    }
//}
//发送音乐消息
//
//{
//    "touser":"OPENID",
//    "msgtype":"music",
//    "music":
//    {
//      "title":"MUSIC_TITLE",
//      "description":"MUSIC_DESCRIPTION",
//      "musicurl":"MUSIC_URL",
//      "hqmusicurl":"HQ_MUSIC_URL",
//      "thumb_media_id":"THUMB_MEDIA_ID" 
//    }
//}
//发送图文消息 图文消息条数限制在10条以内，注意，如果图文数超过10，则将会无响应。
//
//{
//    "touser":"OPENID",
//    "msgtype":"news",
//    "news":{
//        "articles": [
//         {
//             "title":"Happy Day",
//             "description":"Is Really A Happy Day",
//             "url":"URL",
//             "picurl":"PIC_URL"
//         },
//         {
//             "title":"Happy Day",
//             "description":"Is Really A Happy Day",
//             "url":"URL",
//             "picurl":"PIC_URL"
//         }
//         ]
//    }
//}
