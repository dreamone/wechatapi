package com.adopapa.wechatapi.domain.message;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class VoiceResponseMessage extends ResponseMessage {

	private Voice voice;

	public VoiceResponseMessage() {
		setMsgType(MessageType.voice.name());
	}

	public Voice getVoice() {
		return voice;
	}

	public void setVoice(Voice voice) {
		this.voice = voice;
	}

	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(true));
		xstream.alias("xml", this.getClass());
		return xstream.toXML(this);
	}
}
