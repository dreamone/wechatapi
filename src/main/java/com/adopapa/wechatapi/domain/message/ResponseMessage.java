package com.adopapa.wechatapi.domain.message;

import javax.persistence.MappedSuperclass;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * 被动回复用户消息，针对开发者是响应
 * 开发者--->OpenID
 * 对于每一个POST请求，开发者在响应包（Get）中返回特定XML结构，对该消息进行响应（现支持回复文本、图片、
 * 图文、语音、视频、音乐）。请注意，回复图片等多媒体消息时需要预先上传多媒体文件到微信服务器，
 * 只支持认证服务号
 */
@MappedSuperclass
public abstract class ResponseMessage {

	@XStreamAlias("ToUserName")
	// 接收方帐号（收到的OpenID）
	private String toUserName;
	// 开发者微信号
	private String fromUserName;
	// 消息创建时间 （整型）
	private long createTime;
	// 消息类型（text/music/news）
	private String msgType;
	// 位0x0001被标志时，星标刚收到的消息
	private int flag;
	
	

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	protected void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}
	
	
	public abstract String toXml();
	
	public enum MessageType {
		text, image, voice, video, music, news, transfer_customer_service
		;
	}

}
