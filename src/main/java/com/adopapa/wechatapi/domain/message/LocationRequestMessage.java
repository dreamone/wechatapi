package com.adopapa.wechatapi.domain.message;

import java.util.Map;

public class LocationRequestMessage extends RequestMessage {

	/**
	 * Location_X 地理位置维度 Location_Y 地理位置经度 Scale 地图缩放大小 Label 地理位置信息 Event
	 */

	private double locationX;
	private double locationY;
	private String label;
	private double scale;

	public LocationRequestMessage() {
		setMsgType(MessageType.location.name());
	}

	public double getLocationX() {
		return locationX;
	}

	public void setLocationX(double locationX) {
		this.locationX = locationX;
	}

	public double getLocationY() {
		return locationY;
	}

	public void setLocationY(double locationY) {
		this.locationY = locationY;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>1351776360</CreateTime> <MsgType><![CDATA[location]]></MsgType>
	 * <Location_X>23.134521</Location_X> <Location_Y>113.358803</Location_Y>
	 * <Scale>20</Scale> <Label><![CDATA[位置信息]]></Label>
	 * <MsgId>1234567890123456</MsgId> </xml>
	 */
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setFromUserName(map.get("FromUserName"));
		setToUserName(map.get("ToUserName"));
		setMsgId(Long.parseLong(map.get("MsgId")));
		setLabel(map.get("Label"));
		setScale(Double.parseDouble(map.get("Scale")));
		setLocationX(Double.parseDouble(map.get("Location_X")));
		setLocationY(Double.parseDouble(map.get("Location_Y")));
	}

	@Override
	public String toString() {
		return "LocationRequestMessage [locationX=" + locationX + ", locationY=" + locationY + ", label=" + label
				+ ", scale=" + scale + ", getToUserName()=" + getToUserName() + ", getFromUserName()="
				+ getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType()
				+ ", getMsgId()=" + getMsgId() + "]";
	}

}
