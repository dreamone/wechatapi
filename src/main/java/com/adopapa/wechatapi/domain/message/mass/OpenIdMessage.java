package com.adopapa.wechatapi.domain.message.mass;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.adopapa.wechatapi.domain.message.mass.MpMedia.MessageType;
import com.adopapa.wechatapi.util.JsonUtil;

public class OpenIdMessage {

	private List<String> toUsers = new LinkedList<String>();

	private MpMedia mpMedia;

	private String msgType;

	public List<String> getToUsers() {
		return toUsers;
	}

	public void setToUsers(List<String> toUsers) {
		this.toUsers = toUsers;
	}

	public MpMedia getMpMedia() {
		return mpMedia;
	}

	public void setMpMedia(MpMedia mpMedia) {
		this.mpMedia = mpMedia;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
	
	public String toJson() throws IOException {
		String json = JsonUtil.bean2Json(this);
		json = json.replaceAll("msgType", "msgtype").replaceAll("mediaId", "media_id").replace("toUsers", "touser");
		if (MessageType.mpnews.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "mpnews");
		}else if (MessageType.text.name().equalsIgnoreCase(msgType)) {
		}else if (MessageType.image.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "image");
		}else if (MessageType.voice.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "voice");
		}else if (MessageType.music.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "music");
		}else if (MessageType.mpvideo.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "mpvideo");
		}
		return json;
	}
	
	
	public static void main(String[] args) throws IOException {
		OpenIdMessage message = new OpenIdMessage();
		message.getToUsers().add("1111");
		message.getToUsers().add("2222");
		message.getToUsers().add("3333");
		message.getToUsers().add("4444");
		message.getToUsers().add("5555");
//		message.setText(new Text("111111"));
//		message.setMsgType(MessageType.text.name());
//		message.setMsgType(MessageType.mpnews.name());
//		message.setMsgType(MessageType.image.name());
//		message.setMsgType(MessageType.video.name());
		message.setMsgType(MessageType.voice.name());
//		message.setMsgType(MessageType.music.name());
		message.setMpMedia(new MpMedia("11111111111111"));
		System.out.println(message.toJson());
	}

}

// 图文消息（注意图文消息的media_id需要通过上述方法来得到）：
// {
// "touser":[
// "OPENID1",
// "OPENID2"
// ],
// "mpnews":{
// "media_id":"123dsdajkasd231jhksad"
// },
// "msgtype":"mpnews"
// }
// 文本：
// {
// "touser":[
// "OPENID1",
// "OPENID2"
// ],
// "msgtype": "text",
// "text": { "content": "hello from boxer."}
// }
// 语音：
// {
// "touser":[
// "OPENID1",
// "OPENID2"
// ],
// "voice":{
// "media_id":"mLxl6paC7z2Tl-NJT64yzJve8T9c8u9K2x-Ai6Ujd4lIH9IBuF6-2r66mamn_gIT"
// },
// "msgtype":"voice"
// }
// 图片：
// {
// "touser":[
// "OPENID1",
// "OPENID2"
// ],
// "image":{
// "media_id":"BTgN0opcW3Y5zV_ZebbsD3NFKRWf6cb7OPswPi9Q83fOJHK2P67dzxn11Cp7THat"
// },
// "msgtype":"image"
// 视频：
// {
// "touser":[
// "OPENID1",
// "OPENID2"
// ],
// "video":{
// "media_id":"123dsdajkasd231jhksad",
// "title":"TITLE",
// "description":"DESCRIPTION"
// },
// "msgtype":"video"
// }