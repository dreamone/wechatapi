package com.adopapa.wechatapi.domain.message.mass;

import java.io.IOException;

import javax.persistence.MappedSuperclass;

import com.adopapa.wechatapi.domain.message.customer.Text;
import com.adopapa.wechatapi.domain.message.mass.MpMedia.MessageType;
import com.adopapa.wechatapi.util.JsonUtil;

//filter	是	用于设定图文消息的接收者
//is_to_all	否	用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据group_id发送给指定群组的用户
//group_id	否	群发到的分组的group_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写group_id
//mpnews	是	用于设定即将发送的图文消息
//media_id	是	用于群发的消息的media_id
//msgtype	是	群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video
//title	否	消息的标题
//description	否	消息的描述
//thumb_media_id	是	视频缩略图的媒体ID
@MappedSuperclass
public class GroupMessage {

	private Filter filter;

	private String msgType;

	private Text text;

	private MpMedia mpMedia;

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public MpMedia getMpMedia() {
		return mpMedia;
	}

	public void setMpMedia(MpMedia mpMedia) {
		this.mpMedia = mpMedia;
	}

	
	public String toJson() throws IOException {
		String json = JsonUtil.bean2Json(this);
		json = json.replaceAll("msgType", "msgtype").replaceAll("isToAll", "is_to_all").replaceAll("groupId", "group_id").replaceAll("mediaId", "media_id");
		if (MessageType.mpnews.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "mpnews");
		}else if (MessageType.text.name().equalsIgnoreCase(msgType)) {
		}else if (MessageType.image.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "image");
		}else if (MessageType.voice.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "voice");
		}else if (MessageType.music.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "music");
		}else if (MessageType.mpvideo.name().equalsIgnoreCase(msgType)) {
			json = json.replaceAll("mpMedia", "mpvideo");
		}
		return json;
	}
	
	public class Filter {
		private boolean isToAll;
		private String groupId;

		public Filter() {
			
		}
		public Filter(boolean isToAll, String groupId) {
			this.isToAll = isToAll;
			this.groupId = groupId;
		}
		public boolean isToAll() {
			return isToAll;
		}
		public void setToAll(boolean isToAll) {
			this.isToAll = isToAll;
		}
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
	}
	
	public static void main(String[] args) throws IOException {
		GroupMessage message = new GroupMessage();
		message.setFilter(message.new Filter(false, "1"));
//		message.setText(new Text("111111"));
//		message.setMsgType(MessageType.text.name());
//		message.setMsgType(MessageType.mpnews.name());
//		message.setMsgType(MessageType.image.name());
//		message.setMsgType(MessageType.video.name());
		message.setMsgType(MessageType.voice.name());
//		message.setMsgType(MessageType.music.name());
		message.setMpMedia(new MpMedia("11111111111111"));
		System.out.println(message.toJson());
	}
}

// 图文消息（注意图文消息的media_id需要通过上述方法来得到）：
// {
// "filter":{
// "is_to_all":false
// "group_id":"2"
// },
// "mpnews":{
// "media_id":"123dsdajkasd231jhksad"
// },
// "msgtype":"mpnews"
// }
// 文本：
//
// {
// "filter":{
// "is_to_all":false
// "group_id":"2"
// },
// "text":{
// "content":"CONTENT"
// },
// "msgtype":"text"
// }
// 语音（注意此处media_id需通过基础支持中的上传下载多媒体文件来得到）：
//
// {
// "filter":{
// "is_to_all":false
// "group_id":"2"
// },
// "voice":{
// "media_id":"123dsdajkasd231jhksad"
// },
// "msgtype":"voice"
// }
// 图片（注意此处media_id需通过基础支持中的上传下载多媒体文件来得到）：
//
// {
// "filter":{
// "is_to_all":false
// "group_id":"2"
// },
// "image":{
// "media_id":"123dsdajkasd231jhksad"
// },
// "msgtype":"image"
// }
// 视频
// {
// "filter":{
// "is_to_all":false
// "group_id":"2"
// },
// "mpvideo":{
// "media_id":"IhdaAQXuvJtGzwwc0abfXnzeezfO0NgPK6AQYShD8RQYMTtfzbLdBIQkQziv2XJc",
// },
// "msgtype":"mpvideo"
// }