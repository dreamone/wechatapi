package com.adopapa.wechatapi.domain.message;

public class Image  {
	
	/**
	 * 图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 * 通过上传多媒体文件，得到的id。
	 */
	private String mediaId; 
	
	/**
	 * for request
	 * 图片链接
	 */
	private String picUrl;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}  
	
	
	

}
