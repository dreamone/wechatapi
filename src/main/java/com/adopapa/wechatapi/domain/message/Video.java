package com.adopapa.wechatapi.domain.message;

public class Video {

	/**
	 * 是 通过上传多媒体文件，得到的id 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	private String mediaId;

	/**
	 * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。 for requset
	 */
	private String ThumbMediaId;

	/**
	 * for response
	 */
	private String title; // 否视频消息的标题
	private String description; // 否 视频消息的描述

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getThumbMediaId() {
		return ThumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
