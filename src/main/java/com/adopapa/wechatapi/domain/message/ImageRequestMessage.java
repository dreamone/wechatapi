package com.adopapa.wechatapi.domain.message;

import java.util.Map;

public class ImageRequestMessage extends RequestMessage {
	/**
	 * PicUrl 图片链接 MediaId 图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */

	private String picUrl;
	private String mediaId;

	public ImageRequestMessage() {
		setMsgType(MessageType.image.name());
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>1348831860</CreateTime>
	 * <MsgType><![CDATA[image]]></MsgType> <PicUrl><![CDATA[this is a
	 * url]]></PicUrl> <MediaId><![CDATA[media_id]]></MediaId>
	 * <MsgId>1234567890123456</MsgId> </xml>
	 */
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setFromUserName(map.get("FromUserName"));
		setToUserName(map.get("ToUserName"));
		setMsgId(Long.parseLong(map.get("MsgId")));
		setPicUrl(map.get("PicUrl"));
		setMediaId(map.get("MediaId"));
	}

	@Override
	public String toString() {
		return "ImageRequestMessage [picUrl=" + picUrl + ", mediaId=" + mediaId + ", getToUserName()=" + getToUserName() + ", getFromUserName()="
				+ getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType() + ", getMsgId()=" + getMsgId() + "]";
	}
	
	

}
