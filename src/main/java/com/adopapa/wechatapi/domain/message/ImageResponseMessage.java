package com.adopapa.wechatapi.domain.message;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class ImageResponseMessage extends ResponseMessage {
	
	private Image image;
	
	public ImageResponseMessage() {
		setMsgType(MessageType.image.name());
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(true));
		xstream.alias("xml", this.getClass());
		return xstream.toXML(this);
	}

	
	

}
