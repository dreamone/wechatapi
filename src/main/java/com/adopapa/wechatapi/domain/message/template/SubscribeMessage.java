package com.adopapa.wechatapi.domain.message.template;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import com.adopapa.wechatapi.util.JsonUtil;

/**
 * touser 是 填接收消息的用户openid template_id 是 订阅消息模板ID url 否 点击消息跳转的链接，需要有ICP备案
 * miniprogram 否 跳小程序所需数据，不需跳小程序可不用传该数据 appid 是
 * 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，并且小程序要求是已发布的） pagepath 是
 * 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar） scene 是 订阅场景值 title 是 消息标题，15字以内 data
 * 是 消息正文，value为消息内容文本（200字以内），没有固定格式，可用\n换行，color为整段消息内容的字体颜色（目前仅支持整段消息为一种颜色）
 */

public class SubscribeMessage {

	private String receiver;
	private String templateId;
	private String link;
	private String miniAppId;
	private String pagePath;
	private String scene;
	private String title;
	private String content;
	private String color;

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getMiniAppId() {
		return miniAppId;
	}

	public void setMiniAppId(String miniAppId) {
		this.miniAppId = miniAppId;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * { “touser”:”OPENID”, “template_id”:”TEMPLATE_ID”, “url”:”URL”,
	 * “miniprogram”:{ “appid”:“xiaochengxuappid12345”, “pagepath”:“index?foo=bar”
	 * }, “scene”:”SCENE”, “title”:”TITLE”, “data”:{ “content”:{ “value”:”VALUE”,
	 * “color”:”COLOR” } } }
	 */
	public String getJsonString() {
		ObjectNode rootNode = JsonUtil.objectMapper.createObjectNode();
		rootNode.put("touser", receiver);
		rootNode.put("template_id", templateId);
		if (link != null) {
			rootNode.put("url", link);
		}
		if (miniAppId != null && pagePath != null) {
			ObjectNode miniAppNode = JsonUtil.objectMapper.createObjectNode();
			miniAppNode.put("appid", miniAppId);
			miniAppNode.put("pagepath", pagePath);
			rootNode.put("miniprogram", miniAppNode);
		}
		rootNode.put("scene", scene);
		rootNode.put("title", title);
		
		ObjectNode dataNode = JsonUtil.objectMapper.createObjectNode();
		ObjectNode contentNode = JsonUtil.objectMapper.createObjectNode();
		contentNode.put("value", content);
		contentNode.put("color", color);
		dataNode.put("content", contentNode);
		rootNode.put("data", dataNode);
		
		return rootNode.toString();

	}

}
