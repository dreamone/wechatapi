package com.adopapa.wechatapi.domain.message;

import java.util.Map;

public class VoiceRequestMessage extends RequestMessage {
	/**
	 * MediaId 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。 Format 语音格式，如amr，speex等
	 */
	private String mediaId;
	private String format;
	
	/**
	 * <Recognition><![CDATA[腾讯微信团队]]></Recognition>
	 * Recognition	语音识别结果，UTF8编码
	 */
	private String recognition;

	public VoiceRequestMessage() {
		setMsgType(MessageType.voice.name());
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
	

	public String getRecognition() {
		return recognition;
	}

	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>1357290913</CreateTime>
	 * <MsgType><![CDATA[voice]]></MsgType>
	 * <MediaId><![CDATA[media_id]]></MediaId>
	 * <Format><![CDATA[Format]]></Format> <MsgId>1234567890123456</MsgId>
	 * </xml>
	 */
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setFromUserName(map.get("FromUserName"));
		setToUserName(map.get("ToUserName"));
		setMsgId(Long.parseLong(map.get("MsgId")));
		setMediaId(map.get("MediaId"));
		setFormat(map.get("Format"));
		setRecognition(map.get("Recognition"));
	}

	@Override
	public String toString() {
		return "VoiceRequestMessage [mediaId=" + mediaId + ", format=" + format + ", recognition=" + recognition + ", getToUserName()="
				+ getToUserName() + ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()="
				+ getMsgType() + ", getMsgId()=" + getMsgId() + "]";
	}
	
	

}
