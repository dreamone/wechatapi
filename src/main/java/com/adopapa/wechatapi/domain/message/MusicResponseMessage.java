package com.adopapa.wechatapi.domain.message;

import java.util.Calendar;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class MusicResponseMessage extends ResponseMessage {

	private Music music;

	public MusicResponseMessage() {
		setMsgType(MessageType.music.name());
	}
	
	

	public Music getMusic() {
		return music;
	}



	public void setMusic(Music music) {
		this.music = music;
	}



	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(false));
		xstream.alias("xml", this.getClass());
		return xstream.toXML(this);
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>12345678</CreateTime>
	 * <MsgType><![CDATA[music]]></MsgType> <Music>
	 * <Title><![CDATA[TITLE]]></Title>
	 * <Description><![CDATA[DESCRIPTION]]></Description>
	 * <MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>
	 * <HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>
	 * <ThumbMediaId><![CDATA[media_id]]></ThumbMediaId> </Music> </xml>
	 */
//	@Override
//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		sb.append("<xml>");
//		sb.append("<ToUserName><![CDATA[").append(getToUserName()).append("]]></ToUserName>");
//		sb.append("<FromUserName><![CDATA[").append(getFromUserName()).append("]]></FromUserName>");
//		sb.append("<CreateTime>").append(getCreateTime()).append("</CreateTime>");
//		sb.append("<MsgType><![CDATA[").append(getMsgType()).append("]]></MsgType>");
//		sb.append("<Music>");
//		sb.append("<Title><![CDATA[").append(getMusic().getTitle()).append("]]></Title>");
//		sb.append("<Description><![CDATA[").append(getMusic().getDescription()).append("]]></Description>");
//		sb.append("<MusicUrl><![CDATA[").append(getMusic().getMusicUrl()).append("]]></MusicUrl>");
//		sb.append("<HQMusicUrl><![CDATA[").append(getMusic().getHqMusicUrl()).append("]]></HQMusicUrl>");
//		sb.append("<ThumbMediaId><![CDATA[").append(getMusic()).append("]]></ThumbMediaId>");
//		sb.append("</Music>");
//		sb.append("</xml>");
//		return sb.toString();
//	}
	
	public static void main(String[] args) {
		MusicResponseMessage message = new MusicResponseMessage();
		message.setCreateTime(Calendar.getInstance().getTime().getTime());
		message.setFromUserName("1111");
		message.setToUserName("222222");
		Music music = new Music();
		music.setDescription("kjakjfkajfkadlfkjajfka;jfkd;ajf");
		music.setHqMusicUrl("jkfjkjkjkjkj");
		music.setMusicUrl("jkjkjkfajfk");
		music.setThumbMediaId("999999999999999999999999999");
		music.setTitle("music");
		message.setMusic(music);
		System.out.println(message.toString());
		System.err.println(message.toXml());
	}

}
