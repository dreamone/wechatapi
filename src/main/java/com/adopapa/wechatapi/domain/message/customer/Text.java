package com.adopapa.wechatapi.domain.message.customer;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Text {

	private String content;

	public Text(String content) {
		setContent(content);
	}

	public Text() {

	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Text [content=" + content + "]";
	}

}
