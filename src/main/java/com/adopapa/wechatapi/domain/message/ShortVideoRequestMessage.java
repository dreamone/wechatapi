package com.adopapa.wechatapi.domain.message;

public class ShortVideoRequestMessage extends VideoRequestMessage {
	
	public ShortVideoRequestMessage() {
		setMsgType(MessageType.shortvideo.name());
	}

}
