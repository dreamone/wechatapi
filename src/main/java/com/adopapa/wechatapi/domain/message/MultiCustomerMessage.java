package com.adopapa.wechatapi.domain.message;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class MultiCustomerMessage extends ResponseMessage {


	public MultiCustomerMessage() {
		setMsgType(MessageType.transfer_customer_service.name());
	}

	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(true));
		xstream.alias("xml", this.getClass());
		return xstream.toXML(this);
	}

}
