package com.adopapa.wechatapi.domain.message.template;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.adopapa.wechatapi.application.service.WeChatApi;
import com.adopapa.wechatapi.domain.common.ResultMessage;
import com.adopapa.wechatapi.domain.token.AccessToken;
import com.adopapa.wechatapi.util.JsonUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateMessage {
	private String toUser;
	private String templateId;
	private String url;
	@JsonIgnore
	private String topColor;
	private Map<String, TemplateData> data = new LinkedHashMap<String, TemplateData>();

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTopColor() {
		return topColor;
	}

	public void setTopColor(String topColor) {
		this.topColor = topColor;
	}

	public Map<String, TemplateData> getData() {
		return data;
	}

	public void setData(Map<String, TemplateData> data) {
		this.data = data;
	}

	public String toJson() throws IOException {
		String json = JsonUtil.bean2Json(this);
		json = json.replaceAll("toUser", "touser").replaceAll("templateId", "template_id").replaceAll("topColor", "topcolor");
		return json;
	}

	public static void main(String[] args) throws IOException {
//		TemplateData first = new TemplateData("您撤消悬赏求购: 北京交货；螺纹钢 12 HRB400或不限；产厂不限；仓库不限；需求8.000件；悬赏金10.00元，该信息当日有效，已释放悬赏金10.00元。！", "#173177");
//		TemplateData keynote1 = new TemplateData("系统消息", "#173177");
//		TemplateData keynote2 = new TemplateData("新锐钢铁", "#173177");
//		TemplateData keynote3 = new TemplateData("悬赏采购", "#173177");
//		TemplateData keynote4 = new TemplateData("2018年9月16日", "#173177");
//		TemplateData remark = new TemplateData("一点钢-我的钢贸管家！", "#173177");
		TemplateData first = new TemplateData("您撤消悬赏求购: 北京交货；螺纹钢 12 HRB400或不限；产厂不限；仓库不限；需求8.000件；悬赏金10.00元，该信息当日有效，已释放悬赏金10.00元。！", "#173177");
		TemplateData keynote1 = new TemplateData("系统消息", "#173177");
		TemplateData keynote2 = new TemplateData("已成功", "#173177");
		TemplateData keynote3 = new TemplateData("2018年9月16日", "#173177");
		TemplateData keynote4 = new TemplateData("一点钢网", "#173177");
		TemplateData remark = new TemplateData("一点钢-我的钢贸管家！", "#173177");

		TemplateMessage templateMessage = new TemplateMessage();
		templateMessage.setToUser("okkwgt3rADJlkQnV4UGsrf4zGFno");
		templateMessage.setToUser("okkwgtx_Y-eo2JDA5VTLpKC-D0NU");
		templateMessage.setTemplateId("wL2qqgUxLbo7tE-JZNK5wuHDdbkUC8hk1UdXox8Mzj8");
		templateMessage.setTemplateId("kMdWgA2n7xupkd0Z4AOkz6u-R-SGLuTBGMerpYRB-Pc");
		templateMessage.setTopColor("#173177");
		templateMessage.setUrl("http://www.yidiangang.com");
		templateMessage.getData().put("first", first);
		templateMessage.getData().put("keyword1", keynote1);
		templateMessage.getData().put("keyword2", keynote2);
		templateMessage.getData().put("keyword3", keynote3);
		templateMessage.getData().put("keyword4", keynote4);
		templateMessage.getData().put("remark",remark);

		System.out.println(templateMessage.toJson());
		
		String appId = "wxb30952334a28c368";
		String appSecret = "82a3a93271b370638de9e3c3b267da27";
//		AccessToken accessToken = WeChatApi.getAccessToken(appId, appSecret);
//		System.out.println(accessToken);
//		ResultMessage message = WeChatApi.sendTemplateMessage(accessToken.getToken(), templateMessage);
		String token = "16_ITqst4691-yrhde4P_qA9neDfr5rMySoAtMAJgssbO4gdN3C6iY_QZfpIlvnpJDIJR1X7Ha4wC0UHNNRt9A2YeEl3ISR33-ZZ-u5QRm0xm3cZ4GamClmEIYvzp1SN22zFShQcizgI7TltWTsNEMjAGATKS";
		ResultMessage message = WeChatApi.sendTemplateMessage(token, templateMessage);
		System.out.println(message);
	}

}
// {
// "touser":"OPENID",
// "template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
// "url":"http://weixin.qq.com/download",
// "topcolor":"#FF0000",
// "data":{
// "first": {
// "value":"恭喜你购买成功！",
// "color":"#173177"
// },
// "keynote1":{
// "value":"巧克力",
// "color":"#173177"
// },
// "keynote2": {
// "value":"39.8元",
// "color":"#173177"
// },
// "keynote3": {
// "value":"2014年9月16日",
// "color":"#173177"
// },
// "remark":{
// "value":"欢迎再次购买！",
// "color":"#173177"
// }
// }
// }