package com.adopapa.wechatapi.domain.message;

import java.util.Map;

import javax.persistence.MappedSuperclass;

/**
 * 接收普通消息，针对开发者是请求 OpenID--->开发者 当普通微信用户向公众账号发消息时，
 * 微信服务器将POST消息的XML数据包到开发者填写的URL上
 */
@MappedSuperclass
public abstract class RequestMessage {

	private String toUserName; // 开发者微信号
	private String fromUserName;// 发送方帐号（一个OpenID）
	private long createTime; // 消息创建时间 （整型）
	private String msgType;// text
	private long msgId; // 消息id，64位整型

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public abstract void fromMap(Map<String, String> map ) throws Exception;

	public enum MessageType {
		text, image, voice, video, 
		location, link, event, shortvideo,
		;
	}

}
