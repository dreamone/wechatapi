package com.adopapa.wechatapi.domain.pay;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import com.adopapa.wechatapi.util.MessageUtil;

/**
 * 成功返回 <xml> <return_code><![CDATA[SUCCESS]]></return_code>
 * <return_msg><![CDATA[发放成功.]]></return_msg>
 * <result_code><![CDATA[SUCCESS]]></result_code>
 * <err_code><![CDATA[0]]></err_code>
 * <err_code_des><![CDATA[发放成功.]]></err_code_des>
 * <mch_billno><![CDATA[0010010404201411170000046545]]></mch_billno>
 * <mch_id>10010404</mch_id> <wxappid><![CDATA[wx6fa7e3bab7e15415]]></wxappid>
 * <re_openid><![CDATA[onqOjjmM1tad-3ROpncN-yUfa6uI]]></re_openid>
 * <total_amount>1</total_amount> </xml>
 *
 * 失败返回 <xml> <return_code><![CDATA[FAIL]]></return_code>
 * <return_msg><![CDATA[系统繁忙,请稍后再试.]]></return_msg>
 * <result_code><![CDATA[FAIL]]></result_code>
 * <err_code><![CDATA[268458547]]></err_code>
 * <err_code_des><![CDATA[系统繁忙,请稍后再试.]]></err_code_des>
 * <mch_billno><![CDATA[0010010404201411170000046542]]></mch_billno>
 * <mch_id>10010404</mch_id> <wxappid><![CDATA[wx6fa7e3bab7e15415]]></wxappid>
 * <re_openid><![CDATA[onqOjjmM1tad-3ROpncN-yUfa6uI]]></re_openid>
 * <total_amount>1</total_amount> </xml>
 * 
 * @author Dreamone
 *
 */
public class RedpacketResult implements Serializable {

	private static final long serialVersionUID = 7625992779102645444L;

	private String returnCode;
	private String returnMsg;

	private String resultCode;
	private String errCode;
	private String errCodeDes;

	private String mchBillno;
	private String mchId;
	private String wxappId;
	private String reOpenid;
	private int totalAmount;
	private String sendListid;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrCodeDes() {
		return errCodeDes;
	}

	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}

	public String getMchBillno() {
		return mchBillno;
	}

	public void setMchBillno(String mchBillno) {
		this.mchBillno = mchBillno;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getWxappId() {
		return wxappId;
	}

	public void setWxappId(String wxappId) {
		this.wxappId = wxappId;
	}

	public String getReOpenid() {
		return reOpenid;
	}

	public void setReOpenid(String reOpenid) {
		this.reOpenid = reOpenid;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSendListid() {
		return sendListid;
	}

	public void setSendListid(String sendListid) {
		this.sendListid = sendListid;
	}

	public void fromMap(Map<String, String> map) {
		this.setErrCode(map.get("err_code"));
		this.setErrCodeDes(map.get("err_code_des"));
		this.setMchBillno(map.get("mch_billno"));
		this.setMchId(map.get("mch_id"));
		this.setReOpenid(map.get("re_openid"));
		this.setResultCode(map.get("result_code"));
		this.setReturnMsg(map.get("return_msg"));
		this.setSendListid(map.get("send_listid"));
		this.setReturnCode(map.get("return_code"));
		this.setTotalAmount(Integer.parseInt(map.get("total_amount")));
		this.setWxappId(map.get("wxappid"));
	}

	@Override
	public String toString() {
		return "RedpackResult [returnCode=" + returnCode + ", returnMsg=" + returnMsg + ", resultCode=" + resultCode
				+ ", errCode=" + errCode + ", errCodeDes=" + errCodeDes + ", mchBillno=" + mchBillno + ", mchId="
				+ mchId + ", wxappId=" + wxappId + ", reOpenid=" + reOpenid + ", totalAmount=" + totalAmount
				+ ", sendListid=" + sendListid + "]";
	}

	public static void main(String[] args) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>");
		sb.append("<return_code><![CDATA[SUCCESS]]></return_code>");
		sb.append("<return_msg><![CDATA[发放成功.]]></return_msg>");
		sb.append("<result_code><![CDATA[SUCCESS]]></result_code>");
		sb.append("<err_code><![CDATA[0]]></err_code>");
		sb.append("<err_code_des><![CDATA[发放成功.]]></err_code_des>");
		sb.append("<mch_billno><![CDATA[0010010404201411170000046545]]></mch_billno>");
		sb.append("<mch_id>10010404</mch_id>");
		sb.append("<wxappid><![CDATA[wx6fa7e3bab7e15415]]></wxappid>");
		sb.append("<re_openid><![CDATA[onqOjjmM1tad-3ROpncN-yUfa6uI]]></re_openid>");
		sb.append("<total_amount>1</total_amount>");
		sb.append("<send_listid>100000000020150520314766074200</send_listid>");
		sb.append("</xml>");
		
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		Map<String, String> map = MessageUtil.parseXml(is);
		RedpacketResult redpackResult = new RedpacketResult();
		redpackResult.fromMap(map);
		
		System.out.println(redpackResult);
		
		sb = new StringBuilder();
		sb.append("<xml>");
		sb.append("<return_code><![CDATA[FAIL]]></return_code>");
		sb.append("<return_msg><![CDATA[系统繁忙,请稍后再试.]]></return_msg>");
		sb.append("<result_code><![CDATA[FAIL]]></result_code>");
		sb.append("<err_code><![CDATA[268458547]]></err_code>");
		sb.append("<err_code_des><![CDATA[系统繁忙,请稍后再试.]]></err_code_des>");
		sb.append("<mch_billno><![CDATA[0010010404201411170000046542]]></mch_billno>");
		sb.append("<mch_id>10010404</mch_id>");
		sb.append("<wxappid><![CDATA[wx6fa7e3bab7e15415]]></wxappid>");
		sb.append("<re_openid><![CDATA[onqOjjmM1tad-3ROpncN-yUfa6uI]]></re_openid>");
		sb.append("<total_amount>1</total_amount>");
		sb.append("</xml>");
		
		is = new ByteArrayInputStream(sb.toString().getBytes());
		map = MessageUtil.parseXml(is);
		redpackResult = new RedpacketResult();
		redpackResult.fromMap(map);
		
		System.out.println(redpackResult);
		
	}

}
