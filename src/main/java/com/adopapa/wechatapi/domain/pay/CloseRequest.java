package com.adopapa.wechatapi.domain.pay;

import java.io.Serializable;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

/**
 * 公众账号ID appid 是 String(32) wx8888888888888888 微信分配的公众账号ID（企业号corpid即为此appId）
 * 商户号 mch_id 是 String(32) 1900000109 微信支付分配的商户号 商户订单号 out_trade_no 是 String(32)
 * 1217752501201407033233368018 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@
 * ，且在同一个商户号下唯一。 随机字符串 nonce_str 是 String(32) 5K8264ILTKCH16CQ2502SI8ZNMTM67VS
 * 随机字符串，不长于32位。推荐随机数生成算法 签名 sign 是 String(32) C380BEC2BFD727A4B6845133519F3AD6
 * 签名，详见签名生成算法 签名类型 sign_type 否 String(32) HMAC-SHA256
 * 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
 *
 */
public class CloseRequest implements Serializable {

	private static final long serialVersionUID = 1303399723164254614L;

	private String appId;
	private String mechantId;
	private String outTradeNo;
	private String nonceStr;
	private String sign;
	private String signType;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}
	
	@Override
	public String toString() {
		return "CloseRequest [appId=" + appId + ", mechantId=" + mechantId + ", outTradeNo=" + outTradeNo + ", nonceStr=" + nonceStr + ", sign=" + sign + ", signType=" + signType + "]";
	}

	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(false));
		xstream.alias("xml", this.getClass());
		String xml = xstream.toXML(this);
		xml = xml.replaceAll("AppId", "appid").replaceAll("MechantId", "mch_id");
		xml = xml.replaceAll("Sign", "sign").replaceAll("signType", "sign_type");
		xml = xml.replaceAll("NonceStr", "nonce_str").replaceAll("OutTradeNo", "out_trade_no");
		return xml;
	}

	/**
	 * <xml> <appid>wx2421b1c4370ec43b</appid> <mch_id>10000100</mch_id>
	 * <nonce_str>4ca93f17ddf3443ceabf72f26d64fe0e</nonce_str>
	 * <out_trade_no>1415983244</out_trade_no>
	 * <sign>59FF1DF214B2D279A0EA7077C54DD95D</sign> </xml>
	 */

	public static void main(String[] args) {

		CloseRequest request = new CloseRequest();
		request.setAppId("wx2421b1c4370ec43b");
		request.setMechantId("10000100");
		request.setNonceStr("4ca93f17ddf3443ceabf72f26d64fe0e");
		request.setOutTradeNo("1415983244");
		request.setSign("59FF1DF214B2D279A0EA7077C54DD95D");
		request.setSignType("MD5");
		
		System.out.println(request.toXml());

	}

}
