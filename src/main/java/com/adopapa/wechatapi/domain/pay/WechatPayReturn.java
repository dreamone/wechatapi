package com.adopapa.wechatapi.domain.pay;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class WechatPayReturn {
//	<xml>
//	   <return_code><![CDATA[SUCCESS]]></return_code>
//	   <return_msg><![CDATA[OK]]></return_msg>
//	</xml> 

	private String returnCode;
	private String returnMessage;
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	
	@Override
	public String toString() {
		return "ReturnInfo [returnCode=" + returnCode + ", returnMessage=" + returnMessage + "]";
	}
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(true));
		xstream.alias("xml", this.getClass());
		String xml = xstream.toXML(this);
		xml = xml.replaceAll("ReturnCode", "return_code").replaceAll("ReturnMessage", "return_msg");
		return xml;
	}
	
	
}
