package com.adopapa.wechatapi.domain.pay;

import java.io.Serializable;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

/**
 * <xml> <appid>wx2421b1c4370ec43b</appid> <mch_id>10000100</mch_id>
 * <nonce_str>6cefdb308e1e2e8aabd48cf79e546a02</nonce_str>
 * <out_refund_no>1415701182</out_refund_no>
 * <out_trade_no>1415757673</out_trade_no> <refund_fee>1</refund_fee>
 * <total_fee>1</total_fee> <transaction_id></transaction_id>
 * <sign>FE56DD4AA85C0EECA82C35595A69E153</sign> </xml>
 *
 */
public class RefundRequest implements Serializable {

	private static final long serialVersionUID = 3704049535259945290L;

	private String appId;
	private String mechantId;
	private String nonceStr;
	private String sign;
	private String signType;
	private String transactionId;
	private String outTradeNo;
	private String outRefundNo;
	private String totalFee;
	private String refundFee;
	private String refundFeeType;
	private String refundDesc;
	private String refundAccount;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getRefundFee() {
		return refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}

	public String getRefundFeeType() {
		return refundFeeType;
	}

	public void setRefundFeeType(String refundFeeType) {
		this.refundFeeType = refundFeeType;
	}

	public String getRefundDesc() {
		return refundDesc;
	}

	public void setRefundDesc(String refundDesc) {
		this.refundDesc = refundDesc;
	}

	public String getRefundAccount() {
		return refundAccount;
	}

	public void setRefundAccount(String refundAccount) {
		this.refundAccount = refundAccount;
	}
	
	
	@Override
	public String toString() {
		return "RefundRequest [appId=" + appId + ", mechantId=" + mechantId + ", nonceStr=" + nonceStr + ", sign=" + sign + ", signType=" + signType + ", transactionId=" + transactionId
				+ ", outTradeNo=" + outTradeNo + ", outRefundNo=" + outRefundNo + ", totalFee=" + totalFee + ", refundFee=" + refundFee + ", refundFeeType=" + refundFeeType
				+ ", refundDesc=" + refundDesc + ", refundAccount=" + refundAccount + "]";
	}

	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(false));
		xstream.alias("xml", this.getClass());
		String xml = xstream.toXML(this);
		xml = xml.replaceAll("AppId", "appid").replaceAll("MechantId", "mch_id").replaceAll("TransactionId", "transaction_id");
		xml = xml.replaceAll("Sign", "sign").replaceAll("signType", "sign_type");
		xml = xml.replaceAll("NonceStr", "nonce_str").replaceAll("OutTradeNo", "out_trade_no").replaceAll("OutRefundNo", "out_refund_no");
		xml = xml.replaceAll("TotalFee", "total_fee").replaceAll("RefundFee", "refund_fee").replaceAll("RefundFeeType", "refund_fee_type");
		xml = xml.replaceAll("refundDesc", "refund_desc").replaceAll("RefundAccount", "refund_account");
		return xml;
	}
	
	/**
	 * <xml> <appid>wx2421b1c4370ec43b</appid> <mch_id>10000100</mch_id>
	 * <nonce_str>6cefdb308e1e2e8aabd48cf79e546a02</nonce_str>
	 * <out_refund_no>1415701182</out_refund_no>
	 * <out_trade_no>1415757673</out_trade_no> <refund_fee>1</refund_fee>
	 * <total_fee>1</total_fee> <transaction_id></transaction_id>
	 * <sign>FE56DD4AA85C0EECA82C35595A69E153</sign> </xml>
	 *
	 */
	
	public static void main(String[] args) {
		RefundRequest refundRequest = new RefundRequest();
		refundRequest.setAppId("wx2421b1c4370ec43b");
		refundRequest.setMechantId("10000100");
		refundRequest.setNonceStr("6cefdb308e1e2e8aabd48cf79e546a02");
		refundRequest.setOutRefundNo("1415701182");
		refundRequest.setOutTradeNo("1415757673");
		refundRequest.setTotalFee("1");
		refundRequest.setRefundFee("1");
		refundRequest.setTransactionId("1111");
		refundRequest.setSign("FE56DD4AA85C0EECA82C35595A69E153");
		refundRequest.setSignType("MD5");
		
		System.out.println(refundRequest.toXml());
		
	}

}

/**
 * 公众账号ID appid 是 String(32) wx8888888888888888 微信分配的公众账号ID（企业号corpid即为此appId）
 * 商户号 mch_id 是 String(32) 1900000109 微信支付分配的商户号 随机字符串 nonce_str 是 String(32)
 * 5K8264ILTKCH16CQ2502SI8ZNMTM67VS 随机字符串，不长于32位。推荐随机数生成算法 签名 sign 是 String(32)
 * C380BEC2BFD727A4B6845133519F3AD6 签名，详见签名生成算法 签名类型 sign_type 否 String(32)
 * HMAC-SHA256 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5 微信订单号 transaction_id 二选一
 * String(28) 1217752501201407033233368018 微信生成的订单号，在支付通知中有返回 商户订单号 out_trade_no
 * String(32) 1217752501201407033233368018 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@
 * ，且在同一个商户号下唯一。 商户退款单号 out_refund_no 是 String(64) 1217752501201407033233368018
 * 商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。 订单金额 total_fee 是 Int
 * 100 订单总金额，单位为分，只能为整数，详见支付金额 退款金额 refund_fee 是 Int 100
 * 退款总金额，订单总金额，单位为分，只能为整数，详见支付金额 货币种类 refund_fee_type 否 String(8) CNY 货币类型，符合ISO
 * 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型 退款原因 refund_desc 否 String(80) 商品已售完
 * 若商户传入，会在下发给用户的退款消息中体现退款原因 退款资金来源 refund_account 否 String(30)
 * REFUND_SOURCE_RECHARGE_FUNDS 仅针对老资金流商户使用
 * REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）
 * REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
 */
