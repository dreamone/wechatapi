package com.adopapa.wechatapi.domain.pay;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import com.adopapa.wechatapi.util.MessageUtil;

public class RefundResponse implements Serializable {

	private static final long serialVersionUID = 8844657779419449835L;

	private String returnCode;
	private String returnMessage;
	private String resultCode;
	private String errorCode;
	private String errorMessage;

	private String appId;
	private String mechantId;
	private String nonceStr;
	private String sign;
	private String transactionId;
	private String outTradeNo;
	private String outRefundNo;
	private String refundId;
	private String refundFee;
	private String settlementRefundFee;
	private String totalFee;
	private String settlementTotalFee;
	private String feeType;
	private String cashFee;
	private String cashFeeType;
	private String cashRefundFee;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getRefundId() {
		return refundId;
	}

	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	public String getRefundFee() {
		return refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}

	public String getSettlementRefundFee() {
		return settlementRefundFee;
	}

	public void setSettlementRefundFee(String settlementRefundFee) {
		this.settlementRefundFee = settlementRefundFee;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getSettlementTotalFee() {
		return settlementTotalFee;
	}

	public void setSettlementTotalFee(String settlementTotalFee) {
		this.settlementTotalFee = settlementTotalFee;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getCashFee() {
		return cashFee;
	}

	public void setCashFee(String cashFee) {
		this.cashFee = cashFee;
	}

	public String getCashFeeType() {
		return cashFeeType;
	}

	public void setCashFeeType(String cashFeeType) {
		this.cashFeeType = cashFeeType;
	}

	public String getCashRefundFee() {
		return cashRefundFee;
	}

	public void setCashRefundFee(String cashRefundFee) {
		this.cashRefundFee = cashRefundFee;
	}

	@Override
	public String toString() {
		return "RefundResult [returnCode=" + returnCode + ", returnMessage=" + returnMessage + ", resultCode=" + resultCode + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage
				+ ", appId=" + appId + ", mechantId=" + mechantId + ", nonceStr=" + nonceStr + ", sign=" + sign + ", transactionId=" + transactionId + ", outTradeNo=" + outTradeNo
				+ ", outRefundNo=" + outRefundNo + ", refundId=" + refundId + ", refundFee=" + refundFee + ", settlementRefundFee=" + settlementRefundFee + ", totalFee=" + totalFee
				+ ", settlementTotalFee=" + settlementTotalFee + ", feeType=" + feeType + ", cashFee=" + cashFee + ", cashFeeType=" + cashFeeType + ", cashRefundFee=" + cashRefundFee
				+ "]";
	}

	public void fromMap(Map<String, String> map) {
		setReturnCode(map.get("return_code"));
		setReturnMessage(map.get("return_msg"));
		setAppId(map.get("appid"));
		setMechantId(map.get("mch_id"));
		setNonceStr(map.get("nonce_str"));
		setSign(map.get("sign"));
		setResultCode(map.get("result_code"));
		setErrorCode(map.get("err_code"));
		setErrorMessage(map.get("err_code_des"));
		setOutTradeNo(map.get("out_trade_no"));
		setOutRefundNo(map.get("out_refund_no"));
		setTransactionId(map.get("transaction_id"));
		setRefundId(map.get("refund_id"));
		setTotalFee(map.get("total_fee"));
		setRefundFee(map.get("refund_fee"));
		setCashFee(map.get("cash_fee"));

	}

	/**
	 * <xml> <return_code><![CDATA[SUCCESS]]></return_code>
	 * <return_msg><![CDATA[OK]]></return_msg>
	 * <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
	 * <mch_id><![CDATA[10000100]]></mch_id>
	 * <nonce_str><![CDATA[NfsMFbUFpdbEhPXP]]></nonce_str>
	 * <sign><![CDATA[B7274EB9F8925EB93100DD2085FA56C0]]></sign>
	 * <result_code><![CDATA[SUCCESS]]></result_code>
	 * <transaction_id><![CDATA
	 * [1008450740201411110005820873]]></transaction_id>
	 * <out_trade_no><![CDATA[1415757673]]></out_trade_no>
	 * <out_refund_no><![CDATA[1415701182]]></out_refund_no>
	 * <refund_id><![CDATA[2008450740201411110000174436]]></refund_id>
	 * <refund_channel><![CDATA[]]></refund_channel>
	 * <refund_fee>1</refund_fee> </xml>
	 */
	public static void main(String[] args) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>");
		sb.append("<return_code><![CDATA[SUCCESS]]></return_code>");
		sb.append("<return_msg><![CDATA[OK]]></return_msg>");
		sb.append("<appid><![CDATA[wx2421b1c4370ec43b]]></appid>");
		sb.append("<mch_id><![CDATA[10000100]]></mch_id>");
		sb.append("<nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str>");
		sb.append("<sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign>");
		sb.append("<result_code><![CDATA[SUCCESS]]></result_code>");
		sb.append("<transaction_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></transaction_id>");
		sb.append("<out_trade_no><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></out_trade_no>");
		sb.append("<out_refund_no><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></out_refund_no>");
		sb.append("<refund_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></refund_id>");
		sb.append("<refund_fee>1</refund_fee>");
		sb.append("<total_fee>1</total_fee>");
		sb.append("<cash_fee>1</cash_fee>");

		sb.append("</xml>");

		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		Map<String, String> map = MessageUtil.parseXml(is);
		RefundResponse result = new RefundResponse();
		result.fromMap(map);
		System.out.println(result);

	}

}
