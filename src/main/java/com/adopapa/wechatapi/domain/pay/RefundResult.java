package com.adopapa.wechatapi.domain.pay;

import java.io.Serializable;
import java.util.Map;

public class RefundResult implements Serializable {

	private static final long serialVersionUID = 8844657779419449835L;

	private String returnCode;
	private String returnMessage;
	private String resultCode;
	private String errorCode;
	private String errorMessage;

	private String appId;
	private String mechantId;
	private String nonceStr;

	private String reqInfo;

	private String transactionId;
	private String outTradeNo;
	private String refundId;
	private String outRefundNo;
	private String totalFee;

	private String settlementTotalFee; // 当该订单有使用非充值券时，返回此字段。应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
	private String refundFee;
	private String settlementRefundFee; // 退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
	private String refundStatus;// SUCCESS-退款成功 CHANGE-退款异常 REFUNDCLOSE—退款关闭
	private String successTime;
	// 取当前退款单的退款入账方1）退回银行卡：{银行名称}{卡类型}{卡尾号}2）退回支付用户零钱:用户零钱
	// 3）退还商户 商户基本账户 商户结算银行账户
	private String refundRecvAccount;
	private String refundAccount; // REFUND_SOURCE_RECHARGE_FUNDS
					// 可用余额退款/基本账户
					// REFUND_SOURCE_UNSETTLED_FUNDS 未结算资金退款
	private String refundRequestSource; // VENDOR_PLATFORM

	@Override
	public String toString() {
		return "RefundResult [returnCode=" + returnCode + ", returnMessage=" + returnMessage + ", resultCode=" + resultCode + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage
				+ ", appId=" + appId + ", mechantId=" + mechantId + ", nonceStr=" + nonceStr + ", reqInfo=" + reqInfo + ", transactionId=" + transactionId + ", outTradeNo="
				+ outTradeNo + ", refundId=" + refundId + ", outRefundNo=" + outRefundNo + ", totalFee=" + totalFee + ", settlementTotalFee=" + settlementTotalFee + ", refundFee="
				+ refundFee + ", settlementRefundFee=" + settlementRefundFee + ", refundStatus=" + refundStatus + ", successTime=" + successTime + ", refundRecvAccount="
				+ refundRecvAccount + ", refundAccount=" + refundAccount + ", refundRequestSource=" + refundRequestSource + "]";
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getReqInfo() {
		return reqInfo;
	}

	public void setReqInfo(String reqInfo) {
		this.reqInfo = reqInfo;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getRefundId() {
		return refundId;
	}

	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getSettlementTotalFee() {
		return settlementTotalFee;
	}

	public void setSettlementTotalFee(String settlementTotalFee) {
		this.settlementTotalFee = settlementTotalFee;
	}

	public String getRefundFee() {
		return refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}

	public String getSettlementRefundFee() {
		return settlementRefundFee;
	}

	public void setSettlementRefundFee(String settlementRefundFee) {
		this.settlementRefundFee = settlementRefundFee;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getSuccessTime() {
		return successTime;
	}

	public void setSuccessTime(String successTime) {
		this.successTime = successTime;
	}

	public String getRefundRecvAccount() {
		return refundRecvAccount;
	}

	public void setRefundRecvAccount(String refundRecvAccount) {
		this.refundRecvAccount = refundRecvAccount;
	}

	public String getRefundAccount() {
		return refundAccount;
	}

	public void setRefundAccount(String refundAccount) {
		this.refundAccount = refundAccount;
	}

	public String getRefundRequestSource() {
		return refundRequestSource;
	}

	public void setRefundRequestSource(String refundRequestSource) {
		this.refundRequestSource = refundRequestSource;
	}

	public void fromMap(Map<String, String> map) {
		setReturnCode(map.get("return_code"));
		setReturnMessage(map.get("return_msg"));
		setAppId(map.get("appid"));
		setMechantId(map.get("mch_id"));
		setNonceStr(map.get("nonce_str"));
		setReqInfo(map.get("sign"));
		setResultCode(map.get("result_code"));
		setErrorCode(map.get("err_code"));
		setErrorMessage(map.get("err_code_des"));
		setOutTradeNo(map.get("out_trade_no"));
		setOutRefundNo(map.get("out_refund_no"));
		setTransactionId(map.get("transaction_id"));
		setRefundId(map.get("refund_id"));
		setTotalFee(map.get("total_fee"));
		setRefundFee(map.get("refund_fee"));
		
		
		

	}

}
