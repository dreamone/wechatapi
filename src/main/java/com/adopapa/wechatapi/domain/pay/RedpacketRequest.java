package com.adopapa.wechatapi.domain.pay;

import java.util.Map;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

/**
 * <xml> <sign><![CDATA[E1EE61A91C8E90F299DE6AE075D60A2D]]></sign>
 * <mch_billno><![CDATA[0010010404201411170000046545]]></mch_billno>
 * <mch_id><![CDATA[888]]></mch_id>
 * <wxappid><![CDATA[wxcbda96de0b165486]]></wxappid>
 * <send_name><![CDATA[send_name]]></send_name>
 * <re_openid><![CDATA[onqOjjmM1tad-3ROpncN-yUfa6uI]]></re_openid>
 * <total_amount><![CDATA[200]]></total_amount>
 * <total_num><![CDATA[1]]></total_num> <wishing><![CDATA[恭喜发财]]></wishing>
 * <client_ip><![CDATA[127.0.0.1]]></client_ip>
 * <act_name><![CDATA[新年红包]]></act_name> <remark><![CDATA[新年红包]]></remark>
 * <scene_id><![CDATA[PRODUCT_2]]></scene_id>
 * <nonce_str><![CDATA[50780e0cca98c8c8e814883e5caa672e]]></nonce_str>
 * <risk_info>posttime%3d123123412%26clientversion%3d234134%26mobile%3d122344545%26deviceid%3dIOS</risk_info>
 * </xml>
 *
 */
// @XStreamAlias("xml")
public class RedpacketRequest {
	// 活动名称
	private String actName;
	// 微信公众号appid
	private String wxappId;
	// 商户号 mch_id
	private String mchId;
	/**
	 * 商户订单号（每个订单号必须唯一。取值范围：0~9，a~z，A~Z） 接口根据商户订单号支持重入，如出现超时可再调用。
	 */
	// 商户订单号 mch_billno
	private String mchBillno;
	// 商户名称 send_name
	private String sendName;
	// 用户openid re_openid
	private String reOpenid;
	// 付款金额 total_amount 单位分
	private String totalAmount;
	// 红包发放总人数 total_num
	private String totalNum;
	// 红包祝福语 wishing 128
	private String wishing;
	// ip地址 client_ip
	private String clientIp;
	// 备注 remark 256
	private String remark;
	/**
	 * 发放红包使用场景， PRODUCT_1:商品促销 PRODUCT_2:抽奖 PRODUCT_3:虚拟物品兑奖  PRODUCT_4:企业内部福利
	 * PRODUCT_5:渠道分润 PRODUCT_6:保险回馈 PRODUCT_7:彩票派奖 PRODUCT_8:税务刮奖
	 */
	// 场景id scene_id 红包金额大于200或者小于1元时必传
	private String sceneId;
	// 随机字符串nonce_str
	private String nonceStr;
	// 签名sign
	private String sign;
	/**
	 * posttime:用户操作的时间戳 mobile:业务系统账号的手机号，国家代码-手机号。不需要+号 deviceid :mac 地址或者设备唯一标识 
	 * clientversion :用户操作的客户端版本 把值为非空的信息用key=value进行拼接，再进行urlencode
	 * urlencode(posttime=xx& mobile =xx&deviceid=xx) risk_info活动信息
	 */
	private String riskInfo;

	//////////////////////////////////////////////////////////////////////////////
	public String getActName() {
		return actName;
	}

	public void setActName(String actName) {
		this.actName = actName;
	}

	public String getWxappId() {
		return wxappId;
	}

	public void setWxappId(String wxappId) {
		this.wxappId = wxappId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getMchBillno() {
		return mchBillno;
	}

	public void setMchBillno(String mchBillno) {
		this.mchBillno = mchBillno;
	}

	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getReOpenid() {
		return reOpenid;
	}

	public void setReOpenid(String reOpenid) {
		this.reOpenid = reOpenid;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(String totalNum) {
		this.totalNum = totalNum;
	}

	public String getWishing() {
		return wishing;
	}

	public void setWishing(String wishing) {
		this.wishing = wishing;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getRiskInfo() {
		return riskInfo;
	}

	public void setRiskInfo(String riskInfo) {
		this.riskInfo = riskInfo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * nonce_str sign mch_billno mch_id wxappid send_name re_openid total_amount
	 * total_num wishing client_ip act_name remark scene_id risk_info
	 */
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver(true, false));
		// XStream xstream = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-",
		// "_")));
		xstream.alias("xml", this.getClass());
		xstream.aliasField("act_name", this.getClass(), "actName");
		xstream.aliasField("nonce_str", this.getClass(), "nonceStr");
		xstream.aliasField("mch_billno", this.getClass(), "mchBillno");
		xstream.aliasField("mch_id", this.getClass(), "mchId");
		xstream.aliasField("wxappid", this.getClass(), "wxappId");
		xstream.aliasField("send_name", this.getClass(), "sendName");
		xstream.aliasField("re_openid", this.getClass(), "reOpenid");
		xstream.aliasField("total_amount", this.getClass(), "totalAmount");
		xstream.aliasField("total_num", this.getClass(), "totalNum");
		xstream.aliasField("client_ip", this.getClass(), "clientIp");
		xstream.aliasField("scene_id", this.getClass(), "sceneId");
		xstream.aliasField("risk_info", this.getClass(), "riskInfo");
		String xml = xstream.toXML(this);
		return xml.replaceAll("__", "_");
	}

	/**
	 * <xml> <sign><![CDATA[E1EE61A91C8E90F299DE6AE075D60A2D]]></sign>
	 * <mch_billno><![CDATA[0010010404201411170000046545]]></mch_billno>
	 * <mch_id><![CDATA[888]]></mch_id>
	 * <wxappid><![CDATA[wxcbda96de0b165486]]></wxappid>
	 * <send_name><![CDATA[send_name]]></send_name>
	 * <re_openid><![CDATA[onqOjjmM1tad-3ROpncN-yUfa6uI]]></re_openid>
	 * <total_amount><![CDATA[200]]></total_amount>
	 * <total_num><![CDATA[1]]></total_num> <wishing><![CDATA[恭喜发财]]></wishing>
	 * <client_ip><![CDATA[127.0.0.1]]></client_ip>
	 * <act_name><![CDATA[新年红包]]></act_name> <remark><![CDATA[新年红包]]></remark>
	 * <scene_id><![CDATA[PRODUCT_2]]></scene_id>
	 * <nonce_str><![CDATA[50780e0cca98c8c8e814883e5caa672e]]></nonce_str>
	 * <risk_info>posttime%3d123123412%26clientversion%3d234134%26mobile%3d122344545%26deviceid%3dIOS</risk_info>
	 * </xml>
	 * @throws Exception 
	 *
	 */
	public static void main(String[] args) throws Exception {
		RedpacketRequest redPack = new RedpacketRequest();
		redPack.setClientIp("127.0.0.1");
		redPack.setMchBillno("122333333309000");
		redPack.setMchId("122222");
		redPack.setNonceStr("13jjakkoafa");
		redPack.setReOpenid("13jfdkjfa");
		redPack.setRiskInfo(null);
		redPack.setSceneId("PRODUCT_2");
		redPack.setSendName("一点钢网");
		redPack.setSign("1111");
		redPack.setTotalAmount("10");
		redPack.setTotalNum("1");
		redPack.setWishing("Happy Hello");
		redPack.setWxappId("98077665555");

		System.out.println(redPack.toXml());
		
		String result = "<?xml version='1.0' encoding='utf-8'?><xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[IP地址非你在商户平台设置的可用IP地址]]></return_msg><result_code><![CDATA[FAIL]]></result_code><err_code><![CDATA[NO_AUTH]]></err_code><err_code_des><![CDATA[IP地址非你在商户平台设置的可用IP地址]]></err_code_des><mch_billno><![CDATA[H201902241337529289]]></mch_billno><mch_id><![CDATA[1486220982]]></mch_id><wxappid><![CDATA[wx20c3792650764e5d]]></wxappid><re_openid><![CDATA[og5m6w8OMH4bty1Rfwe6KB8558VU]]></re_openid><total_amount>158</total_amount></xml>";
		Map<String, String> parseXml = MessageUtil.parseXml(result);
		System.err.println(parseXml);
	}
}
