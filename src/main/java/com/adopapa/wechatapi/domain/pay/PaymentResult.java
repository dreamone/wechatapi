package com.adopapa.wechatapi.domain.pay;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import com.adopapa.wechatapi.util.MessageUtil;

/**
 * PaymentResponse
 */
public class PaymentResult {
	private String appId;
	private String attach;
	private String bankType;
	private String feeType;
	private String isSubscribe;
	private String mechantId;
	private String nonceStr;
	private String openId;
	private String outTradeNo;
	private String resultCode;
	private String returnCode;
	private String returnMessage;
	private String errorCode;
	private String errorMessage;
	private String sign;
	private String subMechantId;
	private String timeEnd;
	private String totalFee;
	private String tradeType;
	private String transactionId;
	
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getBankType() {
		return bankType;
	}
	public void setBankType(String bankType) {
		this.bankType = bankType;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public String getIsSubscribe() {
		return isSubscribe;
	}
	public void setIsSubscribe(String isSubscribe) {
		this.isSubscribe = isSubscribe;
	}
	public String getMechantId() {
		return mechantId;
	}
	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}
	public String getNonceStr() {
		return nonceStr;
	}
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getOutTradeNo() {
		return outTradeNo;
	}
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getSubMechantId() {
		return subMechantId;
	}
	public void setSubMechantId(String subMechantId) {
		this.subMechantId = subMechantId;
	}
	public String getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}
	public String getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	/*<xml>
	   <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
	   <attach><![CDATA[支付测试]]></attach>
	   <bank_type><![CDATA[CFT]]></bank_type>
	   <fee_type><![CDATA[CNY]]></fee_type>
	   <is_subscribe><![CDATA[Y]]></is_subscribe>
	   <mch_id><![CDATA[10000100]]></mch_id>
	   <nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
	   <openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
	   <out_trade_no><![CDATA[1409811653]]></out_trade_no>
	   <result_code><![CDATA[SUCCESS]]></result_code>
	   <return_code><![CDATA[SUCCESS]]></return_code>
	   <sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
	   <sub_mch_id><![CDATA[10000100]]></sub_mch_id>
	   <time_end><![CDATA[20140903131540]]></time_end>
	   <total_fee>1</total_fee>
	   <trade_type><![CDATA[JSAPI]]></trade_type>
	   <transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
	</xml> */
	
	public void fromMap(Map<String, String> map) {
		setAppId(map.get("appid"));
		setAttach(map.get("attach"));
		setBankType(map.get("bank_type"));
		setFeeType(map.get("fee_type"));
		setIsSubscribe(map.get("is_subscribe"));
		setMechantId(map.get("mch_id"));
		setNonceStr(map.get("nonce_str"));
		setOpenId(map.get("openid"));
		setOutTradeNo(map.get("out_trade_no"));
		setResultCode(map.get("result_code"));
		setReturnCode(map.get("return_code"));
		setReturnMessage(map.get("return_msg"));
		setErrorCode(map.get("err_code"));
		setErrorMessage(map.get("err_code_des"));
		setSign(map.get("sign"));
		setSubMechantId(map.get("sub_mch_id"));
		setTimeEnd(map.get("time_end"));
		setTotalFee(map.get("total_fee"));
		setTradeType(map.get("trade_type"));
		setTransactionId(map.get("transaction_id"));
	}
	
	
	
	@Override
	public String toString() {
		return "PaymentResult [appId=" + appId + ", attach=" + attach + ", bankType=" + bankType + ", feeType=" + feeType + ", isSubscribe=" + isSubscribe + ", mechantId="
				+ mechantId + ", nonceStr=" + nonceStr + ", openId=" + openId + ", outTradeNo=" + outTradeNo + ", resultCode=" + resultCode + ", returnCode="
				+ returnCode + ", sign=" + sign + ", subMechantId=" + subMechantId + ", timeEnd=" + timeEnd + ", totalFee=" + totalFee + ", tradeType=" + tradeType
				+ ", transactionId=" + transactionId + "]";
	}
	public static void main(String[] args) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>");
		sb.append("<appid><![CDATA[wx2421b1c4370ec43b]]></appid>");
		sb.append("<attach><![CDATA[支付测试]]></attach>");
		sb.append("<bank_type><![CDATA[CFT]]></bank_type>");
		sb.append("<fee_type><![CDATA[CNY]]></fee_type>");
		sb.append("<is_subscribe><![CDATA[Y]]></is_subscribe>");
		sb.append("<mch_id><![CDATA[10000100]]></mch_id>");
		sb.append("<nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>");
		sb.append("<openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>");
		sb.append("<out_trade_no><![CDATA[1409811653]]></out_trade_no>");
		sb.append("<result_code><![CDATA[SUCCESS]]></result_code>");
		sb.append("<return_code><![CDATA[SUCCESS]]></return_code>");
		sb.append("<sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>");
		sb.append("<sub_mch_id><![CDATA[10000100]]></sub_mch_id>");
		sb.append("<time_end><![CDATA[20140903131540]]></time_end>");
		sb.append("<total_fee>1</total_fee>");
		sb.append("<trade_type><![CDATA[JSAPI]]></trade_type>");
		sb.append("<transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>");
		sb.append("</xml>");
		
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		Map<String, String> map = MessageUtil.parseXml(is);
		PaymentResult result = new PaymentResult();
		result.fromMap(map);
		System.out.println(result);
	}

}
