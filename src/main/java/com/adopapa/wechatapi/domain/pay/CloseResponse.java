package com.adopapa.wechatapi.domain.pay;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import com.adopapa.wechatapi.util.MessageUtil;

public class CloseResponse implements Serializable {

	private static final long serialVersionUID = 6321452287225836673L;

	private String returnCode;
	private String returnMessage;
	private String resultCode;
	private String resultMessage;

	private String errorCode;
	private String errorMessage;

	private String appId;
	private String mechantId;
	private String nonceStr;
	private String sign;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	

	@Override
	public String toString() {
		return "CloseResult [returnCode=" + returnCode + ", returnMessage=" + returnMessage + ", resultCode=" + resultCode + ", resultMessage=" + resultMessage + ", errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + ", appId=" + appId + ", mechantId=" + mechantId + ", nonceStr=" + nonceStr + ", sign=" + sign + "]";
	}

	public void fromMap(Map<String, String> map) {
		setReturnCode(map.get("return_code"));
		setReturnMessage(map.get("return_msg"));
		setAppId(map.get("appid"));
		setMechantId(map.get("mch_id"));
		setNonceStr(map.get("nonce_str"));
		setSign(map.get("sign"));
		setResultCode(map.get("result_code"));
		setResultMessage(map.get("result_msg"));
		setErrorCode(map.get("err_code"));
		setErrorMessage(map.get("err_code_des"));

	}

	/**
	 * <xml> <return_code><![CDATA[SUCCESS]]></return_code>
	 * <return_msg><![CDATA[OK]]></return_msg>
	 * <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
	 * <mch_id><![CDATA[10000100]]></mch_id>
	 * <nonce_str><![CDATA[BFK89FC6rxKCOjLX]]></nonce_str>
	 * <sign><![CDATA[72B321D92A7BFA0B2509F3D13C7B1631]]></sign>
	 * <result_code><![CDATA[SUCCESS]]></result_code>
	 * <result_msg><![CDATA[OK]]></result_msg> </xml>
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>");
		sb.append("<return_code><![CDATA[SUCCESS]]></return_code>");
		sb.append("<return_msg><![CDATA[OK]]></return_msg>");
		sb.append("<appid><![CDATA[wx2421b1c4370ec43b]]></appid>");
		sb.append("<mch_id><![CDATA[10000100]]></mch_id>");
		sb.append("<nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str>");
		sb.append("<sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign>");
		sb.append("<result_code><![CDATA[SUCCESS]]></result_code>");
		sb.append("<result_msg><![CDATA[SUCCESS]]></result_msg>");
		sb.append("</xml>");

		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		Map<String, String> map = MessageUtil.parseXml(is);
		CloseResponse result = new CloseResponse();
		result.fromMap(map);
		System.out.println(result);

	}

}
