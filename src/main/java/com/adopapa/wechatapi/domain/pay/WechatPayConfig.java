package com.adopapa.wechatapi.domain.pay;

public class WechatPayConfig {
	// timestamp: 1430607451, //
	// 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
	// nonceStr: "0e81278487b14523add45ad848627800", // 支付签名随机串，不长于 32 位
	// package: "prepay_id=wx2015050306573111f978a2b60613636291", //
	// 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
	// signType: "MD5", // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
	// paySign: "F8E199F034B9FE7279005E07EA5BDEA7", // 支付签名
	// app支付
	// { "appid":"wxb4ba3c02aa476ea1",
	// "noncestr":"9317585770323ecef7fd7b413a96170c",
	// "package":"Sign=WXPay", "partnerid":"10000100",
	// "prepayid":"wx201601171046543f3b35ba350938271485",
	// "timestamp":"1452998814", "sign":"914B6C5E8720289FF52B9D1D864EDDC7" }
	private String timeStamp;
	private String nonceStr;
	private String pkg; // packageValue
	private String signType; // JSAPI 需要
	private String paySign; // APP为sign
	private String appId; // APP需要
	private String partnerId; // APP需要
	private String parepayId; // APP需要

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getPkg() {
		return pkg;
	}

	public void setPkg(String pkg) {
		this.pkg = pkg;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getParepayId() {
		return parepayId;
	}

	public void setParepayId(String parepayId) {
		this.parepayId = parepayId;
	}

}
