package com.adopapa.wechatapi.domain.pay;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import com.adopapa.wechatapi.util.MessageUtil;

public class UnifiedResult {
	// 返回状态码 return_code 是 String(16) SUCCESS SUCCESS/FAIL
	// 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
	private String returnCode;
	// 返回信息//return_msg否 String(128) 签名失败
	private String returnMessage;
	// 以下字段在return_code为SUCCESS的时候有返回
	// 公众账号 appid 是 String(32) wx8888888888888888 调用接口提交的公众账号ID
	private String appId;
	// 商户号 mch_id是 String(32) 1900000109 调用接口提交的商户号
	private String mechantId;
	// 设备号 device_info 否 String(32) 013467007045764 调用接口提交的终端设备号，
	private String deviceInfo;
	// 随机字符串 nonce_str 是 String(32) 5K8264ILTKCH16CQ2502SI8ZNMTM67VS 微信返回的随机字符串
	private String nonceStr;
	// 签名 sign 是 String(32) C380BEC2BFD727A4B6845133519F3AD6 微信返回的签名，详见签名算法
	private String sign;
	// 业务结果 result_code 是 String(16) SUCCESS SUCCESS/FAIL
	private String resultCode;
	// 错误代码 err_code 否 String(32) SYSTEMERROR 详细参见第6节错误列表
	private String errCode;
	// 错误代码描述 err_code_des 否 String(128) 系统错误 错误返回的信息描述
	private String errCodeDesc;
	// 以下字段在return_code 和result_code都为SUCCESS的时候有返回
	// 交易类型 trade_type 是 String(16) JSAPI
	// 调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP，详细说明见参数规定
	private String tradeType;
	// 预支付交易会话标识 prepay_id 是 String(64) wx201410272009395522657a690389285100
	// 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时
	private String prepayId;
	// 二维码链接 code_url 否 String(64) URl： weixin://wxpay/s/An4baqw
	// trade_type为NATIVE是有返回，可将该参数值生成二维码展示出来进行扫码支付
	private String codeUrl;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrCodeDesc() {
		return errCodeDesc;
	}

	public void setErrCodeDesc(String errCodeDesc) {
		this.errCodeDesc = errCodeDesc;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getPrepayId() {
		return prepayId;
	}

	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}

	public String getCodeUrl() {
		return codeUrl;
	}

	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	@Override
	public String toString() {
		return "UnifiedResult [returnCode=" + returnCode + ", returnMessage=" + returnMessage + ", appId=" + appId + ", mechantId=" + mechantId
				+ ", deviceInfo=" + deviceInfo + ", nonceStr=" + nonceStr + ", sign=" + sign + ", resultCode=" + resultCode + ", errCode=" + errCode
				+ ", errCodeDesc=" + errCodeDesc + ", tradeType=" + tradeType + ", prepayId=" + prepayId + ", codeUrl=" + codeUrl + "]";
	}

	/**
	 * <xml> <return_code><![CDATA[SUCCESS]]></return_code>
	 * <return_msg><![CDATA[OK]]></return_msg>
	 * <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
	 * <mch_id><![CDATA[10000100]]></mch_id>
	 * <nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str>
	 * <sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign>
	 * <result_code><![CDATA[SUCCESS]]></result_code>
	 * <prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id>
	 * <trade_type><![CDATA[JSAPI]]></trade_type> </xml>
	 */
	public void fromMap(Map<String, String> map) {
		setReturnCode(map.get("return_code"));
		setReturnMessage(map.get("return_msg"));
		setAppId(map.get("appid"));
		setMechantId(map.get("mch_id"));
		setDeviceInfo(map.get("device_info"));
		setNonceStr(map.get("nonce_str"));
		setSign(map.get("sign"));
		setResultCode(map.get("result_code"));
		setErrCode(map.get("err_code"));
		setErrCodeDesc(map.get("err_code_des"));
		setTradeType(map.get("trade_type"));
		setPrepayId(map.get("prepay_id"));
		setCodeUrl(map.get("code_url"));
	}
	
	public static void main(String[] args) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>");
		sb.append("<return_code><![CDATA[SUCCESS]]></return_code>");
		sb.append("<return_msg><![CDATA[OK]]></return_msg>");
		sb.append("<appid><![CDATA[wx2421b1c4370ec43b]]></appid>");
		sb.append("<mch_id><![CDATA[10000100]]></mch_id>");
		sb.append("<nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str>");
		sb.append("<sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign>");
		sb.append("<result_code><![CDATA[SUCCESS]]></result_code>");
		sb.append("<prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id>");
		sb.append("<trade_type><![CDATA[JSAPI]]></trade_type>");
		sb.append("</xml>");
		
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		Map<String, String> map = MessageUtil.parseXml(is);
		UnifiedResult result = new UnifiedResult();
		result.fromMap(map);
		System.out.println(result);
		
	}
	

}
