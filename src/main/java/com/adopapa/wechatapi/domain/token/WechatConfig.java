package com.adopapa.wechatapi.domain.token;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.adopapa.wechatapi.util.JsonUtil;

public class WechatConfig {
	
	private boolean debug ;
	private String appId;
	private String timestamp;
	private String nonceStr;
	private String signature;
	private List<String> jsApiList;
	
	
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getNonceStr() {
		return nonceStr;
	}
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public List<String> getJsApiList() {
		return jsApiList;
	}
	public void setJsApiList(List<String> jsApiList) {
		this.jsApiList = jsApiList;
	}
	
	@Override
	public String toString() {
		return "WechatConfig [debug=" + debug + ", appId=" + appId + ", timestamp=" + timestamp + ", nonceStr="
				+ nonceStr + ", signature=" + signature + ", jsApiList=" + jsApiList + "]";
	}
	
	
	public static void main(String[] args) throws IOException {
		WechatConfig config = new WechatConfig();
		config.setDebug(false);
		config.setAppId("12445333");
		config.setNonceStr("kajfkajfkajfkd");
		config.setSignature("jkjfkdjadkf");
		config.setTimestamp(Long.toString(System.currentTimeMillis() / 1000));
		List<String> jsApiList = new LinkedList<String>();
		jsApiList.add("chooseWXpay");
		jsApiList.add("getNetworkTYpe");
		config.setJsApiList(jsApiList);
		
		System.out.println(config);
		
		System.out.println(JsonUtil.bean2Json(config));
		//1429942270
		//1429965601
		
//		System.out.println(WeChatUtil.sign4JsApi("sM4AOVdWfPE4DxkXGEs8VHpyC4fZdg1jQuNtkTUHQsAy9a8MtTQTLDfZ--loFInPzeZOm460yGXY5AL71P0YBA", 
//				"adopapa", "1429968187", "http://118.244.237.56/app/testjs/toTestjs.do"));
	}
	
}
