package com.adopapa.wechatapi.domain.token;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;

/**
 * 微信通用接口凭证
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessToken extends ResultMessage implements Serializable {

	private static final long serialVersionUID = 4150567105673692503L;

	public AccessToken(int errcode, String errmsg) {
		super(errcode, errmsg);
	}
	
	public AccessToken() {
		this(0, "ok");
	}

	// 获取到的凭证
	@JsonProperty("access_token")
	private String token;
	// 凭证有效时间，单位：秒 7200秒 2小时
	@JsonProperty("expires_in")
	private int expiresIn;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	@Override
	public String toString() {
		return "AccessToken [getToken()=" + getToken() + ", getExpiresIn()=" + getExpiresIn() + ", getErrcode()=" + getErrcode() + ", getErrmsg()="
				+ getErrmsg() + "]";
	}

}