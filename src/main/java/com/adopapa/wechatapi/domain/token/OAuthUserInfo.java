package com.adopapa.wechatapi.domain.token;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.MappedSuperclass;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.adopapa.wechatapi.domain.common.ResultMessage;

/**
 * 网页授权获取的用户信息
 */

@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public class OAuthUserInfo extends ResultMessage implements Serializable {

	private static final long serialVersionUID = -1271354604788207257L;

	public OAuthUserInfo(int errcode, String errmsg) {
		super(errcode, errmsg);
	}

	public OAuthUserInfo() {
		this(0, "ok");
	}

	// openid 用户的唯一标识
	// nickname 用户昵称
	// sex 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	// province 用户个人资料填写的省份
	// city 普通用户个人资料填写的城市
	// country 国家，如中国为CN
	// headimgurl 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），
	// 用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	// privilege 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	// unionid 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。详见：获取用户个人信息（UnionID机制）

	// {
	// "openid":" OPENID",
	// " nickname": NICKNAME,
	// "sex":"1",
	// "province":"PROVINCE"
	// "city":"CITY",
	// "country":"COUNTRY",
	// "headimgurl":
	// "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46",
	// "privilege":[
	// "PRIVILEGE1"
	// "PRIVILEGE2"
	// ]
	// "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
	// }

	// subscribe 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
	// openid 用户的标识，对当前公众号唯一
	// nickname 用户的昵称
	// sex 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	// city 用户所在城市
	// country 用户所在国家
	// province 用户所在省份
	// language 用户的语言，简体中文为zh_CN
	// headimgurl
	// 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	// subscribe_time 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	// unionid 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。详见：获取用户个人信息（UnionID机制

	// {
	// "subscribe": 1,
	// "openid": "okkwgt3rADJlkQnV4UGsrf4zGFno",
	// "nickname": "无意亦真意",
	// "sex": 0,
	// "language": "zh_CN",
	// "city": "",
	// "province": "",
	// "country": "",
	// "headimgurl":
	// "http://wx.qlogo.cn/mmopen/dTVW9DbUG5gdzYz86NvTFKrI8PbN0yYJsbcDs8ZHuCHAOaeY4FJUnhprTsTx9ZliaJCqrRABeAOoP13QF6gFeaVmP0fz49ibqw/0",
	// "subscribe_time": 1424239712,
	// "remark": ""
	// }

	private String openid;
	private String nickname;
	private String sex;
	private String language;
	private String province;
	private String city;
	private String country;
	private String headimgurl;
	private String[] privilege;
	private String unionid;

	private int subscribe;

	private long subscribeTime;

	private String remark;

	private String groupid;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public String[] getPrivilege() {
		return privilege;
	}

	public void setPrivilege(String[] privilege) {
		this.privilege = privilege;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public int getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(int subscribe) {
		this.subscribe = subscribe;
	}

	public long getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(long subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	@Override
	public String toString() {
		return "OAuthUserInfo [openid=" + openid + ", nickname=" + nickname + ", sex=" + sex + ", language=" + language + ", province=" + province + ", city=" + city
				+ ", country=" + country + ", headimgurl=" + headimgurl + ", privilege=" + Arrays.toString(privilege) + ", unionid=" + unionid + ", subscribe="
				+ subscribe + ", subscribeTime=" + subscribeTime + ", remark=" + remark + ", groupid=" + groupid + "]";
	}

	

}
