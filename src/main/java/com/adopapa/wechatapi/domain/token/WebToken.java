package com.adopapa.wechatapi.domain.token;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.adopapa.wechatapi.domain.common.ResultMessage;

/**
 * 网页授权Token
 *
 * { "access_token":"ACCESS_TOKEN", "expires_in":7200,
 * "refresh_token":"REFRESH_TOKEN", "openid":"OPENID", "scope":"SCOPE" }
 */
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebToken extends ResultMessage implements Serializable {

	private static final long serialVersionUID = -6991691437396940408L;

	public WebToken(int errcode, String errmsg) {
		super(errcode, errmsg);
	}

	public WebToken() {
		this(0, "ok");
	}

	// access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	// expires_in access_token接口调用凭证超时时间，单位（秒）
	// refresh_token 用户刷新access_token
	// openid 用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID
	// scope 用户授权的作用域，使用逗号（,）分隔
	// {
	// "access_token":"ACCESS_TOKEN",
	// "expires_in":7200,
	// "refresh_token":"REFRESH_TOKEN",
	// "openid":"OPENID",
	// "scope":"SCOPE"
	// }
	// {"errcode":40029,"errmsg":"invalid code"}
	private String accessToken;
	private int expiresIn;
	private String refreshToken;
	private String openId;
	private String scope;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

}
