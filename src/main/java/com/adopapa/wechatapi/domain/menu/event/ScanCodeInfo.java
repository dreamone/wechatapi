package com.adopapa.wechatapi.domain.menu.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ScanCodeInfo {

	// ScanType 扫描类型，一般是qrcode
	// ScanResult 扫描结果，即二维码对应的字符串信息

	@XStreamAlias("ScanType")
	private String scanType = "qrcode";
	@XStreamAlias("ScanResult")
	private String scanResult;

	public String getScanType() {
		return scanType;
	}

	public void setScanType(String scanType) {
		this.scanType = scanType;
	}

	public String getScanResult() {
		return scanResult;
	}

	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}

	@Override
	public String toString() {
		return "ScanCodeInfo [scanType=" + scanType + ", scanResult=" + scanResult + "]";
	}

	
	
}
