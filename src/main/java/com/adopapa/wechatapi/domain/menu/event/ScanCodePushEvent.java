package com.adopapa.wechatapi.domain.menu.event;

import java.io.Serializable;
import java.util.Map;

import com.adopapa.wechatapi.domain.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * 扫码推事件的事件推送
 */
@Deprecated
@XStreamAlias("xml")
public class ScanCodePushEvent extends Event implements Serializable {
	
	private static final long serialVersionUID = 5244587387548203930L;

	// ToUserName 开发者微信号
	// FromUserName 发送方帐号（一个OpenID）
	// CreateTime 消息创建时间（整型）
	// MsgType 消息类型，event
	// Event 事件类型，scancode_push
	// EventKey 事件KEY值，由开发者在创建菜单时设定
	// ScanCodeInfo 扫描信息
	// ScanType 扫描类型，一般是qrcode
	// ScanResult 扫描结果，即二维码对应的字符串信息
	//
	// <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
	// <FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
	// <CreateTime>1408090502</CreateTime>
	// <MsgType><![CDATA[event]]></MsgType>
	// <Event><![CDATA[scancode_push]]></Event>
	// <EventKey><![CDATA[6]]></EventKey>
	// <ScanCodeInfo><ScanType><![CDATA[qrcode]]></ScanType>
	// <ScanResult><![CDATA[1]]></ScanResult>
	// </ScanCodeInfo>
	// </xml>
	//
	
	@XStreamAlias("EventKey")
	private String eventKey;
	@XStreamAlias("ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;
	
	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}
	
	@Override
	public void fromMap(Map<String, String> map) {
		setEventKey(map.get("EventKey"));
		ScanCodeInfo scanCodeInfo = new ScanCodeInfo();
		scanCodeInfo.setScanResult(map.get("ScanResult"));
		scanCodeInfo.setScanType(map.get("ScanType"));
		setScanCodeInfo(scanCodeInfo);
	}

	public ScanCodePushEvent() {
		setMsgType("event");
		setEvent(MenuEventType.scancode_push.name());
	}

	@Override
	public String toString() {
		return "ScanCodePushEvent [getEventKey()=" + getEventKey() + ", getScanCodeInfo()=" + getScanCodeInfo()
				+ ", getToUserName()=" + getToUserName() + ", getFromUserName()=" + getFromUserName()
				+ ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType() + ", getEvent()="
				+ getEvent() + "]";
	}
	
	

}
