package com.adopapa.wechatapi.domain.menu.event;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class SendPicsInfo {

	// Count 发送的图片数量
	// PicList 图片列表
	// PicMd5Sum 图片的MD5值，开发者若需要，可用于验证接收到图片
	// <SendPicsInfo><Count>1</Count>
	// <PicList><item><PicMd5Sum><![CDATA[1b5f7c23b5bf75682a53e7b6d163e185]]></PicMd5Sum>
	// </item>
	// </PicList>
	// </SendPicsInfo>

	@XStreamAlias("Count")
	private int count;

//	@XStreamAlias("PicList")
//	private PicInfos picInfos;

	@XStreamAlias("PicList")
//	 @XStreamImplicit(itemFieldName = "item")
	 private List<PicInfo> picList;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	// public List<PicInfo> getPicList() {
	// return picList;
	// }
	//
	// public void setPicList(List<PicInfo> picList) {
	// this.picList = picList;
	// }

	 @Override
	 public String toString() {
	 return "SendPicsInfo [count=" + count + ", picList=" + picList + "]";
	 }

//	public PicInfos getPicInfos() {
//		return picInfos;
//	}
//
//	public void setPicInfos(PicInfos picInfos) {
//		this.picInfos = picInfos;
//	}
//	@Override
//	public String toString() {
//		return "SendPicsInfo [count=" + count + ", picInfos=" + picInfos + "]";
//	}


//	public class PicInfos {
//
//		@XStreamImplicit(itemFieldName = "item")
//		private List<PicInfo> picInfos;
//
//		public List<PicInfo> getPicInfos() {
//			return picInfos;
//		}
//
//		public void setPicInfos(List<PicInfo> picInfos) {
//			this.picInfos = picInfos;
//		}
//
//		@Override
//		public String toString() {
//			return "PicInfos [picInfos=" + picInfos + "]";
//		}
//
//	}

	@XStreamAlias("item")
	public class PicInfo {

		@XStreamAlias("PicMd5Sum")
		private String picMd5Sum;

		public String getPicMd5Sum() {
			return picMd5Sum;
		}

		public void setPicMd5Sum(String picMd5Sum) {
			this.picMd5Sum = picMd5Sum;
		}

		@Override
		public String toString() {
			return "PicInfo [picMd5Sum=" + picMd5Sum + "]";
		}

	}

}
