package com.adopapa.wechatapi.domain.menu.event;

import java.util.Map;

import com.adopapa.wechatapi.domain.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 弹出地理位置选择器的事件推送
 */

@Deprecated
@XStreamAlias("xml")
public class LocationSelectEvent extends Event {

	// <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
	// <FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
	// <CreateTime>1408091189</CreateTime>
	// <MsgType><![CDATA[event]]></MsgType>
	// <Event><![CDATA[location_select]]></Event>
	// <EventKey><![CDATA[6]]></EventKey>
	// <SendLocationInfo><Location_X><![CDATA[23]]></Location_X>
	// <Location_Y><![CDATA[113]]></Location_Y>
	// <Scale><![CDATA[15]]></Scale>
	// <Label><![CDATA[ 广州市海珠区客村艺苑路 106号]]></Label>
	// <Poiname><![CDATA[]]></Poiname>
	// </SendLocationInfo>
	// </xml>
	// ToUserName 开发者微信号
	// FromUserName 发送方帐号（一个OpenID）
	// CreateTime 消息创建时间 （整型）
	// MsgType 消息类型，event
	// Event 事件类型，location_select
	// EventKey 事件KEY值，由开发者在创建菜单时设定
	// SendLocationInfo 发送的位置信息
	// Location_X X坐标信息
	// Location_Y Y坐标信息
	// Scale 精度，可理解为精度或者比例尺、越精细的话 scale越高
	// Label 地理位置的字符串信息
	// Poiname 朋友圈POI的名字，可能为空

	@XStreamAlias("EventKey")
	private String eventKey;
	@XStreamAlias("SendLocationInfo")
	private SendLocationInfo sendLocationInfo;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public SendLocationInfo getSendLocationInfo() {
		return sendLocationInfo;
	}

	public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
		this.sendLocationInfo = sendLocationInfo;
	}

	@Override
	public void fromMap(Map<String, String> map) {
		setEventKey(map.get("EventType"));
		SendLocationInfo sendLocationInfo = new SendLocationInfo();
		sendLocationInfo.setLocationX(map.get("Location_X"));
		sendLocationInfo.setLocationY(map.get("Location_Y"));
		sendLocationInfo.setLabel(map.get("Label"));
		sendLocationInfo.setScale(map.get("scale"));
		sendLocationInfo.setPoiName(map.get("Poiname"));
		setSendLocationInfo(sendLocationInfo);
	}

}
