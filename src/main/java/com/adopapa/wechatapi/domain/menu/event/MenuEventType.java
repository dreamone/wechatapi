package com.adopapa.wechatapi.domain.menu.event;

/**
 * 用户点击自定义菜单后，微信会把点击事件推送给开发者
 */
public enum MenuEventType {
	
	CLICK, VIEW, scancode_push, scancode_waitmsg, pic_sysphoto, pic_photo_or_album, pic_weixin, location_select,
	;

//	1 点击菜单拉取消息时的事件推送
//	2 点击菜单跳转链接时的事件推送
//	3 scancode_push：扫码推事件的事件推送
//	4 scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框的事件推送
//	5 pic_sysphoto：弹出系统拍照发图的事件推送
//	6 pic_photo_or_album：弹出拍照或者相册发图的事件推送
//	7 pic_weixin：弹出微信相册发图器的事件推送
//	8 location_select：弹出地理位置选择器的事件推送
	
}
