package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import com.adopapa.wechatapi.domain.menu.event.ScanCodeInfo;
import com.adopapa.wechatapi.domain.menu.event.SendLocationInfo;
import com.adopapa.wechatapi.domain.menu.event.SendPicsInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("xml")
public class WeChatEvent extends Event {

	/**
	 * for CLICK, VIEW, Subscribe, Unsubscribe
	 */

	/**
	 * for LOCATION
	 */
	private double latitude;
	private double longitude;
	private double precision;

	/**
	 * for SCAN
	 */
	@XStreamAlias("Ticket")
	private String ticket;

	/**
	 * for templateMessage
	 */
	@XStreamAlias("Status")
	private String status;

	@XStreamAlias("MsgID")
	private long msgId;

	@XStreamAlias("EventKey")
	private String eventKey;

	/**
	 * for scancode_push, scancode_waitmsg
	 */
	@XStreamAlias("ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;

	/**
	 * pic_sysphoto, pic_photo_or_album, pic_weixin
	 */
	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	/**
	 * location_select
	 */
	@XStreamAlias("SendLocationInfo")
	private SendLocationInfo sendLocationInfo;

	@XStreamAlias("MenuId")
	private int menuId;

	@XStreamAlias("ConnectTime")
	private long connectTime;
	@XStreamAlias("ExpireTime")
	private long expireTime;
	@XStreamAlias("VendorId")
	private String vendorId;
	@XStreamAlias("ShopId")
	private String shopId;
	@XStreamAlias("DeviceNo")
	private String deviceNo;

	@Override
	public void fromMap(Map<String, String> map) {

	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}

	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public SendLocationInfo getSendLocationInfo() {
		return sendLocationInfo;
	}

	public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
		this.sendLocationInfo = sendLocationInfo;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public long getConnectTime() {
		return connectTime;
	}

	public void setConnectTime(long connectTime) {
		this.connectTime = connectTime;
	}

	public long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	@Override
	public String toString() {
		return "WeChatEvent [latitude=" + latitude + ", longitude=" + longitude + ", precision=" + precision
				+ ", ticket=" + ticket + ", status=" + status + ", eventKey=" + eventKey + ", scanCodeInfo="
				+ scanCodeInfo + ", sendPicsInfo=" + sendPicsInfo + ", sendLocationInfo=" + sendLocationInfo
				+ ", menuId=" + menuId + ", connectTime=" + connectTime + ", expireTime=" + expireTime + ", vendorId="
				+ vendorId + ", shopId=" + shopId + ", deviceNo=" + deviceNo + ", getToUserName()=" + getToUserName()
				+ ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime()
				+ ", getMsgType()=" + getMsgType() + ", getEvent()=" + getEvent() + "]";
	}

}
