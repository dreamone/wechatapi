package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import javax.persistence.MappedSuperclass;

/**
 * 取消关注事件
 */
@Deprecated
@MappedSuperclass
public class UnsubscribeEvent extends Event {

	public UnsubscribeEvent() {
		setMsgType("event");
		setEvent(EventType.unsubscribe.name());
	}
	
	@Override
	public void fromMap(Map<String, String> map) {
		setToUserName(map.get("ToUserName"));
		setFromUserName(map.get("FromUserName"));
		setCreateTime(Long.parseLong(map.get("CreateTime")));
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[FromUser]]></FromUserName>
	 * <CreateTime>123456789</CreateTime>
	 * <MsgType><![CDATA[event]]></MsgType>
	 * <Event><![CDATA[subscribe]]></Event> </xml>
	 * 
	 * ToUserName 开发者微信号 FromUserName 发送方帐号（一个OpenID） CreateTime 消息创建时间 （整型）
	 * MsgType 消息类型，event Event 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
	 */
}
