package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import javax.persistence.MappedSuperclass;

/**
 * 上报地理位置事件
 * 用户同意上报地理位置后，每次进入公众号会话时，都会在进入时上报地理位置，
 * 或在进入会话后每5秒上报一次地理位置，公众号可以在公众平台网站中修改以上设置。
 * 上报地理位置时，微信会将上报地理位置事件推送到开发者填写的URL。
 */
@Deprecated
@MappedSuperclass
public class LocationEvent extends Event {

	private double latitude;

	private double longitude;

	private double precision;

	public LocationEvent() {
		setMsgType("event");
		setEvent(EventType.LOCATION.name());
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	@Override
	public void fromMap(Map<String, String> map) {
		setToUserName(map.get("ToUserName"));
		setFromUserName(map.get("FromUserName"));
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setLatitude(Double.parseDouble(map.get("Latitude")));
		setLongitude(Double.parseDouble(map.get("Longitude")));
		setPrecision(Double.parseDouble(map.get("Precision")));
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>123456789</CreateTime>
	 * <MsgType><![CDATA[event]]></MsgType>
	 * <Event><![CDATA[LOCATION]]></Event> <Latitude>23.137466</Latitude>
	 * <Longitude>113.352425</Longitude> <Precision>119.385040</Precision>
	 * </xml>
	 */

	/**
	 * ToUserName 开发者微信号 FromUserName 发送方帐号（一个OpenID） CreateTime 消息创建时间 （整型）
	 * MsgType 消息类型，event Event 事件类型，LOCATION Latitude 地理位置纬度 Longitude
	 * 地理位置经度 Precision 地理位置精度
	 */

}
