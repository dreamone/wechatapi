package com.adopapa.wechatapi.domain.event;

public enum EventType {
	subscribe, unsubscribe, SCAN, LOCATION,  
	CLICK, VIEW, scancode_push, scancode_waitmsg, pic_sysphoto, pic_photo_or_album, pic_weixin, location_select, //菜单推送事件
	TEMPLATESENDJOBFINISH, //模板消息推送事件
	MASSSENDJOBFINISH, //群发消息结果推送事件
	enter_agent, //成员进入应用事件
	batch_job_result, //异步任务完成事件推送
	WifiConnected
	;
}