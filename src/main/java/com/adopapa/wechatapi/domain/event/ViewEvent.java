package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import javax.persistence.MappedSuperclass;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * 自定义菜单事件
 * 点击菜单跳转链接时的事件推送
 */
@MappedSuperclass
@XStreamAlias("xml")
@Deprecated
public class ViewEvent extends Event {
	/**
	 * ToUserName 开发者微信号 FromUserName 发送方帐号（一个OpenID） CreateTime 消息创建时间 （整型）
	 * MsgType 消息类型，event Event 事件类型，VIEW EventKey 事件KEY值，设置的跳转URL
	 */
	@XStreamAlias("EventKey")
	private String eventKey;

	public ViewEvent() {
		setMsgType("event");
		setEvent(EventType.VIEW.name());

	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	@Override
	public void fromMap(Map<String, String> map) {
		setToUserName(map.get("ToUserName"));
		setFromUserName(map.get("FromUserName"));
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setEventKey(map.get("EventKey"));
	}

	@Override
	public String toString() {
		return "ViewEvent [getEventKey()=" + getEventKey() + ", getToUserName()=" + getToUserName() + ", getFromUserName()="
				+ getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType()
				+ ", getEvent()=" + getEvent() + "]";
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[FromUser]]></FromUserName>
	 * <CreateTime>123456789</CreateTime>
	 * <MsgType><![CDATA[event]]></MsgType> <Event><![CDATA[VIEW]]></Event>
	 * <EventKey><![CDATA[www.qq.com]]></EventKey> </xml>
	 */
	
	

}
