package com.adopapa.wechatapi.domain.common;

public enum SignType {

	MD5("MD5"), HMAC_SHA256("HMAC-SHA256"),
	;

	private String value;

	private SignType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
