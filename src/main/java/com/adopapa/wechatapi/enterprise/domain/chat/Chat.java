package com.adopapa.wechatapi.enterprise.domain.chat;

import java.io.Serializable;
import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Chat implements Serializable {

	private static final long serialVersionUID = -8790804822597072387L;

	// chatid 是 会话id
	// op_user 是 操作人userid
	// name 否 会话标题
	// owner 否 管理员userid，必须是该会话userlist的成员之一
	// add_user_list 否 会话新增成员列表，成员用userid来标识
	// del_user_list 否 会话退出成员列表，成员用userid来标识
	// userlist 会话成员列表，成员用userid来标识
	@JsonProperty("chatid")
	private String chatId;
	@JsonProperty("op_user")
	private String operator;
	private String owner;
	private String name;
	@JsonProperty("userlist")
	private String[] userList;
	@JsonProperty("add_user_list")
	private String[] addUserList;
	@JsonProperty("del_user_list")
	private String[] delUserList;

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getUserList() {
		return userList;
	}

	public void setUserList(String[] userList) {
		this.userList = userList;
	}

	public String[] getAddUserList() {
		return addUserList;
	}

	public void setAddUserList(String[] addUserList) {
		this.addUserList = addUserList;
	}

	public String[] getDelUserList() {
		return delUserList;
	}

	public void setDelUserList(String[] delUserList) {
		this.delUserList = delUserList;
	}

	@Override
	public String toString() {
		return "Chat [chatId=" + chatId + ", operator=" + operator + ", owner=" + owner + ", name=" + name + ", userList=" + Arrays.toString(userList) + ", addUserList="
				+ Arrays.toString(addUserList) + ", delUserList=" + Arrays.toString(delUserList) + "]";
	}

	
}
