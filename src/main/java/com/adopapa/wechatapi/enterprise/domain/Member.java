package com.adopapa.wechatapi.enterprise.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Member extends ResultMessage {

	private static final long serialVersionUID = 7084679183840738858L;

	@JsonProperty("userid")
	private String userId;
	private String name;
	@JsonProperty("department")
	private Long[] departIds;
	private String position;
	private String mobile;
	private String gender;
	private String tel;
	private String email;
	@JsonProperty("weixinid")
	private String weiXinId;
	private String avatar;
	private Integer status;
	private Integer enable;
	@JsonProperty("extattr")
	private ExtAttr extAttr;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long[] getDepartIds() {
		return departIds;
	}

	public void setDepartIds(Long[] departIds) {
		this.departIds = departIds;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWeiXinId() {
		return weiXinId;
	}

	public void setWeiXinId(String weiXinId) {
		this.weiXinId = weiXinId;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getEnable() {
		return enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	public ExtAttr getExtAttr() {
		return extAttr;
	}

	public void setExtAttr(ExtAttr extAttr) {
		this.extAttr = extAttr;
	}

	@Override
	public String toString() {
		return "Member [userId=" + userId + ", name=" + name + ", departIds=" + Arrays.toString(departIds) + ", position=" + position + ", mobile=" + mobile + ", gender=" + gender + ", tel="
				+ tel + ", email=" + email + ", weiXinId=" + weiXinId + ", avatar=" + avatar + ", status=" + status + ", enable=" + enable + ", extAttr=" + extAttr + "]";
	}

	public static class ExtAttr {
		ArrayList<Attr> attrs = new ArrayList<Member.Attr>();

		public List<Attr> getAttrs() {
			return attrs;
		}

		public void setAttrs(ArrayList<Attr> attrs) {
			this.attrs = attrs;
		}

		@Override
		public String toString() {
			return "ExtAttr [attrs=" + attrs + "]";
		}

	}

	public static class Attr {

		private String name;
		private String value;
		
		public Attr() {
		}

		public Attr(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Attr [name=" + name + ", value=" + value + "]";
		}

	}
}
