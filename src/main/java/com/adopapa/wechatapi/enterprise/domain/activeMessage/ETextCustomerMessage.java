package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 消息内容，最长不超过2048个字节，注意：主页型应用推送的文本消息在微信端最多只显示20个字（包含中英文
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ETextCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = 8965359542532067175L;

	private Text text;

	public ETextCustomerMessage() {
		super();
		setMessageType(TEXT_MESSAGE_TYPE);
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}
	

	@Override
	public String toString() {
		return "ETextCustomerMessage [text=" + text + ", toString()=" + super.toString() + "]";
	}

	public static class Text {
		private String content;
		
		public Text() {
		}
		
		public Text(String content) {
			setContent(content);
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		@Override
		public String toString() {
			return "Text [content=" + content + "]";
		}

	}

}
