package com.adopapa.wechatapi.enterprise.domain;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;

//errcode 	请求错误码，0表示成功
//errmsg 	请求错误信息
//status 	任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
//type 	操作类型，字节串，目前分别有：
//1. sync_user(增量更新成员)
//2. replace_user(全量覆盖成员)
//3. replace_party(全量覆盖部门)
//total 	任务运行总条数
//percentage 	目前运行百分比，当任务完成时为100
//remaintime 	预估剩余时间（单位：分钟），当任务完成时为0
//result 	详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效 
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobResult extends ResultMessage {

	private static final long serialVersionUID = 2505637127417274077L;
	// {
	// "errcode": 0,
	// "errmsg": "ok",
	// "status": 1,
	// "type": "replace_user",
	// "total": 3,
	// "percentage": 33,
	// "remaintime": 1,
	// "result": [{},{}]
	// }

	private int Status;
	private String type;
	private long total;
	private int percentage;
	@JsonProperty("remaintime")
	private int remainTime;
	@JsonProperty("result")
	private ArrayList<TaskResult> results = new ArrayList<TaskResult>();

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getRemainTime() {
		return remainTime;
	}

	public void setRemainTime(int remainTime) {
		this.remainTime = remainTime;
	}

	public ArrayList<TaskResult> getResults() {
		return results;
	}

	public void setResults(ArrayList<TaskResult> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "JobResult [Status=" + Status + ", type=" + type + ", total=" + total + ", percentage=" + percentage + ", remainTime=" + remainTime + ", results=" + results + ", toString()="
				+ super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class TaskResult extends ResultMessage {

		private static final long serialVersionUID = -4475252689454649435L;
		// action 操作类型（按位或）：1 新建部门 ，2 更改部门名称， 4 移动部门， 8 修改部门排序
		// partyid 部门ID
		// errcode 该部门对应操作的结果错误码
		// errmsg 错误信息，例如无权限错误，键值冲突，格式错误等

		// action 操作类型（按位或）：1 表示修改，2 表示新增
		// userid 成员UserID。对应管理端的帐号
		// errcode 该成员对应操作的结果错误码
		// errmsg 错误信息，例如无权限错误，键值冲突，格式错误等

		private int action;
		@JsonProperty("userid")
		private String userId;
		@JsonProperty("partyid")
		private String partyId;

		public int getAction() {
			return action;
		}

		public void setAction(int action) {
			this.action = action;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getPartyId() {
			return partyId;
		}

		public void setPartyId(String partyId) {
			this.partyId = partyId;
		}

		@Override
		public String toString() {
			return "TaskResult [action=" + action + ", userId=" + userId + ", partyId=" + partyId + "]";
		}

	}

}
