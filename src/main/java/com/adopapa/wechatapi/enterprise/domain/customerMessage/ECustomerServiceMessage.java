package com.adopapa.wechatapi.enterprise.domain.customerMessage;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.enterprise.domain.activeMessage.EFileCustomerMessage.File;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.EImageCustomerMessage.Image;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.ETextCustomerMessage.Text;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.EVoiceCustomerMessage.Voice;

/**
 * 该接口用于向客服人员发送消息，支持文本、图片、文件消息。sender和receiver有且只有一个类型为kf。当receiver为kf时，
 * 表示向客服发送用户咨询的问题消息。当sender为kf时，表示客服从其它IM工具回复客户，并同步消息到客服的微信上
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ECustomerServiceMessage implements Serializable {

	private static final long serialVersionUID = 3063241359726117035L;

	public final static String TEXT_MESSAGE_TYPE = "text";
	public final static String IMAGE_MESSAGE_TYPE = "image";
	public final static String FILE_MESSAGE_TYPE = "file";
	public final static String VOICE_MESSAGE_TYPE = "voice";

	@JsonProperty("msgtype")
	private String messageType;
	private Operator sender;
	private Operator receiver;
	private Text text;
	private Image image;
	private File file;
	private Voice voice;

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Operator getSender() {
		return sender;
	}

	public void setSender(Operator sender) {
		this.sender = sender;
	}

	public Operator getReceiver() {
		return receiver;
	}

	public void setReceiver(Operator receiver) {
		this.receiver = receiver;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Voice getVoice() {
		return voice;
	}

	public void setVoice(Voice voice) {
		this.voice = voice;
	}

	public static ECustomerServiceMessage getTextCustomerServiceMessage(String senderId, String senderType, String receiverId, String receiverType, String content) {
		ECustomerServiceMessage customerServiceMessage = new ECustomerServiceMessage();
		Operator sender = new Operator(senderId, senderType);
		Operator receiver = new Operator(receiverId, receiverType);
		customerServiceMessage.setSender(sender);
		customerServiceMessage.setReceiver(receiver);
		customerServiceMessage.setMessageType(TEXT_MESSAGE_TYPE);
		customerServiceMessage.setText(new Text(content));
		return customerServiceMessage;

	}

	public static ECustomerServiceMessage getImageCustomerServiceMessage(String senderId, String senderType, String receiverId, String receiverType, String mediaId) {
		ECustomerServiceMessage customerServiceMessage = new ECustomerServiceMessage();
		Operator sender = new Operator(senderId, senderType);
		Operator receiver = new Operator(receiverId, receiverType);
		customerServiceMessage.setSender(sender);
		customerServiceMessage.setReceiver(receiver);
		customerServiceMessage.setMessageType(IMAGE_MESSAGE_TYPE);
		Image image = new Image();
		image.setMediaId(mediaId);
		customerServiceMessage.setImage(image);
		return customerServiceMessage;

	}

	public static ECustomerServiceMessage getFileCustomerServiceMessage(String senderId, String senderType, String receiverId, String receiverType, String mediaId) {
		ECustomerServiceMessage customerServiceMessage = new ECustomerServiceMessage();
		Operator sender = new Operator(senderId, senderType);
		Operator receiver = new Operator(receiverId, receiverType);
		customerServiceMessage.setSender(sender);
		customerServiceMessage.setReceiver(receiver);
		customerServiceMessage.setMessageType(IMAGE_MESSAGE_TYPE);
		File file = new File();
		file.setMediaId(mediaId);
		customerServiceMessage.setFile(file);
		return customerServiceMessage;

	}

	public static ECustomerServiceMessage getVoiceCustomerServiceMessage(String senderId, String senderType, String receiverId, String receiverType, String mediaId) {
		ECustomerServiceMessage customerServiceMessage = new ECustomerServiceMessage();
		Operator sender = new Operator(senderId, senderType);
		Operator receiver = new Operator(receiverId, receiverType);
		customerServiceMessage.setSender(sender);
		customerServiceMessage.setReceiver(receiver);
		customerServiceMessage.setMessageType(IMAGE_MESSAGE_TYPE);
		Voice voice = new Voice();
		voice.setMediaId(mediaId);
		customerServiceMessage.setVoice(voice);
		return customerServiceMessage;

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Operator implements Serializable {

		private static final long serialVersionUID = 5020707132608950817L;

		// 用户类型，有下面几种
		// 1. kf
		// 客服
		// 2. userid
		// 客户，企业员工userid
		// 3. openid
		// 客户，公众号openid

		public final static String KF_OPERATOR_TYPE = "kf";
		public final static String USERID_OPERATOR_TYPE = "userid";
		public final static String OPENID_OPERATOR_TYPE = "openid";

		private String id;
		private String type;

		public Operator(String id, String type) {
			super();
			this.id = id;
			this.type = type;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return "Operator [id=" + id + ", type=" + type + "]";
		}

	}

}
