package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EFileCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = 5626685870901091292L;

	private File file;

	public EFileCustomerMessage() {
		super();
		setMessageType(FILE_MESSAGE_TYPE);
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public void setFile(String mediaId) {
		File file = new File();
		file.setMediaId(mediaId);
		this.file = file;
	}
	

	@Override
	public String toString() {
		return "EFileCustomerMessage [file=" + file + ", toString()=" + super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class File {
		@JsonProperty("media_id")
		private String mediaId;
		
		public File() {
			
		}
		
		public File(String mediaId) {
			setMediaId(mediaId);
		}

		public String getMediaId() {
			return mediaId;
		}

		public void setMediaId(String mediaId) {
			this.mediaId = mediaId;
		}

		@Override
		public String toString() {
			return "File [mediaId=" + mediaId + "]";
		}

	}

}
