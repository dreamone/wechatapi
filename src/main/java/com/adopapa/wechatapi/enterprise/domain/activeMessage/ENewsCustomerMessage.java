package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.adopapa.wechatapi.domain.message.Article;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ENewsCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = -4336479929601014311L;

	private News news;

	public ENewsCustomerMessage() {
		super();
		setMessageType(NEWS_MESSAGE_TYPE);
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public String toString() {
		return "ENewsCustomerMessage [news=" + news + ", toString()=" + super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class News {
		private ArrayList<Article> articles = new ArrayList<Article>();

		public ArrayList<Article> getArticles() {
			return articles;
		}

		public void setArticles(ArrayList<Article> articles) {
			this.articles = articles;
		}

		@Override
		public String toString() {
			return "News [articles=" + articles + "]";
		}

	}

}
