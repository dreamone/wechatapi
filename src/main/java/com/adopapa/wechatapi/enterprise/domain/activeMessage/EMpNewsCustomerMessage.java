package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.message.mass.MpNews.Article;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EMpNewsCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = -1733844997983238281L;

	@JsonProperty("mpnews")
	private MpNews mpNews;

	public EMpNewsCustomerMessage() {
		super();
		setMessageType(MPNEWS_MESSAGE_TYPE);
	}

	public MpNews getMpNews() {
		return mpNews;
	}

	public void setMpNews(MpNews mpNews) {
		this.mpNews = mpNews;
	}

	@Override
	public String toString() {
		return "EMpNewsCustomerMessage [mpNews=" + mpNews + ", toString()=" + super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class MpNews {
		private ArrayList<Article> articles = new ArrayList<Article>();

		public ArrayList<Article> getArticles() {
			return articles;
		}

		public void setArticles(ArrayList<Article> articles) {
			this.articles = articles;
		}

		@Override
		public String toString() {
			return "MpNews [articles=" + articles + "]";
		}

	}

}
