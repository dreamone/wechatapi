package com.adopapa.wechatapi.enterprise.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tag extends ResultMessage {

	private static final long serialVersionUID = -4559728736937419495L;

	@JsonProperty("tagid")
	private Long tagId;
	
	@JsonProperty("tagname")
	private String tagName;

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	
	
	

}
