package com.adopapa.wechatapi.enterprise.domain.message;

import java.util.Map;

import com.adopapa.wechatapi.domain.message.TextRequestMessage;

/**
 * ToUserName 企业号CorpID FromUserName 成员UserID CreateTime 消息创建时间（整型） MsgType
 * 消息类型，此时固定为：text Content 文本消息内容 MsgId 消息id，64位整型 AgentID 企业应用的id，整型。
 */
public class ETextRequestMessage extends TextRequestMessage {

	/**
	 * 企业应用id
	 */
	private int agentId;

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		super.fromMap(map);
		setAgentId(agentId);
	}

}
