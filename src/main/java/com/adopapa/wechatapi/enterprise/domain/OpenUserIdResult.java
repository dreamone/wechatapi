package com.adopapa.wechatapi.enterprise.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;

/**
 * 微信支付
 * 在接口使用上，企业号与服务号、订阅号则有所区别，请开发者注意：
 * 1. 由于企业号标识内部成员使用的是userid，因此在使用微信支付下单时，需要调用
 * （userid转换openid接口），将userid转换成openid进行支付。在调用支付下单接口时，APPID请使用企业号的corpid。
 * 2. 如需让企业号成员外的用户进行微信支付，请使用企业号的 （OAuth2验证接口）
 * ，若获取不到userid，即当前微信用户不在企业号内，则会返回该用户对应此企业号的openid。
 */

/**
 * 微信现金红包
 * 
 * 企业号开通微信支付后，即可使用微信红包功能。
 * 在使用微信红包前，请仔细阅读以下流程：
 * 确定需要发放红包的企业号应用和成员名单；
 * 使用企业号（userid转openid接口），将应用ID和成员userid转换成红包参数中要求的appid和openid；
 * 注意：在使用userid转openid接口时，请务必传参agentid来获取对应的appid，否则会导致红包发送失败
 */

/**
 * 企业付款
 * 
 * 同样，企业号在开通了微信支付之后，就可以使用企业付款功能，企业付款是微信支付提供给企业向普通个人用户转账的功能。其能支持单笔交易金额远大于微信红包，
 * 且无需用户领取，直接存到用户的微信零钱包里。 在使用企业号企业付款功能前，你同样需要进行参数转换： 确定需要企业付款的成员；
 * 调用（userid转openid接口），将成员userid转换成openid；
 * （注意：在使用userid转openid接口时，无需传参agentid，在使用企业付款时appid即为企业号的corpid）
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenUserIdResult extends ResultMessage {

	private static final long serialVersionUID = 3033838499358031871L;
	/**
	 * 参数 说明 UserId 成员UserID DeviceId 手机设备号(由微信在安装时随机生成，删除重装会改变，升级不受影响)
	 * 非企业成员的标识，对当前企业号唯一
	 */
	@JsonProperty("UserId")
	private String userId;
	@JsonProperty("OpenId")
	private String openId;
	@JsonProperty("DeviceId")
	private String deviceId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return "OpenUserIdResult [userId=" + userId + ", openId=" + openId + ", deviceId=" + deviceId + "]";
	}

}
