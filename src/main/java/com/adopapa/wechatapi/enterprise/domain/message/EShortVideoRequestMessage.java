package com.adopapa.wechatapi.enterprise.domain.message;

import java.util.Map;

import com.adopapa.wechatapi.domain.message.ShortVideoRequestMessage;

public class EShortVideoRequestMessage extends ShortVideoRequestMessage {

	/**
	 * 企业应用id
	 */
	private int agentId;

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		super.fromMap(map);
		setAgentId(agentId);
	}
}
