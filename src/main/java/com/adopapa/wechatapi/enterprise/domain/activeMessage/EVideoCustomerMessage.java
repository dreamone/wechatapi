package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EVideoCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = 7001594820693065404L;

	private Video video;

	public EVideoCustomerMessage() {
		super();
		setMessageType(VIDEO_MESSAGE_TYPE);
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	@Override
	public String toString() {
		return "EVideoCustomerMessage [video=" + video + ", toString()=" + super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Video {
		@JsonProperty("media_id")
		private String mediaId;
		private String title;
		private String description;

		public String getMediaId() {
			return mediaId;
		}

		public void setMediaId(String mediaId) {
			this.mediaId = mediaId;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return "Video [mediaId=" + mediaId + ", title=" + title + ", description=" + description + "]";
		}

	}

}
