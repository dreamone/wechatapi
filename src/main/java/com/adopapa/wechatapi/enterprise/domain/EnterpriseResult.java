package com.adopapa.wechatapi.enterprise.domain;

import java.util.ArrayList;

import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.domain.common.ResultMessage;
import com.adopapa.wechatapi.enterprise.domain.chat.Chat;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnterpriseResult extends ResultMessage {

	private static final long serialVersionUID = -1429681638170548985L;

	@Transient
	private String id;

	@JsonProperty("department")
	private ArrayList<Department> departments = new ArrayList<Department>();

	@JsonProperty("userlist")
	private ArrayList<Member> members = new ArrayList<Member>();

	@JsonProperty("tags")
	private ArrayList<Tag> tags = new ArrayList<Tag>();

	// message
	// "invaliduser": "UserID1",
	// "invalidparty":"PartyID1",
	// "invalidtag":"TagID1"
	@JsonProperty("invaliduser")
	private ArrayList<String> invalidUsers = new ArrayList<String>();
	@JsonProperty("invalidlist")
	private ArrayList<String> invalidlists = new ArrayList<String>();
	@JsonProperty("invalidparty")
	private ArrayList<String> invalidParties = new ArrayList<String>();
	@JsonProperty("invalidtag")
	private ArrayList<String> invalidTags = new ArrayList<String>();

	@JsonProperty("userid")
	private String userId;
	@JsonProperty("appid")
	private String appId;
	@JsonProperty("openid")
	private String openId;

	// 异步任务id
	@JsonProperty("jobid")
	private String jobId;

	// 回话信息
	@JsonProperty("chat_info")
	private Chat chatInfo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(ArrayList<Department> departments) {
		this.departments = departments;
	}

	public ArrayList<Member> getMembers() {
		return members;
	}

	public void setMembers(ArrayList<Member> members) {
		this.members = members;
	}

	public ArrayList<Tag> getTags() {
		return tags;
	}

	public void setTags(ArrayList<Tag> tags) {
		this.tags = tags;
	}

	public ArrayList<String> getInvalidUsers() {
		return invalidUsers;
	}

	public void setInvalidUsers(ArrayList<String> invalidUsers) {
		this.invalidUsers = invalidUsers;
	}

	public ArrayList<String> getInvalidlists() {
		return invalidlists;
	}

	public void setInvalidlists(ArrayList<String> invalidlists) {
		this.invalidlists = invalidlists;
	}

	public ArrayList<String> getInvalidParties() {
		return invalidParties;
	}

	public void setInvalidParties(ArrayList<String> invalidParties) {
		this.invalidParties = invalidParties;
	}

	public ArrayList<String> getInvalidTags() {
		return invalidTags;
	}

	public void setInvalidTags(ArrayList<String> invalidTags) {
		this.invalidTags = invalidTags;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Chat getChatInfo() {
		return chatInfo;
	}

	public void setChatInfo(Chat chatInfo) {
		this.chatInfo = chatInfo;
	}

}
