package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EImageCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = -5796240042124734906L;

	private Image image;

	public EImageCustomerMessage() {
		super();
		setMessageType(IMAGE_MESSAGE_TYPE);
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	public void setImage(String mediaId) {
		Image image = new Image();
		image.setMediaId(mediaId);
		this.image = image;
	}
	

	@Override
	public String toString() {
		return "EImageCustomerMessage [image=" + image + ", toString()=" + super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Image {
		@JsonProperty("media_id")
		private String mediaId;
		
		public Image() {
		}
		
		public Image(String mediaId) {
			setMediaId(mediaId);
		}

		public String getMediaId() {
			return mediaId;
		}

		public void setMediaId(String mediaId) {
			this.mediaId = mediaId;
		}

		@Override
		public String toString() {
			return "Image [mediaId=" + mediaId + "]";
		}

	}
}
