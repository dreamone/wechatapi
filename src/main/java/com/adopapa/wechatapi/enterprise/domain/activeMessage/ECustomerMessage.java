package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 消息型应用支持文本、图片、语音、视频、文件、图文等消息类型。主页型应用只支持文本消息类型，且文本长度不超过20个字
 * touser 否 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送 
 * toparty 否 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数 
 * totag 否 标签ID列表，多个接收者用‘|’分隔。当touser为@all时忽略本参数
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class ECustomerMessage implements Serializable {

	private static final long serialVersionUID = -2601891896361255844L;

	// 1 text消息
	// 2 image消息
	// 3 voice消息
	// 4 video消息
	// 5 file消息
	// 6 news消息
	// 7 mpnews消息

	public final static String TEXT_MESSAGE_TYPE = "text";
	public final static String IMAGE_MESSAGE_TYPE = "image";
	public final static String VOICE_MESSAGE_TYPE = "voice";
	public final static String VIDEO_MESSAGE_TYPE = "video";
	public final static String FILE_MESSAGE_TYPE = "file";
	public final static String NEWS_MESSAGE_TYPE = "news";
	public final static String MPNEWS_MESSAGE_TYPE = "mpnews";

	// {
	// "touser": "UserID1|UserID2|UserID3",
	// "toparty": " PartyID1 | PartyID2 ",
	// "totag": " TagID1 | TagID2 ",
	// "msgtype": "text",
	// "agentid": 1,
	// "text": {
	// "content": "Holiday Request For Pony(http://xxxxx)"
	// },
	// "safe":"0"
	// }

	@JsonProperty("touser")
	private String toUser;
	@JsonProperty("toparty")
	private String toParty;
	@JsonProperty("totag")
	private String toTag;
	@JsonProperty("msgtype")
	private String messageType;
	@JsonProperty("agentid")
	private int agentId;
	private String safe;

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getToParty() {
		return toParty;
	}

	public void setToParty(String toParty) {
		this.toParty = toParty;
	}

	public String getToTag() {
		return toTag;
	}

	public void setToTag(String toTag) {
		this.toTag = toTag;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public String getSafe() {
		return safe;
	}

	public void setSafe(String safe) {
		this.safe = safe;
	}

	@Override
	public String toString() {
		return "ECustomerMessage [toUser=" + toUser + ", toParty=" + toParty + ", toTag=" + toTag + ", messageType=" + messageType + ", agentId=" + agentId + ", safe=" + safe + "]";
	}

}
