package com.adopapa.wechatapi.enterprise.domain;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Department implements Serializable {

	private static final long serialVersionUID = -2970552606620248971L;
	/**
	 * name 是 部门名称。长度限制为1~64个字节，字符不能包括\:*?"<>｜ 
	 * parentid 是 父亲部门id。根部门id为1
	 * order 否 在父部门中的次序值。order值小的排序靠前。 
	 * id 否 部门id，整型。指定时必须大于1，不指定时则自动生成
	 */
	private Long id;
	private String name;
	@JsonProperty("parentid")
	private Long parentId;
	private Long order;
	
	
	
	
	
	public Long getId() {
		return id;
	}





	public void setId(Long id) {
		this.id = id;
	}





	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public Long getParentId() {
		return parentId;
	}





	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}





	public Long getOrder() {
		return order;
	}





	public void setOrder(Long order) {
		this.order = order;
	}





	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", parentId=" + parentId + ", order=" + order + "]";
	}
	
	
	
	

}
