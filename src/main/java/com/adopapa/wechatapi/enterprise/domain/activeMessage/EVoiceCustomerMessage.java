package com.adopapa.wechatapi.enterprise.domain.activeMessage;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EVoiceCustomerMessage extends ECustomerMessage {

	private static final long serialVersionUID = 1964058929219119968L;

	private Voice voice;

	public EVoiceCustomerMessage() {
		super();
		setMessageType(VOICE_MESSAGE_TYPE);
	}

	public Voice getVoice() {
		return voice;
	}

	public void setVoice(Voice voice) {
		this.voice = voice;
	}

	@Override
	public String toString() {
		return "EVoiceCustomerMessage [voice=" + voice + ", toString()=" + super.toString() + "]";
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Voice {
		@JsonProperty("media_id")
		private String mediaId;
		
		public Voice() {
		}
		
		public Voice(String mediaId) {
			setMediaId(mediaId);
		}

		public String getMediaId() {
			return mediaId;
		}

		public void setMediaId(String mediaId) {
			this.mediaId = mediaId;
		}

		@Override
		public String toString() {
			return "Voice [mediaId=" + mediaId + "]";
		}

	}
}
