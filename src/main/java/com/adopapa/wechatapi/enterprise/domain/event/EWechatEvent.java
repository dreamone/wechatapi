package com.adopapa.wechatapi.enterprise.domain.event;

import java.util.Map;

import com.adopapa.wechatapi.domain.event.Event;
import com.adopapa.wechatapi.domain.menu.event.ScanCodeInfo;
import com.adopapa.wechatapi.domain.menu.event.SendLocationInfo;
import com.adopapa.wechatapi.domain.menu.event.SendPicsInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

//<xml>
//<ToUserName><![CDATA[wx28dbb14e37208abe]]></ToUserName>
//<FromUserName><![CDATA[FromUser]]></FromUserName>
//<CreateTime>1425284517</CreateTime>
//<MsgType><![CDATA[event]]></MsgType>
//<Event><![CDATA[batch_job_result]]></Event>
//<BatchJob>
//<JobId><![CDATA[S0MrnndvRG5fadSlLwiBqiDDbM143UqTmKP3152FZk4]]></JobId>
//<JobType><![CDATA[sync_user]]></JobType>
//<ErrCode>0</ErrCode>
//<ErrMsg><![CDATA[ok]]></ErrMsg>
//</BatchJob>
//</xml>

@XStreamAlias("xml")
public class EWechatEvent extends Event {
	
	/**
	 * for CLICK, VIEW, Subscribe, Unsubscribe
	 */

	/**
	 * for LOCATION
	 */
	private double latitude;
	private double longitude;
	private double precision;

	/**
	 * for SCAN
	 */
	private String ticket;

	/**
	 * for templateMessage
	 */
	private String status;

	@XStreamAlias("EventKey")
	private String eventKey;

	/**
	 * for scancode_push, scancode_waitmsg
	 */
	@XStreamAlias("ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;

	/**
	 * pic_sysphoto, pic_photo_or_album, pic_weixin
	 */
	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	/**
	 * location_select
	 */
	@XStreamAlias("SendLocationInfo")
	private SendLocationInfo sendLocationInfo;

	@XStreamAlias("MenuId")
	private int menuId;


	@XStreamAlias("BatchJob")
	private BatchJob batchJob;

	@XStreamAlias("AgentID")
	private int agentId;

	public BatchJob getBatchJob() {
		return batchJob;
	}

	public void setBatchJob(BatchJob batchJob) {
		this.batchJob = batchJob;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}

	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public SendLocationInfo getSendLocationInfo() {
		return sendLocationInfo;
	}

	public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
		this.sendLocationInfo = sendLocationInfo;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	@Override
	public void fromMap(Map<String, String> map) {
		
	}

	

	// 1. sync_user(增量更新成员)
	// 2. replace_user(全量覆盖成员)
	// 3. invite_user(邀请成员关注)
	// 4. replace_party(全量覆盖部门)
	public static class BatchJob {
		@XStreamAlias("JobId")
		private String jobId;
		@XStreamAlias("JobType")
		private String jobType;
		@XStreamAlias("ErrCode")
		private int errorCode;
		@XStreamAlias("ErrMsg")
		private String errorMessage;

		public String getJobId() {
			return jobId;
		}

		public void setJobId(String jobId) {
			this.jobId = jobId;
		}

		public String getJobType() {
			return jobType;
		}

		public void setJobType(String jobType) {
			this.jobType = jobType;
		}

		public int getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		@Override
		public String toString() {
			return "BatchJob [jobId=" + jobId + ", jobType=" + jobType + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
		}
	}


}
