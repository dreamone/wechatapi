package com.adopapa.wechatapi.enterprise.domain.chat;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.adopapa.wechatapi.enterprise.domain.activeMessage.EFileCustomerMessage.File;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.EImageCustomerMessage.Image;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.ETextCustomerMessage.Text;
import com.adopapa.wechatapi.enterprise.domain.activeMessage.EVoiceCustomerMessage.Voice;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatMessage implements Serializable {

	private static final long serialVersionUID = 5163104668738009957L;

	public final static String CHAT_TEXT_MESSAGE_TYPE = "text";
	public final static String CHAT_IMAGE_MESSAGE_TYPE = "image";
	public final static String CHAT_VOICE_MESSAGE_TYPE = "voice";
	public final static String CHAT_FILE_MESSAGE_TYPE = "file";
	public final static String CHAT_LINK_MESSAGE_TYPE = "link";
	public final static String CHAT_RECEIVER_SINGLE_TYPE = "single";
	public final static String CHAT_RECEIVER_GROUP_TYPE = "group";

	private Receiver receiver;
	private String sender;
	@JsonProperty("msgtype")
	private String messageType;
	private Text text;
	private Image image;
	private File file;
	private Voice voice;
	private Link link;

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Voice getVoice() {
		return voice;
	}

	public void setVoice(Voice voice) {
		this.voice = voice;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}
	
	@Override
	public String toString() {
		return "ChatMessage [receiver=" + receiver + ", sender=" + sender + ", messageType=" + messageType + ", text=" + text + ", image=" + image + ", file=" + file + ", voice=" + voice
				+ ", link=" + link + "]";
	}
	
	public static ChatMessage getTextChatMessage(String receiver, String sender, boolean single, String content) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setMessageType(CHAT_TEXT_MESSAGE_TYPE);
		chatMessage.setReceiver(new Receiver(single ? CHAT_RECEIVER_SINGLE_TYPE : CHAT_RECEIVER_GROUP_TYPE, receiver));
		chatMessage.setSender(sender);
		chatMessage.setText(new Text(content));
		return chatMessage;
	}
	

	public static ChatMessage getImageChatMessage(String receiver, String sender, boolean single, String mediaId) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setMessageType(CHAT_IMAGE_MESSAGE_TYPE);
		chatMessage.setReceiver(new Receiver(single ? CHAT_RECEIVER_SINGLE_TYPE : CHAT_RECEIVER_GROUP_TYPE, receiver));
		chatMessage.setSender(sender);
		chatMessage.setImage(new Image(mediaId));
		return chatMessage;
	}
	
	public static ChatMessage getFileChatMessage(String receiver, String sender, boolean single, String mediaId) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setMessageType(CHAT_FILE_MESSAGE_TYPE);
		chatMessage.setReceiver(new Receiver(single ? CHAT_RECEIVER_SINGLE_TYPE : CHAT_RECEIVER_GROUP_TYPE, receiver));
		chatMessage.setSender(sender);
		chatMessage.setFile(new File(mediaId));
		return chatMessage;
	}
	
	public static ChatMessage getVoiceChatMessage(String receiver, String sender, boolean single, String mediaId) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setMessageType(CHAT_VOICE_MESSAGE_TYPE);
		chatMessage.setReceiver(new Receiver(single ? CHAT_RECEIVER_SINGLE_TYPE : CHAT_RECEIVER_GROUP_TYPE, receiver));
		chatMessage.setSender(sender);
		chatMessage.setVoice(new Voice(mediaId));
		return chatMessage;
	}
	
	/**
	 * description 不是必须的 
	 */
	public static ChatMessage getLinkChatMessage(String receiver, String sender, boolean single, String title, String url, String mediaId, String description) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setMessageType(CHAT_LINK_MESSAGE_TYPE);
		chatMessage.setReceiver(new Receiver(single ? CHAT_RECEIVER_SINGLE_TYPE : CHAT_RECEIVER_GROUP_TYPE, receiver));
		chatMessage.setSender(sender);
		Link link = new Link();
		link.setTitle(title);
		link.setUrl(url);
		link.setThumbMediaId(mediaId);
		link.setDescription(description);
		chatMessage.setLink(link);
		return chatMessage;
	}
	
	
	
	
	
	

	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Receiver implements Serializable {
		private static final long serialVersionUID = 8451059793438436011L;
		// type 是 接收人类型：single|group，分别表示：群聊|单聊
		// id 是 接收人的值，为userid|chatid，分别表示：成员id|会话id
		private String type;
		private String id;

		public Receiver() {
		}

		public Receiver(String type, String id) {
			this.type = type;
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@Override
		public String toString() {
			return "Receiver [type=" + type + ", id=" + id + "]";
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Link implements Serializable {
		private static final long serialVersionUID = 3401962977157385028L;
		// "title": "title01",
		// "description":"link消息描述",
		// "url":"http://www.qq.com",
		// "thumb_media_id":
		// "177fIcVBfOLYa703hzBByU1EH3_sdp4hyyaxN4Gfdc-o66vG7k-lXgEacQqfuCcJ-VbZnPlUKJDF8ig_8Zgh6-g"
		// title 是 消息标题，不超过128个字节
		// description 否 消息描述，不超过512个字节
		// url 是 跳转的url
		// thumb_media_id 是 图片media_id，可以调用上传素材文件接口获取
		private String title;
		private String description;
		private String url;
		@JsonProperty("thumb_media_id")
		private String thumbMediaId;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getThumbMediaId() {
			return thumbMediaId;
		}

		public void setThumbMediaId(String thumbMediaId) {
			this.thumbMediaId = thumbMediaId;
		}

		@Override
		public String toString() {
			return "Link [title=" + title + ", description=" + description + ", url=" + url + ", thumbMediaId=" + thumbMediaId + "]";
		}

	}

}
