package com.adopapa.wechatapi.xstream;

import java.io.IOException;
import java.io.InputStream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XstreamUtil {

	@SuppressWarnings("unchecked")
	public static <T> T XML2Bean(InputStream input, Class<T> clazz) throws IOException {

		XStream xstream = new XStream(new DomDriver());
//		xstream.alias("item", PicInfo.class);
		xstream.processAnnotations(clazz);

		return (T) xstream.fromXML(input);
	}

	public static void main(String[] args) {
		Alipay alipay = null;
		try {
			InputStream inputStream = XstreamUtil.class.getResourceAsStream("alipay.xml");
			alipay = XstreamUtil.XML2Bean(inputStream, Alipay.class);
//			alipay = XstreamUtil.XML2Bean(new FileInputStream("alipay.xml"), Alipay.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(alipay.getIsSuccess());
		System.out.println(alipay.getRequest().getParam().get(0).getContent());
	}
}