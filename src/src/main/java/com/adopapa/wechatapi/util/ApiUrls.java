package com.adopapa.wechatapi.util;

import java.text.MessageFormat;
import java.util.Date;

public class ApiUrls {

	/**
	 * 获取accessToken
	 */
	public final static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";
	/**
	 * 获取用户基本信息
	 */
	public final static String GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}&lang={2}";
	
	/**
	 * 上传多媒体文件
	 */
	public final static String UPLOAD_MEDIA_FILE_URL = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token={0}&type={1}";
	/**
	 * 下载多媒体问题
	 */
	public final static String GET_MEDIA_FILE_URL = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={0}&media_id={1}";
	/**
	 * 创建菜单
	 */
	public final static String MENU_CREATE_URL = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}";
	/**
	 * 查询菜单
	 */
	public final static String MENU_GET_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}";
	/**
	 * 删除菜单
	 */
	public final static String MENU_DELETE_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token={0}";
	/**
	 * 更新用户备注
	 */
	public final static String UPDATE_REMARK = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token={0}";
	/**
	 * 获取code
	 */
	public final static String SCOPE_CODE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type={2}&scope={3}&state={4}#wechat_redirect";
	/**
	 * 获取网页accessToken
	 */
	public final static String WEB_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type={3}";
	/**
	 * 刷新accessToken
	 */
	public final static String REFRESH_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={0}&grant_type={1}&refresh_token={2}";
	/**
	 * 获取web用户信息
	 */
	public final static String GET_WEB_USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token={0}&openid={1}&lang={2}";
	/**
	 * 创建二维码
	 */
	public final static String CREATE_QRCODE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}";
	/**
	 * 显示二维码
	 */
	public final static String SHOW_QRCODE_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}";
	/**
	 * 发送客服消息
	 */
	public final static String CUSTOMER_MESSAGE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}";
	/**
	 * 发送模板消息
	 */
	public final static String TEMPLATE_MESSAGE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";
	
	/***********************************************高级群发***********************************************************************************************/
	/**
	 * 上传图文素材
	 */
	public final static String UPLOAD_NEWS_URL = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token={0}";
	/**
	 * 上传视频素材
	 */
	public final static String UPLOAD_VIDEO_URL = "https://file.api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token={0}";
	/**
	 * 根据分组群发
	 */
	public final static String MASS_MESSAGE_SENDALL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={0}";
	/**
	 * 根据OpenID列表群发
	 */
	public final static String MASS_MESSAGE_SEND = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={0}";
	/********************************************************************************************************************************************************/
	

	/**
	 * 支付统一下单
	 */
	public final static String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	/**
	 * 
	 * 获取jsapi_ticket
	 */
	public final static String GET_JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={0}&type=jsapi";
	
	public static void main(String[] args) {
		System.out.println(MessageFormat.format(ACCESS_TOKEN_URL, "1", "2"));

		String messageFormat = "lexical error at position {0}, encountered {1}, expected {2}";
		System.out.println(MessageFormat.format(messageFormat, new Date(), 100, 456));
		String stringFormat = "lexical error at position %s, encountered %s, expected %s ";
		System.out.println(String.format(stringFormat, 123, 100, 456));
		
		String accessToken = "qy7ofp2EgKBEti5ntIN8Ua0fA9zXsc17tSdLW223-lzd-n_FMgTWksMqGaNBfE8QgL_XNPCJ4RqhO_ylW7KAiWOWvk1K1Dd0SyvVPgf12Po";
		System.out.println(MessageFormat.format(GET_USER_INFO, accessToken, "2", "3"));
	}

}
