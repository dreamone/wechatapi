package com.adopapa.wechatapi.domain.message.mass;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.adopapa.wechatapi.util.JsonUtil;

public class MpNews {

	private List<MpNews.Article> articles = new LinkedList<MpNews.Article>();

	public List<MpNews.Article> getArticles() {
		return articles;
	}

	public void setArticles(List<MpNews.Article> articles) {
		this.articles = articles;
	}

	public static class Article {
		// Articles 是 图文消息，一个图文消息支持1到10条图文
		// thumb_media_id 是 图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得
		// author 否 图文消息的作者
		// title 是 图文消息的标题
		// content_source_url 否 在图文消息页面点击“阅读原文”后的页面
		// content 是 图文消息页面的内容，支持HTML标签
		// digest 否 图文消息的描述
		// show_cover_pic 否 是否显示封面，1为显示，0为不显示
		private String thumbMediaId;
		private String author;
		private String title;
		private String content;
		private String contentSourceUrl;
		private String digest;
		private int showCoverPic;

		public String getThumbMediaId() {
			return thumbMediaId;
		}

		public void setThumbMediaId(String thumbMediaId) {
			this.thumbMediaId = thumbMediaId;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getContentSourceUrl() {
			return contentSourceUrl;
		}

		public void setContentSourceUrl(String contentSourceUrl) {
			this.contentSourceUrl = contentSourceUrl;
		}

		public String getDigest() {
			return digest;
		}

		public void setDigest(String digest) {
			this.digest = digest;
		}

		public int getShowCoverPic() {
			return showCoverPic;
		}

		public void setShowCoverPic(int showCoverPic) {
			this.showCoverPic = showCoverPic;
		}

		public boolean validate() {
			return (thumbMediaId != null && title != null && content != null);
		}

	}

	public String toJson() throws IOException {
		String json = JsonUtil.bean2Json(this);
		json = json.replaceAll("thumbMediaId", "thumb_media_id").replaceAll("contentSourceUrl", "content_source_url");
		json = json.replaceAll("showCoverPic", "show_cover_pic");
		return json;
	}

	public static void main(String[] args) throws IOException {
		Article article1 = new Article();
		article1.setAuthor("author");
		article1.setContent("content");
		article1.setContentSourceUrl("contet source url");
		article1.setDigest("digest");
		article1.setShowCoverPic(0);
		article1.setThumbMediaId("thumb media id");
		article1.setTitle("title");
		Article article2 = new Article();
		article2.setAuthor("author");
		article2.setContent("content");
		article2.setContentSourceUrl("contet source url");
		article2.setDigest("digest");
		article2.setShowCoverPic(0);
		article2.setThumbMediaId("thumb media id");
		article2.setTitle("title");
		MpNews mpNews = new MpNews();
		mpNews.articles.add(article1);
		mpNews.articles.add(article2);
		
		System.out.println(mpNews.toJson());

	}

	// {
	// "thumb_media_id":"qI6_Ze_6PtV7svjolgs-rN6stStuHIjs9_DidOHaj0Q-mwvBelOXCFZiq2OsIU-p",
	// "author":"xxx",
	// "title":"Happy Day",
	// "content_source_url":"www.qq.com",
	// "content":"content",
	// "digest":"digest",
	// "show_cover_pic":"1"
	// },
	// {
	// "thumb_media_id":"qI6_Ze_6PtV7svjolgs-rN6stStuHIjs9_DidOHaj0Q-mwvBelOXCFZiq2OsIU-p",
	// "author":"xxx",
	// "title":"Happy Day",
	// "content_source_url":"www.qq.com",
	// "content":"content",
	// "digest":"digest",
	// "show_cover_pic":"0"
	// }

}
