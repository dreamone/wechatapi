package com.adopapa.wechatapi.domain.event;

import java.util.Map;

/**
 * 扫描带参数二维码事件 用户已关注时的事件推送
 */
@Deprecated
public class ScanEvent extends Event {

	private String eventKey;

	private String ticket;

	public ScanEvent() {
		setMsgType("event");
		setEvent(EventType.SCAN.name());
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Override
	public void fromMap(Map<String, String> map) {
		setToUserName(map.get("ToUserName"));
		setFromUserName(map.get("FromUserName"));
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setEventKey(map.get("EventKey"));
		setTicket(map.get("Ticket"));

	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[FromUser]]></FromUserName>
	 * <CreateTime>123456789</CreateTime>
	 * <MsgType><![CDATA[event]]></MsgType> <Event><![CDATA[SCAN]]></Event>
	 * <EventKey><![CDATA[SCENE_VALUE]]></EventKey>
	 * <Ticket><![CDATA[TICKET]]></Ticket> </xml>
	 */

	/**
	 * ToUserName 开发者微信号 FromUserName 发送方帐号（一个OpenID） CreateTime 消息创建时间 （整型）
	 * MsgType 消息类型，event Event 事件类型，SCAN EventKey
	 * 事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id Ticket 二维码的ticket，可用来换取二维码图片
	 */

}
