package com.adopapa.wechatapi.domain.media;

import java.io.BufferedInputStream;
import java.io.Serializable;

import com.adopapa.wechatapi.domain.common.ResultMessage;

public class DownloadMediaResult extends ResultMessage implements Serializable {

	private static final long serialVersionUID = 6707169324834306252L;

	private String mediaId;
	private String fileName;
	private String fullName;
	private String suffix;
	private String contentLength;
	private String contentType;
	private BufferedInputStream inputStream;

	public DownloadMediaResult(int errcode, String errmsg) {
		super(errcode, errmsg);
	}
	
	public DownloadMediaResult() {
		this(0, "ok");
	}
	
	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getContentLength() {
		return contentLength;
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public BufferedInputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(BufferedInputStream inputStream) {
		this.inputStream = inputStream;
	}

}
