package com.adopapa.wechatapi.domain.token;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import com.adopapa.wechatapi.domain.common.ResultMessage;


@MappedSuperclass
public class QrCode extends ResultMessage implements Serializable {

	private static final long serialVersionUID = 7700843220221951540L;

	public QrCode(int errcode, String errmsg) {
		super(errcode, errmsg);
	}
	
	public QrCode() {
		this(0, "ok");
	}

//	
//	ticket	获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
//	expire_seconds	二维码的有效时间，以秒为单位。最大不超过1800。
	// url 二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片
	
	private String ticket;
	
	private int expires;
	
	private String url;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public int getExpire() {
		return expires;
	}

	public void setExpire(int expire) {
		this.expires = expire;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	

}
