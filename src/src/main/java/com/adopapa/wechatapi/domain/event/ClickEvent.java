package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import javax.persistence.MappedSuperclass;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 自定义菜单事件
 * 
 * 用户点击自定义菜单后，微信会把点击事件推送给开发者，请注意，点击菜单弹出子菜单，不会产生上报。 
 * 点击菜单拉取消息时的事件推送
 */
@MappedSuperclass
@XStreamAlias("xml")
@Deprecated
public class ClickEvent extends Event {
	/**
	 * ToUserName 开发者微信号 FromUserName 发送方帐号（一个OpenID） CreateTime 消息创建时间 （整型）
	 * MsgType 消息类型，event Event 事件类型，CLICK EventKey 事件KEY值，与自定义菜单接口中KEY值对应
	 */

	@XStreamAlias("EventKey")
	private String eventKey;

	public ClickEvent() {
		setMsgType("event");
		setEvent(EventType.CLICK.name());
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	@Override
	public void fromMap(Map<String, String> map) {
		setToUserName(map.get("ToUserName"));
		setFromUserName(map.get("FromUserName"));
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setEventKey(map.get("EventKey"));
	}

	@Override
	public String toString() {
		return "ClickEvent [getEventKey()=" + getEventKey() + ", getToUserName()=" + getToUserName() + ", getFromUserName()="
				+ getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType()
				+ ", getEvent()=" + getEvent() + "]";
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[FromUser]]></FromUserName>
	 * <CreateTime>123456789</CreateTime>
	 * <MsgType><![CDATA[event]]></MsgType> <Event><![CDATA[CLICK]]></Event>
	 * <EventKey><![CDATA[EVENTKEY]]></EventKey> </xml>
	 */

	
}
