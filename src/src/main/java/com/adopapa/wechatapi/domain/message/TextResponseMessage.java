package com.adopapa.wechatapi.domain.message;

import java.util.Calendar;

import javax.persistence.MappedSuperclass;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

@MappedSuperclass
public class TextResponseMessage extends  ResponseMessage {
	
	//回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
	private String content;
	
	
	public TextResponseMessage () {
		setMsgType(MessageType.text.name());
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver());
		xstream.alias("xml", this.getClass());
		return xstream.toXML(this);
	}
	
//	public static void main(String[] args) {
//		TextResponseMessage responseMessage = new TextResponseMessage();
//		responseMessage.setCreateTime(Calendar.getInstance().getTime().getTime());
//		responseMessage.setFromUserName("123");
//		responseMessage.setToUserName("234");
//		responseMessage.setContent("222222");
//		System.out.println(responseMessage.toXml());
//	}
	
	

}
