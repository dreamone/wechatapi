package com.adopapa.wechatapi.domain.menu.event;

import java.util.Map;

import com.adopapa.wechatapi.domain.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 弹出系统拍照发图的事件推送
 */
@Deprecated
@XStreamAlias("xml")
public class PicSysPhotoEvent extends Event {
	// ToUserName 开发者微信号
	// FromUserName 发送方帐号（一个OpenID）
	// CreateTime 消息创建时间 （整型）
	// MsgType 消息类型，event
	// Event 事件类型，pic_sysphoto
	// EventKey 事件KEY值，由开发者在创建菜单时设定
	// SendPicsInfo 发送的图片信息
	// Count 发送的图片数量
	// PicList 图片列表
	// PicMd5Sum 图片的MD5值，开发者若需要，可用于验证接收到图片

	// <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
	// <FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
	// <CreateTime>1408090651</CreateTime>
	// <MsgType><![CDATA[event]]></MsgType>
	// <Event><![CDATA[pic_sysphoto]]></Event>
	// <EventKey><![CDATA[6]]></EventKey>
	// <SendPicsInfo><Count>1</Count>
	// <PicList><item><PicMd5Sum><![CDATA[1b5f7c23b5bf75682a53e7b6d163e185]]></PicMd5Sum>
	// </item>
	// </PicList>
	// </SendPicsInfo>
	// </xml>

	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	@XStreamAlias("EventKey")
	private String eventKey;

	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public PicSysPhotoEvent() {
		setMsgType("event");
		setEvent(MenuEventType.pic_sysphoto.name());
	}

	@Override
	public void fromMap(Map<String, String> map) {
		setEventKey(map.get("EventKey"));
	}

	// <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
	// <FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
	// <CreateTime>1408090651</CreateTime>
	// <MsgType><![CDATA[event]]></MsgType>
	// <Event><![CDATA[pic_sysphoto]]></Event>
	// <EventKey><![CDATA[6]]></EventKey>
	// <SendPicsInfo><Count>1</Count>
	// <PicList><item><PicMd5Sum><![CDATA[1b5f7c23b5bf75682a53e7b6d163e185]]></PicMd5Sum>
	// </item>
	// </PicList>
	// </SendPicsInfo>
	// </xml>

	// ToUserName 开发者微信号
	// FromUserName 发送方帐号（一个OpenID）
	// CreateTime 消息创建时间 （整型）
	// MsgType 消息类型，event
	// Event 事件类型，pic_sysphoto
	// EventKey 事件KEY值，由开发者在创建菜单时设定
	// SendPicsInfo 发送的图片信息
	// Count 发送的图片数量
	// PicList 图片列表
	// PicMd5Sum 图片的MD5值，开发者若需要，可用于验证接收到图片

}
