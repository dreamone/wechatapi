package com.adopapa.wechatapi.domain.message;

public class Voice {
	
	/**
	 * 音消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 * 通过上传多媒体文件，得到的id
	 */
	private String mediaId; 
	
	
	/**
	 * 语音格式，如amr，speex等
	 * for request
	 */
	private String format;


	public String getMediaId() {
		return mediaId;
	}


	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}


	public String getFormat() {
		return format;
	}


	public void setFormat(String format) {
		this.format = format;
	} 
	
	
	

}
