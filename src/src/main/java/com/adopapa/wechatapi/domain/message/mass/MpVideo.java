package com.adopapa.wechatapi.domain.message.mass;

import java.io.IOException;

import com.adopapa.wechatapi.util.JsonUtil;

public class MpVideo {
	// {
	// "media_id":
	// "rF4UdIMfYK3efUfyoddYRMU50zMiRmmt_l0kszupYh_SzrcW5Gaheq05p_lHuOTQ",
	// "title": "TITLE",
	// "description": "Description"
	// }
	private String mediaId;
	private String tilte;
	private String description;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getTilte() {
		return tilte;
	}

	public void setTilte(String tilte) {
		this.tilte = tilte;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String toJson() throws IOException {
		String json = JsonUtil.bean2Json(this);
		json = json.replaceAll("mediaId", "media_id");
		return json;
	}

}
