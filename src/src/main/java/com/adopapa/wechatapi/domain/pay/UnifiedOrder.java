package com.adopapa.wechatapi.domain.pay;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

/**
 * <xml> <appid>wx2421b1c4370ec43b</appid> <attach>支付测试</attach>
 * <body>JSAPI支付测试</body> <mch_id>10000100</mch_id>
 * <nonce_str>1add1a30ac87aa2db72f57a2375d8fec</nonce_str>
 * <notify_url>http://wxpay.weixin.qq.com/pub_v2/pay/notify.v2.php</notify_url>
 * <openid>oUpF8uMuAJO_M2pxb1Q9zNjWeS6o</openid>
 * <out_trade_no>1415659990</out_trade_no>
 * <spbill_create_ip>14.23.150.211</spbill_create_ip> <total_fee>1</total_fee>
 * <trade_type>JSAPI</trade_type> <sign>0CB01533B8C1EF103065174F50BCA001</sign>
 * </xml>
 *
 */
public class UnifiedOrder {
	// 公众账号ID appid
	private String appId;
	// 商户号 mch_id 微信支付分配的商户号
	private String mechantId;
	// 设备号 device_info********终端设备号(门店号或收银设备ID)，注意：PC网页或公众号内支付请传"WEB"
	private String deviceInfo;
	// 随机字符串 nonce_str 随机字符串，不长于32位
	private String nonceStr;
	// 签名 
	private String sign;
	// 商品描述 商品或支付单简要描述
	private String body;
	// 商品详情 *************商品名称明细列表
	private String detail;
	// 附加数据 **********附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
	private String attach;
	// 商户订单号out_trade_no 商户系统内部的订单号,32个字符内、可包含字母,
	private String outTradeNo;
	// 货币类型 fee_type ******* 符合ISO 4217标准的三位字母代码，默认人民币：CNY，
	private String feeType;
	// 总金额 total_fee 订单总金额，只能为整数
	private String totalFee; 
	// 终端IP spbill_create_ip APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
	private String spbillCreateIP;
	// 交易起始时间 time_start******订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。
	private String timeStart;
	// 交易结束时间 time_expire******订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
	private String timeExpire;
	// 商品标记 goods_tag *******
	private String goodsTag;
	// 通知地址notify_url 接收微信支付异步通知回调地址
	private String notifyUrl;
	// 交易类型 trade_type 取值如下：JSAPI，NATIVE，APP
	private String tradeType;
	// 商品Id product_id rade_type=NATIVE，此参数必传。此id为二维码中包含的商品ID，商户自行定义
	private String productId;
	// 用户标识 openid trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识。下单前需要调用【网页授权获取用户信息】接口获取到用户的Openid
	private String openId;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMechantId() {
		return mechantId;
	}

	public void setMechantId(String mechantId) {
		this.mechantId = mechantId;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getSpbillCreateIP() {
		return spbillCreateIP;
	}

	public void setSpbillCreateIP(String spbillCreateIP) {
		this.spbillCreateIP = spbillCreateIP;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeExpire() {
		return timeExpire;
	}

	public void setTimeExpire(String timeExpire) {
		this.timeExpire = timeExpire;
	}

	public String getGoodsTag() {
		return goodsTag;
	}

	public void setGoodsTag(String goodsTag) {
		this.goodsTag = goodsTag;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	@Override
	public String toString() {
		return "UnifiedOrder [appId=" + appId + ", mechantId=" + mechantId + ", deviceInfo=" + deviceInfo + ", nonceStr=" + nonceStr + ", sign="
				+ sign + ", body=" + body + ", detail=" + detail + ", attach=" + attach + ", outTradeNo=" + outTradeNo + ", feeType=" + feeType
				+ ", totalFee=" + totalFee + ", spbillCreateIP=" + spbillCreateIP + ", timeStart=" + timeStart + ", timeExpire=" + timeExpire
				+ ", goodsTag=" + goodsTag + ", notifyUrl=" + notifyUrl + ", tradeType=" + tradeType + ", productId=" + productId + ", openId="
				+ openId + "]";
	}

	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver());
		xstream.alias("xml", this.getClass());
		String xml = xstream.toXML(this);
		xml = xml.replaceAll("AppId", "appid").replaceAll("MechantId", "mch_id").replaceAll("DeviceInfo", "device_info");
		xml = xml.replaceAll("Sign", "sign").replaceAll("Body", "body").replaceAll("Detail", "detail").replaceAll("Attach", "attach");
		xml = xml.replaceAll("NonceStr", "nonce_str").replaceAll("OutTradeNo", "out_trade_no").replaceAll("FeeType", "fee_type");
		xml = xml.replaceAll("TotalFee", "total_fee").replaceAll("SpbillCreateIP", "spbill_create_ip").replaceAll("GoodsTag", "goods_tag");
		xml = xml.replaceAll("TimeStart", "time_start").replaceAll("TimeExpire", "time_expire").replaceAll("NotifyUrl", "notify_url");
		xml = xml.replaceAll("ProductId", "product_id").replaceAll("OpenId", "openid").replaceAll("TradeType", "trade_type");
		return xml;
	}

	public enum TradeType {
		JSAPI, NATIVE, APP, ;

	}

	public static void main(String[] args) {
		UnifiedOrder order = new UnifiedOrder();
		order.setAppId("appid");
		order.setAttach("附加数据");
		order.setBody("body");
		order.setDetail("detail");
		//order.setDeviceInfo("WEB");
		order.setFeeType("feetype");
		order.setGoodsTag("goodstag");
		order.setMechantId("mch_id");
		order.setNonceStr("nonce_str");
		order.setNotifyUrl("notify_url");
		order.setOpenId("openid");
		order.setOutTradeNo("out_trade_no");
		order.setProductId("productid");
		order.setSign("sign");
		order.setSpbillCreateIP("127.0.0.1");
		order.setTimeExpire("20091225091010");
		order.setTimeStart("20091225091010");
		order.setTotalFee("12000");
		order.setTradeType(TradeType.JSAPI.name());

		System.out.println(order.toXml());

	}

}

/**
 * 请求参数 字段名 变量名 必填 类型 示例值 描述 公众账号ID appid 是 String(32) wx8888888888888888
 * 微信分配的公众账号ID 商户号 mch_id 是 String(32) 1900000109 微信支付分配的商户号 设备号 device_info 否
 * String(32) 013467007045764 终端设备号(门店号或收银设备ID)，注意：PC网页或公众号内支付请传"WEB" 随机字符串
 * nonce_str 是 String(32) 5K8264ILTKCH16CQ2502SI8ZNMTM67VS
 * 随机字符串，不长于32位。推荐随机数生成算法 签名 sign 是 String(32) C380BEC2BFD727A4B6845133519F3AD6
 * 签名，详见签名生成算法 商品描述 body 是 String(32) Ipad mini 16G 白色 商品或支付单简要描述 商品详情 detail 否
 * String(8192) Ipad mini 16G 白色 商品名称明细列表 附加数据 attach 否 String(127) 说明
 * 附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据 商户订单号 out_trade_no 是 String(32)
 * 1217752501201407033233368018 商户系统内部的订单号,32个字符内、可包含字母, 其他说明见商户订单号 货币类型
 * fee_type 否 String(16) CNY 符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型 总金额
 * total_fee 是 Int 888 订单总金额，只能为整数，详见支付金额 终端IP spbill_create_ip 是 String(16)
 * 8.8.8.8 APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。 交易起始时间 time_start 否
 * String(14) 20091225091010
 * 订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
 * 交易结束时间 time_expire 否 String(14) 20091227091010
 * 订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则 商品标记
 * goods_tag 否 String(32) WXG 商品标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠 通知地址 notify_url 是
 * String(256) http://www.baidu.com/ 接收微信支付异步通知回调地址 交易类型 trade_type 是 String(16)
 * JSAPI 取值如下：JSAPI，NATIVE，APP，详细说明见参数规定 商品ID product_id 否 String(32)
 * 12235413214070356458058 trade_type=NATIVE，此参数必传。此id为二维码中包含的商品ID，商户自行定义。 用户标识
 * openid 否 String(128) oUpF8uMuAJO_M2pxb1Q9zNjWeS6o
 * trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识。下单前需要调用【网页授权获取用户信息】接口获取到用户的Openid。
 */
