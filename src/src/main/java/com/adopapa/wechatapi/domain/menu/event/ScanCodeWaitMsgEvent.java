package com.adopapa.wechatapi.domain.menu.event;

import java.util.Map;

import com.adopapa.wechatapi.domain.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * 扫码推事件且弹出“消息接收中”提示框的事件推送 
 */
@Deprecated
@XStreamAlias("xml")
public class ScanCodeWaitMsgEvent extends Event {
//	<xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
//	<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
//	<CreateTime>1408090606</CreateTime>
//	<MsgType><![CDATA[event]]></MsgType>
//	<Event><![CDATA[scancode_waitmsg]]></Event>
//	<EventKey><![CDATA[6]]></EventKey>
//	<ScanCodeInfo><ScanType><![CDATA[qrcode]]></ScanType>
//	<ScanResult><![CDATA[2]]></ScanResult>
//	</ScanCodeInfo>
//	</xml>

//	ToUserName	开发者微信号
//	FromUserName	发送方帐号（一个OpenID）
//	CreateTime	消息创建时间 （整型）
//	MsgType	消息类型，event
//	Event	事件类型，scancode_waitmsg
//	EventKey	事件KEY值，由开发者在创建菜单时设定
//	ScanCodeInfo	扫描信息
//	ScanType	扫描类型，一般是qrcode
//	ScanResult	扫描结果，即二维码对应的字符串信息
	
	@XStreamAlias("EventKey")
	private String eventKey;
	@XStreamAlias("ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;
	
	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}

	public ScanCodeWaitMsgEvent() {
		setMsgType("event");
		setEvent(MenuEventType.scancode_waitmsg.name());
	}

	@Override
	public void fromMap(Map<String, String> map) {
		// TODO Auto-generated method stub
		
	}
	

}
