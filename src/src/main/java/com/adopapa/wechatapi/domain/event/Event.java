package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import javax.persistence.MappedSuperclass;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@MappedSuperclass
public abstract class Event {
	/**
	 * ToUserName 开发者微信号 
	 * FromUserName 发送方帐号（一个OpenID） 
	 * CreateTime 消息创建时间 （整型）
	 * MsgType 消息类型，event
	 */
	@XStreamAlias("ToUserName")
	private String toUserName;
	@XStreamAlias("FromUserName")
	private String fromUserName;
	@XStreamAlias("CreateTime")
	private long createTime;
	@XStreamAlias("MsgType")
	private String msgType = "event";
	@XStreamAlias("Event")
	private String event;
	
	
	
	
	

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public abstract void fromMap(Map<String, String> map);

	

}
