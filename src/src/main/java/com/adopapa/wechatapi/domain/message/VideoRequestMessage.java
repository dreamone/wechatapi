package com.adopapa.wechatapi.domain.message;

import java.util.Map;

public class VideoRequestMessage extends RequestMessage {

	private String mediaId;

	private String thumbMediaId;

	public VideoRequestMessage() {
		setMsgType(MessageType.video.name());
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>1357290913</CreateTime>
	 * <MsgType><![CDATA[video]]></MsgType>
	 * <MediaId><![CDATA[media_id]]></MediaId>
	 * <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
	 * <MsgId>1234567890123456</MsgId> </xml>
	 */
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setFromUserName(map.get("FromUserName"));
		setToUserName(map.get("ToUserName"));
		setMsgId(Long.parseLong(map.get("MsgId")));
		setMediaId(map.get("MediaId"));
		setThumbMediaId(map.get("ThumbMediaId"));
	}

	@Override
	public String toString() {
		return "VideoRequestMessage [mediaId=" + mediaId + ", thumbMediaId=" + thumbMediaId + ", getToUserName()=" + getToUserName()
				+ ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType()
				+ ", getMsgId()=" + getMsgId() + "]";
	}
	
	

}
