package com.adopapa.wechatapi.domain.message;

public class Music {
	
	/**
	 * for response
	 */
	/**
	 *  Title	否	音乐标题
	 */
	private String title;
	
	/**
	 * Description	否	音乐描述 
	 */
	private String description;
	
	/**
	 * MusicURL	否	音乐链接 
	 */
	private String musicUrl;
	
	/**
	 *  HQMusicUrl	否	高质量音乐链接，WIFI环境优先使用该链接播放音乐
	 */
	private String hqMusicUrl;
	/**
	 *  ThumbMediaId	是	缩略图的媒体id，通过上传多媒体文件，得到的id
	 */
	private String thumbMediaId;
	
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMusicUrl() {
		return musicUrl;
	}
	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}
	public String getHqMusicUrl() {
		return hqMusicUrl;
	}
	public void setHqMusicUrl(String hqMusicUrl) {
		this.hqMusicUrl = hqMusicUrl;
	}
	public String getThumbMediaId() {
		return thumbMediaId;
	}
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}
	
	

}
