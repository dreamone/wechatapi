package com.adopapa.wechatapi.domain.menu;

import java.io.Serializable;
import java.util.Arrays;

import com.adopapa.wechatapi.domain.common.ResultMessage;

//button	是	一级菜单数组，个数应为1~3个
//sub_button	否	二级菜单数组，个数应为1~5个
//type	是	菜单的响应动作类型
//name	是	菜单标题，不超过16个字节，子菜单不超过40个字节
//key	click等点击类型必须	菜单KEY值，用于消息接口推送，不超过128字节
//url	view类型必须	网页链接，用户点击菜单可打开链接，不超过256字节
public class Menu extends ResultMessage implements Serializable {

	private static final long serialVersionUID = -5814186938066289979L;

	private Button[] buttons;

	public Menu(int errcode, String errmsg) {
		super(errcode, errmsg);
	}
	
	public Menu() {
		this(0, "ok");
	}

	public Button[] getButtons() {
		return buttons;
	}

	public void setButtons(Button[] buttons) {
		this.buttons = buttons;
	}

	@Override
	public String toString() {
		return "Menu [buttons=" + Arrays.toString(buttons) + "]";
	}

}
