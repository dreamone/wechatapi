package com.adopapa.wechatapi.domain.message.mass;

public class MpMedia {

	private String mediaId;

	public MpMedia(String mediaId) {
		setMediaId(mediaId);
	}

	public MpMedia() {

	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	
	/**
	 * 图文消息为mpnews，文本消息为text，语音为voice，音乐为music，
	 * 图片为image，视频为video
	 */
	public enum MessageType {
		mpnews, text, voice, music, image, mpvideo
		;
	}

}
