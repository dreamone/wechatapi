package com.adopapa.wechatapi.domain.message;

import java.util.Map;

public class TextRequestMessage extends RequestMessage {
	/**
	 * 文本消息内容
	 */
	private String content;
	
	public TextRequestMessage () {
		setMsgType(MessageType.text.name());
	}

	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}



	/**
	 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>1348831860</CreateTime>
	 * <MsgType><![CDATA[text]]></MsgType> <Content><![CDATA[this is a
	 * test]]></Content> <MsgId>1234567890123456</MsgId> </xml>
	 */
	@Override
	public void fromMap(Map<String, String> map) throws Exception {
		setCreateTime(Long.parseLong(map.get("CreateTime")));
		setFromUserName(map.get("FromUserName"));
		setToUserName(map.get("ToUserName"));
		setMsgId(Long.parseLong(map.get("MsgId")));
		setContent(map.get("Content"));
	}

	@Override
	public String toString() {
		return "TextRequestMessage [content=" + content + ", getToUserName()=" + getToUserName() + ", getFromUserName()=" + getFromUserName()
				+ ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType() + ", getMsgId()=" + getMsgId() + "]";
	}
	
	

}
