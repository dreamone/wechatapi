package com.adopapa.wechatapi.domain.event;

import java.util.Map;

import com.adopapa.wechatapi.domain.menu.event.ScanCodeInfo;
import com.adopapa.wechatapi.domain.menu.event.SendLocationInfo;
import com.adopapa.wechatapi.domain.menu.event.SendPicsInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("xml")
public class WeChatEvent extends Event {

	/**
	 * for CLICK, VIEW, Subscribe, Unsubscribe
	 */

	/**
	 * for LOCATION
	 */
	private double latitude;
	private double longitude;
	private double precision;

	/**
	 * for SCAN
	 */
	private String ticket;

	/**
	 * for templateMessage
	 */
	private String status;

	@XStreamAlias("EventKey")
	private String eventKey;

	/**
	 * for scancode_push, scancode_waitmsg
	 */
	@XStreamAlias("ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;

	/**
	 * pic_sysphoto, pic_photo_or_album, pic_weixin
	 */
	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	/**
	 * location_select
	 */
	@XStreamAlias("SendLocationInfo")
	private SendLocationInfo sendLocationInfo;

	@Override
	public void fromMap(Map<String, String> map) {

	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}

	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public SendLocationInfo getSendLocationInfo() {
		return sendLocationInfo;
	}

	public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
		this.sendLocationInfo = sendLocationInfo;
	}

	@Override
	public String toString() {
		return "WeChatEvent [getEventKey()=" + getEventKey() + ", getLatitude()=" + getLatitude() + ", getLongitude()=" + getLongitude()
				+ ", getPrecision()=" + getPrecision() + ", getTicket()=" + getTicket() + ", getStatus()=" + getStatus() + ", getScanCodeInfo()="
				+ getScanCodeInfo() + ", getSendPicsInfo()=" + getSendPicsInfo() + ", getSendLocationInfo()=" + getSendLocationInfo()
				+ ", getToUserName()=" + getToUserName() + ", getFromUserName()=" + getFromUserName() + ", getCreateTime()=" + getCreateTime()
				+ ", getMsgType()=" + getMsgType() + ", getEvent()=" + getEvent() + "]";
	}



}
