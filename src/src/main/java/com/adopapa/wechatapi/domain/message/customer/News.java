package com.adopapa.wechatapi.domain.message.customer;

import java.util.List;

import javax.persistence.MappedSuperclass;

import com.adopapa.wechatapi.domain.message.Article;

@MappedSuperclass
public class News {

	private List<Article> articles;

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

}
