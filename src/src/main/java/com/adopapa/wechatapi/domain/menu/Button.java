package com.adopapa.wechatapi.domain.menu;

import java.util.Arrays;

/**
 * button 是 一级菜单数组，个数应为1~3个 sub_button 否 二级菜单数组，个数应为1~5个 type 是 菜单的响应动作类型 name 是
 * 菜单标题，不超过16个字节，子菜单不超过40个字节 key click等点击类型必须 菜单KEY值，用于消息接口推送，不超过128字节 url
 * view类型必须 网页链接，用户点击菜单可打开链接，不超过256字节
 * 
 */
public class Button {
	private String name;

	private Button[] subButtons ;
	/**
	 * for subButton
	 */
	private String type;
	private String key;
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Button[] getSubButtons() {
		return subButtons;
	}

	public void setSubButtons(Button[] subButtons) {
		this.subButtons = subButtons;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Button [name=" + name + ", subButtons=" + Arrays.toString(subButtons) + ", type=" + type + ", key=" + key
				+ ", url=" + url + "]";
	}

}
