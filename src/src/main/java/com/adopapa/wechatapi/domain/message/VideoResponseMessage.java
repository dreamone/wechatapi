package com.adopapa.wechatapi.domain.message;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class VideoResponseMessage extends ResponseMessage {

	private Video video;

	public VideoResponseMessage() {
		setMsgType(MessageType.video.name());
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver());
		xstream.alias("xml", this.getClass());
		return xstream.toXML(this);
	}

}
