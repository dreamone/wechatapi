package com.adopapa.wechatapi.domain.message.template;

public class TemplateData {

	private String value;
	private String color;

	public TemplateData(String value, String color) {
		this.value = value;
		this.color = color;
	}

	public TemplateData() {

	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
