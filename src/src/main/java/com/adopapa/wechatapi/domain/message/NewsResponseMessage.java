package com.adopapa.wechatapi.domain.message;

import java.util.LinkedList;
import java.util.List;

import com.adopapa.wechatapi.util.MessageUtil;
import com.thoughtworks.xstream.XStream;

public class NewsResponseMessage extends ResponseMessage {

	/**
	 * 图文消息个数，限制为10条以内
	 */
	private int articleCount;

	/**
	 * 多条图文消息信息，默认第一个item为大图,注意，如果图文数超过10，则将会无响应
	 */
	private List<Article> articles ;

	public NewsResponseMessage() {
		setMsgType(MessageType.news.name());
	}

	public int getArticleCount() {
		return articleCount;
	}

	public void setArticleCount(int articleCount) {
		this.articleCount = articleCount;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

//	<xml>
//	<ToUserName><![CDATA[toUser]]></ToUserName>
//	<FromUserName><![CDATA[fromUser]]></FromUserName>
//	<CreateTime>12345678</CreateTime>
//	<MsgType><![CDATA[news]]></MsgType>
//	<ArticleCount>2</ArticleCount>
//	<Articles>
//	<item>
//	<Title><![CDATA[title1]]></Title> 
//	<Description><![CDATA[description1]]></Description>
//	<PicUrl><![CDATA[picurl]]></PicUrl>
//	<Url><![CDATA[url]]></Url>
//	</item>
//	<item>
//	<Title><![CDATA[title]]></Title>
//	<Description><![CDATA[description]]></Description>
//	<PicUrl><![CDATA[picurl]]></PicUrl>
//	<Url><![CDATA[url]]></Url>
//	</item>
//	</Articles>
//	</xml>
	@Override
	public String toXml() {
		XStream xstream = new XStream(MessageUtil.getXppDriver());
		xstream.alias("xml", this.getClass());
		xstream.alias("item", Article.class);
		return xstream.toXML(this);
	}
	
//	@Override
//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		sb.append("<xml>");
//		sb.append("<ToUserName><![CDATA[").append(getToUserName()).append("]]></ToUserName>");
//		sb.append("<FromUserName><![CDATA[").append(getFromUserName()).append("]]></FromUserName>");
//		sb.append("<CreateTime>").append(getCreateTime()).append("</CreateTime>");
//		sb.append("<MsgType><![CDATA[").append(getMsgType()).append("]]></MsgType>");
//		sb.append("<ArticleCount>").append(getArticleCount()).append("</ArticleCount>");
//		sb.append("<Articles>");
//		for(Article article : articles) {
//			sb.append("<item>");
//			sb.append("<Title><![CDATA[").append(article.getTitle()).append("]]></Title>");
//			sb.append("<Description><![CDATA[").append(article.getDescription()).append("]]></Description>");
//			sb.append("<PicUrl><![CDATA[").append(article.getPicUrl()).append("]]></PicUrl>");
//			sb.append("<Url><![CDATA[").append(article.getUrl()).append("]]></Url>");
//			sb.append("</item>");
//		}
//		sb.append("</Articles>");
//		sb.append("</xml>");
//		return sb.toString();
//	}
	
	public static void main(String[] args) {
		Article article1 = new Article();
		article1.setTitle("微信公众帐号开发教程Java版");
		article1.setDescription("");
		// 将图片置为空
		article1.setPicUrl("");
		//article1.setUrl("http://blog.csdn.net/lyq8479");

		Article article2 = new Article();
		article2.setTitle("第4篇\n消息及消息处理工具的封装");
		article2.setDescription("");
		article2.setPicUrl("http://avatar.csdn.net/1/4/A/1_lyq8479.jpg");
		article2.setUrl("http://blog.csdn.net/lyq8479/article/details/8949088");

		Article article3 = new Article();
		article3.setTitle("第5篇\n各种消息的接收与响应");
		article3.setDescription("");
		article3.setPicUrl("http://avatar.csdn.net/1/4/A/1_lyq8479.jpg");
		article3.setUrl("http://blog.csdn.net/lyq8479/article/details/8952173");

		Article article4 = new Article();
		article4.setTitle("第6篇\n文本消息的内容长度限制揭秘");
		article4.setDescription("");
		article4.setPicUrl("http://avatar.csdn.net/1/4/A/1_lyq8479.jpg");
		article4.setUrl("http://blog.csdn.net/lyq8479/article/details/8967824");

		List<Article> articles = new LinkedList<Article>();
		articles.add(article1);
		articles.add(article2);
		articles.add(article3);
		articles.add(article4);
		NewsResponseMessage newsMessage = new NewsResponseMessage();
		newsMessage.setArticleCount(articles.size());
		newsMessage.setArticles(articles);
		System.out.println(newsMessage.toXml());
		System.err.println(newsMessage.toString());
		
	}

}
