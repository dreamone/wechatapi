package com.adopapa.wechatapi.domain.message.template;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.adopapa.wechatapi.util.JsonUtil;

public class TemplateMessage {
	private String toUser;
	private String templateId;
	private String url;
	private String topColor;
	private Map<String, TemplateData> data = new LinkedHashMap<String, TemplateData>();

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTopColor() {
		return topColor;
	}

	public void setTopColor(String topColor) {
		this.topColor = topColor;
	}

	public Map<String, TemplateData> getData() {
		return data;
	}

	public void setData(Map<String, TemplateData> data) {
		this.data = data;
	}

	public String toJson() throws IOException {
		String json = JsonUtil.bean2Json(this);
		json = json.replaceAll("toUser", "touser").replaceAll("templateId", "template_id").replaceAll("topColor", "topcolor");
		return json;
	}

	public static void main(String[] args) throws IOException {
		TemplateData first = new TemplateData("恭喜你购买成功！", "#173177");
		TemplateData keynote1 = new TemplateData("巧克力", "#173177");
		TemplateData keynote2 = new TemplateData("39.8元", "#173177");
		TemplateData keynote3 = new TemplateData("2014年9月16日", "#173177");
		TemplateData remark = new TemplateData("欢迎再次购买！", "#173177");

		TemplateMessage templateMessage = new TemplateMessage();
		templateMessage.setToUser("toUser");
		templateMessage.setTemplateId("templateId");
		templateMessage.setTopColor("topColor");
		templateMessage.setUrl("url");
		templateMessage.getData().put("first", first);
		templateMessage.getData().put("keynote1", keynote1);
		templateMessage.getData().put("keynote2", keynote2);
		templateMessage.getData().put("keynote3", keynote3);
		templateMessage.getData().put("remark",remark);

		System.out.println(templateMessage.toJson());

	}

}
// {
// "touser":"OPENID",
// "template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
// "url":"http://weixin.qq.com/download",
// "topcolor":"#FF0000",
// "data":{
// "first": {
// "value":"恭喜你购买成功！",
// "color":"#173177"
// },
// "keynote1":{
// "value":"巧克力",
// "color":"#173177"
// },
// "keynote2": {
// "value":"39.8元",
// "color":"#173177"
// },
// "keynote3": {
// "value":"2014年9月16日",
// "color":"#173177"
// },
// "remark":{
// "value":"欢迎再次购买！",
// "color":"#173177"
// }
// }
// }