package com.adopapa.wechatapi.domain.token;

import java.io.Serializable;

import com.adopapa.wechatapi.domain.common.ResultMessage;

public class JsApiTicket extends ResultMessage implements Serializable {

	private static final long serialVersionUID = 4150567105673692503L;

	public JsApiTicket(int errcode, String errmsg) {
		super(errcode, errmsg);
	}

	public JsApiTicket() {
		this(0, "ok");
	}

	// 获取到的凭证
	private String ticket;
	// 凭证有效时间，单位：秒 7200秒 2小时
	private int expiresIn;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	@Override
	public String toString() {
		return "JsApiTicket [ticket=" + ticket + ", expiresIn=" + expiresIn + ", getErrcode()=" + getErrcode() + ", getErrmsg()=" + getErrmsg() + "]";
	}
	
}
