package com.adopapa.wechatapi.domain.menu.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;


public class SendLocationInfo {
	// <SendLocationInfo><Location_X><![CDATA[23]]></Location_X>
	// <Location_Y><![CDATA[113]]></Location_Y>
	// <Scale><![CDATA[15]]></Scale>
	// <Label><![CDATA[ 广州市海珠区客村艺苑路 106号]]></Label>
	// <Poiname><![CDATA[]]></Poiname>
	// </SendLocationInfo>
	// Location_X X坐标信息
	// Location_Y Y坐标信息
	// Scale 精度，可理解为精度或者比例尺、越精细的话 scale越高
	// Label 地理位置的字符串信息
	// Poiname 朋友圈POI的名字，可能为空

	@XStreamAlias("Location_X")
	private String locationX;
	@XStreamAlias("Location_Y")
	private String locationY;
	@XStreamAlias("Scale")
	private String scale;
	@XStreamAlias("Label")
	private String label;
	@XStreamAlias("Poiname")
	private String poiName;

	public String getLocationX() {
		return locationX;
	}

	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}

	public String getLocationY() {
		return locationY;
	}

	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPoiName() {
		return poiName;
	}

	public void setPoiName(String poiName) {
		this.poiName = poiName;
	}

}
