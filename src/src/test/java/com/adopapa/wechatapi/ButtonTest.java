package com.adopapa.wechatapi;

import org.junit.Test;

import com.adopapa.wechatapi.domain.menu.Button;
import com.adopapa.wechatapi.domain.menu.Menu;
import com.adopapa.wechatapi.util.JsonUtil;

public class ButtonTest {

	@Test
	public void test() {
		String buttonString = "{  \"button\":[   {	\"type\":\"click\",          \"name\":\"今日歌曲\",          \"key\":\"V1001_TODAY_MUSIC\"      },      {           \"name\":\"菜单\",           \"sub_button\":[           {	               \"type\":\"view\",               \"name\":\"搜索\",               \"url\":\"http://www.soso.com/\"            },            {               \"type\":\"view\",               \"name\":\"视频\",               \"url\":\"http://v.qq.com/\"            },            {              \"type\":\"click\",               \"name\":\"赞一下我们\",               \"key\":\"V1001_GOOD\"            }]       }] }";
		try {
			buttonString = buttonString.replaceAll("sub_button", "subButtons").replace("button", "buttons");
			System.out.println(JsonUtil.json2Bean(buttonString, Menu.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
