package com.adopapa.wechatapi.menuevent;

import java.io.InputStream;

import org.junit.Test;

import com.adopapa.wechatapi.domain.event.WeChatEvent;
import com.adopapa.wechatapi.util.WeChatUtil;
import com.adopapa.wechatapi.xstream.XstreamUtil;

public class SubscribeEvent {
	
	@Test
	public void test() {
		try {
			InputStream inputStream = SubscribeEvent.class.getResourceAsStream("subscribe.xml");
			WeChatEvent  menuEvent = XstreamUtil.XML2Bean(inputStream, WeChatEvent.class);
			System.out.println(menuEvent);
			
//			StringBuilder sb = new StringBuilder();
//			sb.append("<xml>");
//			sb.append("<ToUserName><![CDATA[gh_748a16cfbe0f]]></ToUserName>");
//			sb.append("<FromUserName><![CDATA[okkwgt3rADJlkQnV4UGsrf4zGFno]]></FromUserName>");
//			sb.append("<CreateTime>1424239712</CreateTime>");
//			sb.append("<MsgType><![CDATA[event]]></MsgType>");
//			sb.append("<Event><![CDATA[subscribe]]></Event>");
//			sb.append("</xml>");
			StringBuilder sb = new StringBuilder();
			sb.append("<xml>");
			sb.append("<ToUserName><![CDATA[123456]]></ToUserName>");
			sb.append("<FromUserName><![CDATA[123456]]></FromUserName>");
			sb.append("<CreateTime>1424239712</CreateTime>");
			sb.append("<MsgType><![CDATA[event]]></MsgType>");
			sb.append("<Event><![CDATA[VIEW]]></Event>");
			sb.append("<EventKey><![CDATA[classmate/toRegister.do]]></EventKey>");
			sb.append("</xml>");
			
			WeChatUtil.httpRequest("http://localhost:8080/party/wechat", "POST", sb.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
