package com.adopapa.wechatapi.menuevent;

import java.io.InputStream;

import org.junit.Test;

import com.adopapa.wechatapi.domain.event.ClickEvent;
import com.adopapa.wechatapi.domain.event.WeChatEvent;
import com.adopapa.wechatapi.domain.event.ViewEvent;
import com.adopapa.wechatapi.domain.menu.event.LocationSelectEvent;
import com.adopapa.wechatapi.domain.menu.event.PicPhotoAlbumEvent;
import com.adopapa.wechatapi.domain.menu.event.PicSysPhotoEvent;
import com.adopapa.wechatapi.domain.menu.event.PicWeixinEvent;
import com.adopapa.wechatapi.domain.menu.event.ScanCodePushEvent;
import com.adopapa.wechatapi.domain.menu.event.ScanCodeWaitMsgEvent;
import com.adopapa.wechatapi.util.MessageUtil;
import com.adopapa.wechatapi.xstream.XstreamUtil;
import com.thoughtworks.xstream.XStream;

public class MenuEventTest {
	
	@Test
	public void testClick() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("click.xml");
			ClickEvent clickEvent = XstreamUtil.XML2Bean(inputStream, ClickEvent.class);
			System.out.println(clickEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testView() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("view.xml");
			ViewEvent viewEvent = XstreamUtil.XML2Bean(inputStream, ViewEvent.class);
			System.out.println(viewEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testScanCodePush() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("scancode.xml");
			ScanCodePushEvent scanCodePushEvent = XstreamUtil.XML2Bean(inputStream, ScanCodePushEvent.class);
			System.out.println(scanCodePushEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testScanCodeWaitMsg() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("scancode.xml");
			ScanCodeWaitMsgEvent scanCodeWaitMsgEvent = XstreamUtil.XML2Bean(inputStream, ScanCodeWaitMsgEvent.class);
			System.out.println(scanCodeWaitMsgEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPicWeixin() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("pic.xml");
			PicWeixinEvent picWeixinEvent = XstreamUtil.XML2Bean(inputStream, PicWeixinEvent.class);
			System.out.println(picWeixinEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testPicSysPhoto() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("pic.xml");
			PicSysPhotoEvent picSysPhotoEvent = XstreamUtil.XML2Bean(inputStream, PicSysPhotoEvent.class);
			System.out.println(picSysPhotoEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	@Test
	public  void testPicPhotoAlbum () {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("pic.xml");
			PicPhotoAlbumEvent picPhotoAlbumEvent = XstreamUtil.XML2Bean(inputStream, PicPhotoAlbumEvent.class);
			System.out.println(picPhotoAlbumEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testLocationSelect() {
		try {
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("location.xml");
			LocationSelectEvent  locationSelectEvent = XstreamUtil.XML2Bean(inputStream, LocationSelectEvent.class);
			System.out.println(locationSelectEvent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testMenuEvent() {
		try {
//			InputStream inputStream = MenuEventTest.class.getResourceAsStream("location.xml");
//			InputStream inputStream = MenuEventTest.class.getResourceAsStream("pic.xml");
//			InputStream inputStream = MenuEventTest.class.getResourceAsStream("scancode.xml");
			InputStream inputStream = MenuEventTest.class.getResourceAsStream("view.xml");
//			InputStream inputStream = MenuEventTest.class.getResourceAsStream("click.xml");
			WeChatEvent  menuEvent = XstreamUtil.XML2Bean(inputStream, WeChatEvent.class);
			System.out.println(menuEvent);
			
			XStream xstream = new XStream(MessageUtil.getXppDriver());
			xstream.alias("xml", WeChatEvent.class);
			System.out.println(xstream.toXML(menuEvent));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
