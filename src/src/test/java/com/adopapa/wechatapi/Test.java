package com.adopapa.wechatapi;

import javax.script.ScriptException;

public class Test {
	
	public static void main(String[] args) throws ScriptException {
		double data = 1.23E5;
		String formulate = "y =x*x*x+10*x*x+5*x+6";
		formulate = "var x=".concat(String.valueOf(data)).concat(";").concat(formulate).concat(";");
		Object result = ScriptEnginer.getJavaScriptEnginer().eval(formulate);
		
		System.out.println(result);
	}

}
